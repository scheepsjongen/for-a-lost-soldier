# For a Lost Soldier

This eBook was created from various digital scans using OCR tools and manual editing of *Voor een Verloren Soldaat* (*For a Lost Soldier*) by Rudi van Dantzig, translated from Dutch by Arnold J. Pomerans. As of 2021, it’s [incredibly difficult to find](https://www.bookfinder.com/search/?isbn=9780854492374&st=xl&ac=qr) any remaining print copies of this translation, last published by [Gay Men’s Press](https://gmppubs.wordpress.com/) in 1996. I hope this project ensures that the book, in English, continues to be accessible and remembered.

(ISBN: [0 85449 237 2](https://isbnsearch.org/isbn/0854492372)).

From [Wikipedia](https://en.wikipedia.org/wiki/Rudi_van_Dantzig):

> Rudi van Dantzig (4 August 1933 – 19 January 2012) was a Dutch choreographer, company director, and writer. He was a pivotal figure in the rise to world renown of Dutch ballet in the latter half of the twentieth century.
> 
> During liberation of the Netherlands in May 1945, he met Walter Cook, a young soldier in the First Canadian Army, which was largely responsible for the defeat of German forces in Holland. His friendship and love affair with this soldier, who was lost to him when he was suddenly transferred away, provided the basis for his prizewinning novel Voor een Verloren Soldaat (For a Lost Soldier), published in the Netherlands in 1986 and later filmed and translated into English.

Please considering buying the [original](https://www.bookfinder.com/search/?author=Dantzig%2C+Rudi+van&title=Voor+Een+Verloren+Soldaat&lang=nl&st=xl&ac=qr
) in Dutch to support the publisher.

## Files

- [Markdown](book.md) (source, lightweight markup / plain text file)
- [EPUB](https://codeberg.org/scheepsjongen/for-a-lost-soldier/raw/branch/main/files/for-a-lost-soldier.epub) (eReaders, Google Play Books, Apple Books, etc)
- [AZW3](https://codeberg.org/scheepsjongen/for-a-lost-soldier/raw/branch/main/files/for-a-lost-soldier.azw3) (Amazon Kindle)
- [PDF](https://codeberg.org/scheepsjongen/for-a-lost-soldier/raw/branch/main/files/for-a-lost-soldier.pdf)
- [HTML](https://codeberg.org/scheepsjongen/for-a-lost-soldier/raw/branch/main/files/for-a-lost-soldier.html) (web browsers)

The [markdown](book.md) file is the source for all the generated files. If you find any issues you can open an issue or merge request on this project.

## Building the book (macOS)

```
brew install pandoc mactex calibre
./generate.sh
```
