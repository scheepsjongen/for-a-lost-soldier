# Preface

This eBook was created from various digital scans using OCR tools and manual editing of *Voor een Verloren Soldaat* (*For a Lost Soldier*) by Rudi van Dantzig, translated from Dutch by Arnold J. Pomerans. As of 2021, it’s [incredibly difficult to find](https://www.bookfinder.com/search/?isbn=9780854492374&st=xl&ac=qr) any remaining print copies of this translation, last published by [Gay Men’s Press](https://gmppubs.wordpress.com/) in 1996. I hope this project ensures that the book, in English, continues to be accessible and remembered.

Digitised by *scheepsjongen* in July 2021. The source is available at [https://codeberg.org/scheepsjongen/for-a-lost-soldier](https://codeberg.org/scheepsjongen/for-a-lost-soldier).

# Hunger Winter

## 1

THE ALARM CLOCK goes off at five o’clock in the morning. I stumble out of my little room. A rectangular shape is waiting relentlessly in the semi-darkness of the passage: the suitcase. This is the day then.

The linoleum is chill under my feet. Shivering, I wash in the basin. Even the small stream of tap-water seems noisy. My father walks about in stockinged feet, moving cautiously so as not to wake my little brother. He bends over the kitchen table, cuts a piece of bread in half and gives me a searching look.

‘Are you scared?’

‘No.’ I find it hard to talk, my throat is tight. I push the piece of bread away.

‘Too early for you?’

He helps me put a parting in my hair. I see myself in the mirror, a wan smudge.

Last week at school a nurse had rooted about in my hair with two small glass rods, looking for lice. I had felt her icy feelers make sudden movements with the total single mindedness of a hunter in pursuit of prey. A little later, sweating, I saw her conferring with the teacher, her eyes fixed on me.

‘I’ve seen enough now, children,’ she said, ‘you’ll hear the results next week.’

What had she seen? My hairless head, shaved bald? ‘Nit king,’ they’d call after you then and wouldn’t let you come anywhere near them.

Luckily I’m leaving, not a moment too soon.

‘Get a move on,’ whispers my father. ‘We’ve got to go.’

The suitcase is standing in the passage by the door, a looming and inescapable presence.

I vanish into my little room to put on my coat. Aimlessly I walk about in the small oblong-shaped space. What am I looking for? Everything has been packed. I yawn soundlessly, nervously. When I go back into the passage, the floorboards make a grating noise, and I know that every joint and crevice in the staircase will creak. I stand still, at a loss for a moment, then I go on down the dark stairs. My feet feel for the treads which I have run up and down confidently hundreds of times a day. My father, turning the key in the door, hisses a warning after me, ‘Hush, the neighbours are still asleep.’

On the second-floor landing I wait for my father, let him walk past me and then follow him down the dark tunnel, one hand anxiously clasping the banister.

The front door stands half open, a beam of early sunshine filled with swirling specks of dust slanting in.

My father hands me the suitcase.

‘You may as well put it down on the pavement.’

I take a few uncertain steps into the deserted street. My long shadow falls brightly outlined across the paving stones. The cool morning air tickles my nose and blows across my bare knees. I sneeze. The sound rebounds against the houses. I look up at the windows. Some are criss-crossed with strips of brown sticky-tape or covered with rolls of blackout paper. The makeshift pipes of emergency stoves jut out here and there
through broken window-panes.

The balcony doors stand open like gaping mouths, breathing stillness and sleep. This is the day then, when I am leaving all this behind. By the time the children in the street wake up I won’t be there any more.

Annie and Willie, Jan, little Karel, Appie: I have gone away. Gone. To the farmers.

<br>

I still knew nothing about it up to the day before yesterday. Then one of my father’s colleagues — ‘Frits’, my father called him — had come round. They had gone into the front room to talk with the door closed.

‘Go to your room for a bit. We shan’t be long.’

When Mr Anderson had left, my father came and sat on my bed and put his arm round me. That was odd.

The next morning I watched my father feeding my little brother, spoonful by spoonful. Without looking up, he had scraped the empty plate as if trying to scratch the bottom off and then suddenly he had said, slowly and deliberately, ‘You know, Jeroen, you could go to stay with some farmers for a little while. There’s a lorry leaving on Monday, you can go with it. Lots of food, and playing in the fresh air.’

He had looked at me without conviction, as if he were talking to a stranger.

‘I think you should, you know. It’ll do you good to get away from here for a while. And there’ll be that much more to eat for Bobbie.’

I had said nothing, indifferently drawing little figures on the table with my finger.

‘It won’t be for long, the war will soon be over. They’re already in France. And then you’ll be back home.’

<br>
	
I look up at our balcony where the curtain is billowing in the wind. Behind it my little brother is asleep in the small side-room. If he should wake up suddenly and start to cry there’ll be no one to lift him out of his cradle. He’ll bawl the place down.

Leaving home, something I had at first sensed dully and vaguely, now hits me with full force. Tension seizes me between the eyebrows, terror reverberates in the aching hollow of my belly. My body contracts, I feel as if I’m going to be sick.

My father comes out with the bicycle, pulls the door shut softly behind him and parks the bicycle at the kerb as quietly as he can. His face is drawn and tired. I climb on to the luggage carrier and my father places the suitcase between us. I move back a little; the bars of the carrier press coldly against my behind, the edge of the suitcase cuts into my thighs.

We take off with an awkward wobble, the iron rims rattling across the cobblestones. With each bump I shrink more into myself, clingling tightly to my father’s coat.

Before we clatter around the corner I take a last look at our window, our balcony. The sunlight is reflected by the glass and the white net curtain waves like a pale hand. Bye.

We take Admiraal de Ruyterweg and then the Rozengracht. I have never seen the streets so empty, so deserted. The rusty tram-lines stretch out far down the road. We get off the bicycle at a broken piece of road full of sandy potholes and walk along the pavement. I have trouble balancing the suitcase on the carrier. Why are we in such a hurry?

We walk past an empty house. Through the smashed window-panes I can see a man and a woman down on their knees. They are prising planks out of the floor. As we walk by, they stop panic-stricken. The woman presses herself against the floor trying to hide from us. Ashamed of our indiscreet staring, I turn my head away.

‘The lorry is on the Dam, near the Royal Palace,’ my father says. ‘At least it had better be there by now, or else you’ll just have to wait by yourself. I’ll have to get back straight away, on account of the Jerries. And Bobbie’ll be waking up soon.’

People have formed a silent queue outside a dilapidated shop with a dingy display window. It seems to have been disused for some time. We rattle past. I catch the eye of a man leaning against the wall. He has a distant, grey look.

I want to go back, I don’t want to leave. The lorry surely won’t be there yet, it’s too early.

But then, on the shady side of the Palace, I see it: a pick-up truck with a canvas cover over the loading platform. A man is sitting slumped in the cab, looking straight in front of him through the filthy windscreen.

‘Is this the lorry for Friesland, for the Fokker children?’

The man has sleepy eyes. He mumbles something to my father that I can’t catch. Perhaps we are wrong, perhaps the whole thing is a mistake.

We go to the back of the lorry and my father hoists my suitcase over the tail-board.

‘What do you want to do, wait outside or shall I help you in?’

I look at how high the back of the lorry is. I won’t be able to get up there by myself.

‘Please lift me in.’

The iron floor is cold against my knees. There is a pile of grey blankets in a corner, but I daren’t take one.

‘Daddy.’

My father has gone back to the front of the lorry and is talking to the driver again.

‘Daddy, I don’t feel very well. I’d better go back with you, I’m beginning to feel really sick.’

If it had been my mother I would have burst into loud, heart-rending sobs and clung tightly to her. She would have taken me back home with her.

‘Chin up, my boy.’ My father hands me a registration card with a few sheets of ration coupons attached to it. ‘Look after these because they’re going to want them in Friesland. Don’t lose them, whatever you do.’

He hoists himself up on to the tail-board and I throw my arms around his neck. ‘Take care, won’t you. And write.’

I can’t get an answer out. My body is trembling with pent-up terror. Through a haze I watch the blue of his coat receding from the lorry. A wave of his arm, lit brightly by the sun, and then he is round the corner. Gone.

The man in the cab taps against the window.

It won’t be long now,’ he calls out, ‘just as soon as everybody turns up.’

The lorry door slams shut. A couple of sparrows are chirping in the sun on the other side of the street. I crawl to the furthest corner of the platform and position the suitcase next to me. I draw my knees up and feel a drop falling onto my leg and trickling down along my thigh.

I am woken up by someone nudging my shoulder. A girl is sitting next to me and is trying, with a great deal of tugging and pulling, to shove a bag behind her back. She is leaning against me, making a sound halfway between laughing and crying. There is a bow in her hair that has come loose and she has a red nose which she keeps clearing with snuffling noises.

I sit up straight. At least seven children must have joined us. They are quiet and pale, no one talks. Some are nicely dressed, others look shabby. I see that one of the boys has a shaved head. Nit king. The clothes of the girl next to me give off a mean, stale smell that catches in my throat. Outside, in the sun, a woman with a green headscarf stands by the side of the lorry talking animatedly to the driver.

<br>

The town has come to life. People are walking past the lorry, some peering inside inquiringly, as if searching for something. A German lorry is standing a bit further up the street, an armed soldier by its side.

‘Listen, children,’ the woman with the headscarf is leaning over inside, ‘the Germans won’t let us leave until evening. And there are still a few more children to come. Anyone wanting to spend a penny had best come with me now.’

The girl next to me clambers out. I need to pee, too, but stay in my corner. I lay my head on the suitcase and fall into a vacant doze. This lorry doesn’t exist, these children don’t exist, I myself don’t exist. I go on registering all the sounds in the lorry; they seem far away, as if coming from another life.

<br>

Time seems to dissolve into waiting. I don’t know how long we have been there, three hours, ten hours, a day even.

Still more children have joined us and now the lorry seems crammed with bodies, suitcases, bags, all thrown chaotically together. The lady is sitting in the middle, her knees drawn up. She has draped the green scarf over her legs, looking watchfully around as if checking up on us one by one.

The light outside has changed; there are hardly any people in the streets any more. I can still see the German lorry, but the soldier has gone.

I raise my hand.

‘I need to go to the toilet.’

Silently the lady beckons. I crawl to the back of the lorry and climb over the tail-board. She holds me by the elbows and lets me down. ‘Do it beside the truck.’ She points. There is a narrow strip of pavement between the lorry and the Palace. My legs feel wobbly. When I try to open my trousers I am too weak to undo the buttons and nearly wet my pants.

I shiver as the jet spatters against the wall. A sound escapes from my throat and my teeth chatter. It is evening, the sky is turning grey. Shuddering, I look over my shoulder and wonder in which direction I would have to go to get home.

This is all a fragile, glassy dream; it feels as if everything is blurred and happened a long time ago. I turn round. The driver is hanging out of the window, watching me pee. I move backwards. There is a dark stain against the wall and on the pavement, a sign I have left behind, a distress signal for anyone who may be looking for me.

I am hoisted back up again and creep into my corner. Each child is an island to himself, one you may not touch.

Opposite me I see a face I recognise. It is the boy who has been sitting with his head half-hidden under a coat. He smiles and I smile back uncertainly.

‘Jan,’ I say.

How can that be, how has Jan ended up in this lorry? Jan Hogervorst, from our street, from the house across the road.

Jan sits up straight. ‘Come over here,’ he whispers and shoves something to one side, ‘there’s room.’

But I know better than to leave my suitcase unattended. Jan
crawls across to me, pushing in between me and the girl. Immediately I feel that I can sit more easily. Our legs lean against each other and Jan pushes one hand into the back of my knee.

‘Cold legs.’

He turns to the girl. ‘What’s your name?’ he says bossily.

‘He’s called Jeroen and I’m Jan. Are you going to Friesland too?’

She leans away from him with an unfriendly expression. ‘I live near here,’ she says. ‘In Bloedstraat. My name is Greetje.’

She starts to cry, softly at first, then in long drawn-out wails. The lady turns round, looking furious.

‘Be a bit quieter, all of you.’

‘She stinks like a horse as well,’ says Jan. ‘Shove up a bit, I can’t stand it.’

Grudgingly I draw further back into the corner. I can feel Jan sagging closer towards me, breathing heavily. Then I too fall asleep again.

<br>

I can see the tops of trees flitting by through a gap in the canvas overhead. Flashes of sky and the violent rustle of branches and leaves.

‘We’ve nearly reached Hoorn,’ whispers the teacher, ‘but we’ve been told to wait by the IJsselmeer Dam, until it’s completely dark.’ She looks at her watch. ‘Half past eight.’

I think of my mother, wonder where she is. Perhaps we drove past her and I didn’t know.

She left three days ago, with her sister. Both of them were on rickety old bikes. ‘We’re going to the polder,’ she had said, ‘to get some potatoes from the farmers. Maybe other things too, if we’re lucky. Flour and milk.’

I had watched them go from our balcony, cycling together to the end of the street, empty panniers on their luggage carriers. When she looked up and waved I hadn’t waved back, making sure she knew that I was cross.

By the time she gets back I’l be gone; only Daddy and Bobbie will be there. She should never have gone away and now it’s too late. Is this the polder that we’re driving through? If I were togo and sit near the tail-board, I should have a better view of the road. You never know, I might suddenly see her.

Will she be upset to find that I’m gone, will she write to me? But perhaps the post doesn’t go to Friesland.

The lorry comes to an abrupt halt. I fall against Jan, a suitcase topples over. ‘The Germans,’ I hear someone say.

There are voices and soon afterwards the tail-board is let down. Outside stands a soldier, yellow light reflecting off his helmet. He is carrying a rifle. A man with a cap looks inside, our driver behind him with his hands in his pockets. The lady crawls through to the end of the lorry and passes some papers to the German. It all looks very conspiratorial and reminds me of the war games we used to play in the evenings in the bushes along the canal.

‘Sit up straight, all of you, and keep your luggage next to you.’

The soldier with the rifle climbs in and moves bent over double between us. He shoves the cases about and orders two boys to get up. Then they are told to sit down again and he gives them both a pat on the cheek. No one makes a sound.

The man with the cap jumps out of the lorry and waves.

‘Gute Reise.’

‘He says have a good journey,’ explains the lady, ‘so
wave to him.’

Beside us Greetje gives a stifled giggle.

A soldier who looms up out of the dark shines a torch into the back of the lorry, a piercing searchlight. Then suddenly it is dark again. We all crawl out from our places and everyone starts to talk at once. ‘What did he do that for? What did he want you to do? Did he hit you?’ We are all curious about the two boys, they are our heroes.

‘Let’s push all the luggage together now, then there’ll be room for everyone. If we spread the blankets out we’ll all be able to get some sleep.’

We lie down close together, like spoons in a drawer. Jan and I have our faces turned towards each other, I can feel his breath on my face. Suddenly, everything is mysterious and exciting. We look at each other and I feel a prod under the blanket.

Jan chuckles. He turns over. Silence.

I can see some people approaching out of the dark. They look around cautiously and move without making a noise.

‘Who’s in charge here?’ a voice whispers. ‘We’ve got to get to Amsterdam. A boy has been taken ill, he’s lying up the road on the verge. Can you take him with you?’

Nota sound. Is the lady asleep or is she pretending? I try to make out how many people there are and prop myself up a little. Perhaps my mother is there with them.

‘Please, there’s no time to be lost. Isn’t there anyone in charge here?’

‘We’re going the other way. This is a children’s transport to Friesland.’

The lady’s voice sounds muffled, as if she were holding a cloth to her mouth.

‘You’d best get away from here, it’s too dangerous.’

Someone starts to moan softly. Everyone in the lorry is wide awake. My heart thumps. There are footsteps and German voices.

The strangers duck out of sight. We can hear them running for it.

<br>

The lorry is moving again, jolting along slowly.

‘Now we’re going up the Dam,’ whispers the lady. ‘Let’s hope the planes don’t see us, or we’ve had it.’

Jan nestles close up to me, one arm around me, his knees tucked into the backs of mine. Unexpectedly a hand grips me firmly between the legs.

‘Nice nookie,’ he whispers in my ear.

I jerk my head up. The lady has lain down as well, I can see lots of little grey hummocks all around me. In the other corner there is a sound as if someone is choking back sobs. The grip in my crotch loosens.

‘Any idea how much longer all this is going to take? We must be sure to stay together, you know. Hey, are you listening? We mustn’t let them split us up.’

I wish Jan would shut up.

‘Go to sleep, everyone,’ hisses a voice.

I take hold of Jan’s hand and pull it up to my chest. Now and then the lorry drives over a pothole and my head bumps against the hard floor. Jan’s arm has a safe feel to it. I listen to the noise of the tyres on the road.

<br>

We wake up with a start. The tail-board has banged open and a few suitcases have fallen out with a crash; I can hear them sliding around the road.

There is crying. A few children stand up confused and are pulled back down again. We thump our fists against the cab, but the driver keeps going. Outside it is pitch-black.

The lady leans out and swings the tail-board back up.

It could have been my suitcase, I think. No more underwear, no towel, no socks. Have I still got my registration card?

I feel in my trouser pocket. Jan’s arm slips off me. I must
remember to keep my hand on my registration card, otherwise I’l lose that as well.

<br>

The driver drives over the Dam with dipped headlights. That ceaseless murmuring is the sea. A gull drifts over the road like a scrap of paper and disappears into the dark. The lorry is a mole burrowing through the night.

All the other children sleep, like animals seized with fear. Their bodies shake in unison at every bump in the road. Only the lady is awake. She stares at the flapping canvas with wide-open eyes.

## 2

DISHEVELLED, OUR FACES grubby and apprehensive, we huddle together in the morning mist. Two hulking men have lifted us out of the lorry and now we are waiting, expecting the worst.

Jan is sitting on his suitcase, staring at the ground, yawning without stopping. No one says a word.

The lady and the driver have walked away from one end of the lorry. What are they talking about?

We are at a crossroads. I can see quiet country lanes disappearing in three directions. The village stretches out on two sides: small houses, little gardens, a church. Not a soul is to be seen, everyone is asleep.

The road in front of us disappears into dank pastures where the motionless backs of cows stick up out of the mist like black and white stones in a grey river. Is this Friesland? Surely it can’t be.

‘Friesland is one of the northernmost provinces in the Netherlands’, I was taught at school, which would make me think of frozen white mountains and snow-fields. The North Pole, Iceland, Friesland: they all conjured up a cold and mysterious image of icebergs and northern lights, of unknown worlds benumbed with cold.

But where we are now is just like those outskirts of Amsterdam that I would see when I had a day out bicycling with my mother and father during the holidays: green trees, a village street, small houses with low-pitched roofs, absolutely nothing to boast about later in our street back home.

But perhaps this is only a stop on the journey, a short break on the road to our mysterious destination. Several men come out of a gloomy little building with a pointed gable, something halfway between a church and a storehouse. Country people because they are wearing clogs.

They talk in low voices and walk unhurriedly, ponderously, towards the lady. One of them holds the doors of the little building open and beckons us: we are to go inside.

It smells damp in the building, musty, as if no one has been in it for a long time.

Warily we shuffle across the wooden floor and sit down quietly on the narrow benches lined up against the walls. The room is high and bare. There is a bookcase with rows of blue-jacketed books and folded clothes, and hanging on two of the walls are rectangular slate boards, one of them chalked with mysterious letters and figures:

> PS. 112:4
> <br>
> R. 8+9

Wonder if they have something to do with us. Perhaps they are check-marks to be entered on our registration cards so that we can always be identified and traced.

High up on the walls small windows in cast-iron frames let in a little dim light. I look at the row of bent backs on either side of me. There is some shuffling of feet and a bout of hoarse coughing. Jan is sitting some distance from me. He doesn’t move but his eyes are following the lady, who is being excessively busy with the luggage. She shifts and rearranges the suitcases as she notes down on a sheet of paper how many have already been brought in and which ones belong to whom. From time to time she looks at us thoughtfully and bites her pencil.

She is sharing us out, I think to myself. I must go up and tel her that Jan and I belong together, that we’ve got to stay together.

‘You’ll be given something to eat in a minute.’ She points to a table where a pile of bread and butter is lying half-hidden under a tea cloth, beside a tin kettle.

‘As soon as you’ve eaten, you’ll be taken to your families. There is one for each of you here in the neighbourhood.’ What does she mean? All of us look stunned or half asleep.

She pushes two bags to one side and crosses something out on the list. ‘You are very lucky that these kind people are willing to take you in, so be on your best behaviour. They speak Frisian here, it’s hard to understand.’

She makes an attempt at a roguish laugh and pulls a childish face. ‘At first you’ll find you can’t make head or tail of it, I can’t understand half of it myself. But in a month’s time you’ll be speaking it really fluently, just you wait and see.’

We stare at her blankly, as if her words aren’t getting through to us. Why is she laughing, why are we having to sit here all this time?

I can see my suitcase, somewhere at the back, so it’s still there. But really I couldn’t care less, what does a suitcase matter?

Jan has already been given his food and is holding a mug of milk between his knees. He hasn’t spoken one word and looks straight through me, as if we had never known each other or even lived in the same street together. But I couldn’t care less about that any more.

A few men are standing round a table by the door. As they talk, they look at us and point or nod in our direction. Then one of them buries his nose in a writing-pad, as if he is doing a complicated multiplication sum.

One man points his finger at the paper while looking at us out of the corner of his eyes. Then he lifts the fingers of his other hand up, counting: one, two, three ... When it won’t work out, he starts all over again: one, two...

We eat our bread and butter in silence, peering over our mugs as we drink our milk.

The driver sits a bit further back on a chair. He has a small pile of bread in front of him and chews steadily, looking peevish and disgruntled.

I have a plan. I’ll go straight up to him and ask him if the lorry is going back again, and if I may go along too. No one will notice if I disappear. I’ll save my bread and butter and if I give it to him he’ll be sure to let me. I look around: what am I doing here in this stuffy, dark room? Why did I ever allow myself to be taken to the lorry? I should have run away while we were still in Amsterdam. I am filled with regrets.

I picture the driver suddenly, giving me a friendly smile as he takes me along with him to the lorry. Unnoticed, we’ll start it up and drive away. Escape!

But I know that I shall go on dutifully sitting here waiting to see what they will do with me. The driver stands up, talks to the men by the table and unexpectedly walks out. Too late. I’ll have to think up another plan...

The lady takes the first two children by the hand and leads them out like animals to the slaughter. Everyone left behind stares after them.

They have no luggage with them: so it must have been their suitcases which fell out of the lorry last night. Outside the door we can suddenly hear loud crying and the furiously scolding voice of the lady. I can feel all the children in the hall growing smaller, terrified at the sound.

The silence that follows has something threatening about it and there is a strange tension between the adults and the children. The men stand close together. They seem to be uniting against us. Are they likely to hurt us, can we trust them? They talk in hushed voices and not one of them has a smile. They look at each other worriedly and then at us, as if we are an insoluble problem.

Greetje from Bloedstraat is part of the next group to go. She smiles at me with the corners of her mouth turned down, lop-sided like the limp bow in her tangled hair. She makes an almost imperceptible movement with her hand: bye.

Next it’s Jan’s turn. When the lady calls out ‘Hogervorst’, he picks his case up firmly and walks to the door. I follow him with my eyes. ‘If we just stay together,’ Jan had said. Now he is leaving the hall without even sparing me a glance.

Slowly but steadily all the children disappear. I am the only one left behind, just like what always happens during games at school when the boys pick who is going to play in which eleven.

It doesn’t surprise me, I never get picked, it’s all part of the same misery. The men look at me from behind the table; the papers come out again; there is a brief discussion. The lady shrugs her shoulders and looks at her watch. ‘According to the list you should have been a girl,’ she says impatiently, walking over to me. “They’ve made a blunder somewhere as usual. Nothing we can do about it now...’

I pretend to understand, smiling vaguely.

When I am suddenly told to stand up I feel a blinding fear. Stiff-legged I go over to my suitcase and walk out of the door. All their eyes are on me and I have the feeling that everyone is breathing a sigh of relief.

Before I know what exactly is happening, I am sitting on the back of a man’s bicycle, too scared to hold on to his coat.

In my terror I have failed to take a last look at the lady and the driver, the last people known to me. All at once I feel I can’t do without them, now that I am being left alone with this silent man who is pedalling away with his back bent against the wind. We leave a village street behind and then bicycle along a road that curves through sloping pasture-land and past quiet farmhouses. Here and there cows or sheep huddle sullenly together, their shapes reflected in still ditches.

I am sure that even the cows know that I am a stranger here, because sometimes one of them lifts her head and stares after me with round, moist eyes.

Without warning, the man suddenly stops in the middle of the meadows and gets off.

‘It’s a fair stretch,’ he says. I can hear he is doing his best to speak distinctly so that I can understand. ‘We’re making for Laaxum, by the sea. You’ll be lodging with fisherfolk, good people. But I’ve got to go back first, we’ve left your suitcase at the Sunday school.’ I stand all alone on the road, feeling as if I’ve been dropped from the moon. Fields of grass all around me and not a soul in sight.

This is a trap, of course, the man will never come back. I’ve been left here to starve to death, they meant me to all along. And my father was in it as well, that’s for sure. They want to get rid of me.

The man had said, ‘You’ll be lodging with fisherfolk.’ That was wrong for a start. My father had told me over and over again that we would be boarding with farmers, farmers with sheep and stables, haystacks, goats and horses. Like in the books I’d read at school. Fisherfolk? I have a nightmare vision of a ramshackle wooden hut on a wide, wind-blown shore with two old people sitting silently, continually mending nets. I can’t stand fish. I can’t get it down my throat, I’d sooner starve or choke to death! What am I doing here, why am I having to put up with all this? Where is my mother, where is our safe little home? And where have they taken Jan? If I knew where he was we might at least try to escape together. Shall I run back and try to find the driver? If I hurry, the lorry may still be in the village.

A spark of hope. I start racing back down the road like one possessed. The silence roars in my ears with every step I take. I have the taste of blood in my throat.

A cow lifts her head and lows loudly and plaintively. In the distance the man is coming back on the bicycle, my suitcase dangling from the handlebars. When, panting, I stop running, he looks at me in surprise, but asks no questions. I’ve been caught out and feel a little ridiculous. Shamefaced, I get back onto the luggage carrier.

We bicycle on, a road without end.

‘See that dyke at the end of the road? That’s where we’re going.’

I poke my head out from behind the man, but quickly pull back when I get the wind full in my face. I’l be seeing it soon enough.

‘Off you get. This is Laaxum. We’re there.’

I take a look around and see empty, wind-swept countryside with little houses dotted about. Is this a village, this handful of tiny, scattered dwellings? The loneliness grips me and clamps my chest.

We climb over a wooden fence and wade through tall grass. A grazing horse takes a few snorting steps away from us and I quickly draw closer to the man. Another fence, and then to the little house on the left. No trees, no bushes, nothing. Emptiness.

At the back of the house is a stable-door, the upper half open.

‘Akkel’ calls the man. He steps out of his clogs and we go inside.

A large, heavily built woman in dark clothes, seemingly consisting of nothing but enormous round shapes, is bending over in a low room. When she draws herself up, the room looks too small for her. She has a strong face and immensely wide eyes. She walks to the table, drops into a chair and pins back a loose strand into her knot of hair. One of the Fates, as large as life, looking first at my escort and then at me and then bursting into loud and incredulous laughter.

‘Oh, heavens, it’s our war child.’ She looks me over from head to toe. ‘But we asked fora girl for our Pieke to play with.’ Her voice takes on an annoyed and threatening tone.

She stands up, filling the room with her bulk again, moving towards me: there is no escaping her.

‘Go and sit down there.’

I huddle into a chair by the window she points out to me.

Pasture-land outside, emptiness and wind. Inside it smells of food and burning wood. The man goes and sits down to talk at the table with the woman, conspirators speaking an incomprehensible language.

‘He goes to church, I take it?’ Suddenly the question is clearly comprehensible.

I say ‘yes’ quickly. A lie, but otherwise they might throw me out straight away. I’ll hand over the registration card with the ration coupons now. Maybe that’ll make her think a bit better of me. I feel in my pockets: empty. Frantically I look through all my clothes: it can’t be, I can’t have lost them?

The man gets up and holds out his hand to me. ‘They’Il look after you well here,’ he says, ‘as long as you eat up.’

Another familiar face going out of my life. I feel like excess ballast, shunted from one person to another.

The door rattles; I hear the man step into his clogs.

As he recedes into the distance the woman continues her conversation with him, shouting loudly over the fields as he disappears. Then the voices stop and all I can hear is clattering in the kitchen. Maybe the woman is never going to come back into the room again. Silence. A clock ticks behind me.

When she does come back in, my face is wet. She wipes my cheeks dry roughly with her apron, but her silence is friendly and considerate.

‘Ah, little one,’ she says putting my suitcase on the table, ‘are you tired? Would you like something to drink?’

I shake my head. She doesn’t seem cross any more that I’m not a girl, the ice has been broken.

She opens my suitcase. ‘Not very much,’ she says looking the contents over. But then she lifts up a towel and is full of admiration. ‘What a beauty.’ She holds it spread out in front of her. ‘All those colours. That must have cost a lot of money.’

I look at it: a little piece of home.

She sits down facing me by the other window and picks up a bowl of potatoes from the floor. ‘The others will be home soon, then we’ll eat.’ Her eyes bore into me.

The sound of the knife cutting through the potatoes and the ticking of the clock. The window-panes creak in the wind. I peer behind me: it’s only half past nine. Nervously, I take in the smells, the sounds and the shapes. Even time is different here, slow and dragging. Eternities seem to have gone by since yesterday.

My eyes fall shut and I wake in confusion as the woman drops a potato in the pan. I must stay awake, who knows what’ll happen to me otherwise...

A small wooden plate hangs on the wall facing me. ‘Where Love Abides the Lord is in Command.’ Love, I know that, and command as well. You give commands to dogs. Come. Sit. Down. The Lord must be God, of course, but...

‘How old are you?’ Again that inquiring look.

‘Eleven. I’ve just started the sixth year.’

‘Eleven. The same as our Meint. That’s good, the two of you
can do your homework together.’

She walks out of the room and I follow her obediently. Outside she pumps water into a pan and throws the potatoes in. Then she puts the pan on the kitchen-range in a shed fitted up as a cookhouse. She throws logs on to the fire and pokes it hard. Sparks fly out into the open.

‘Does your mother cook on a stove as well?’

I nod ‘yes’, afraid she’ll send me away otherwise. I’ve made up my mind to agree with everything she says.

In my mind’s eye I can see my mother in a summery, bright kitchen, the veranda door open and me playing outside.

The woman’s large round back moves about steadily while she sweeps the stone floor. She pushes me outside firmly because I am in her way. Shivering with cold, I lean against the little shed and look at the dyke that runs from end to end of the horizon. I can hear the sea: somewhere beyond it lies Amsterdam.

When the woman goes back into the house I hang back, then walk meekly after her. I duck into the chair by the window and wait. The clock ticks insistently. Slowly I doze off and give a start when I hear the sound of voices outside the door. Suddenly they hush.

A girl’s voice asks, ‘Is he in the room?’

I brace myself in the chair as the doorknob moves.

## 3

WE EAT POTATOES and meat. No greens. Two big pans stand on the table and the father does the serving up. Now and then somebody holds out a plate without saying a word, the big boy has had three helpings already.

I look around the circle. There are six children sitting at table, shoulder to shoulder, all of them fair, all of them sturdy and all of them silent. They eat hunched over forwards, as if it is hard, strenuous work. They have lost interest in me.

I feel hemmed in and small and try not to take up any room when making movements towards my plate. At strategic moments I take a bite quickly and as unobtrusively as possible, swallowing hurriedly. After a few bites my body feels tired and leaden, a wave of liquid rising up inside me and burning in my throat.

I clench my fingers around the edge of the chair and think of home.

<br>

More and more people had come clumping into the small room, first a boy and a girl my age, followed by a smaller girl with a limp. She had hobbled through the room, steadying herself against the table or the wall. Later a couple more came in who were older, Popke, a tall, weather-beaten boy — the only one to hold out his hand to me and to introduce himself — and a boisterous girl with sturdy breasts under her tight dress.

She had been talking noisily as she came into the room, but when she had seen me sitting there, she had suddenly fallen silent as if someone were sick, or dead.

They had all of them looked me over, a strange little boy in their house sitting there uncomfortably in a chair. After the first awkward silence, they started talking among themselves in undertones. Sometimes I could hear them stifle a laugh. When I glanced in their direction, one of the girls burst out laughing and ran quickly out of the room.

A hush did not fall until the father said commandingly, ‘That will do for now.’ He had come in on black wool-stockinged feet, wearing an old, too-short pair of trousers on his skinny body. He surveyed me with a small lop-sided smile in the corners of his mouth.

‘So you’ve come from the city to see for yourself how we get on here, eh? Well, we can show them a thing or two in Amsterdam, eh, boys?’ He looked around the room. ‘We’ll soon make a man out of him.’

He put a hand on my shoulder and then sat down facing me. Preparing for a new cross-examination, I thrust myself back in my chair as far as I could. The man bent over forward and rubbed his feet, one after the other, an agonised expression on his face. The lame girl went to her father’s side with an exaggerated show of affection. She placed her hand on his knee and leant her head against the back of his chair, looking at me curiously as she did so. I could see she was putting this on for my benefit.

‘That’s your new comrade, Pieke,’ her father had said. ‘Go and show him round the house, so that he’ll know his way about.’

But the two of us kept our distance and didn’t move. I tried to avoid meeting the girl’s eyes and looked uncertainly at the father. I could sense he was a gentle man and that he was taking me for what I was, without any fuss. His gestures were made with deliberation, it looked at times as if he were caressing the air. He sat very quietly, and his look in my direction was friendly and reassuring.

While he’s there, I thought, things will be a bit easier.

The girls came in with the plates and the cutlery and pushed everything about on the table, making a great deal of noise. The father put his hand on my shoulder and showed me to my place. ‘Between Popke and Meint. All the menfolk together.’ He looked round to see if everyone had sat down and said briefly, almost inaudibly, ‘Right.’

The scraping of chairs stopped. Stillness settled over the small room like a heavy blanket. The family sat with folded hands, heads bowed. I looked at the big woman who gave the impression of watching me even with her eyes closed. She made a strange movement with her mouth, as if her teeth had slipped forward a little. I quickly shut my eyes and took up the same posture as the others, peeping out of the corners of my eyes to see when the prayer was over. Suddenly there was 4 chorus of mumbled voices, ‘Bless, oh Lord, this food and drink. Amen.’

‘Don’t your people say grace before meals?’ the woman asked. I searched for an explanation that would ring true but the father answered for me.

‘People in the city are used to something else, eh boy? Aren’t they?’

He gave a brief laugh as though he had seen through me.

<br>

Potatoes with meat. I can’t remember how long it has been since we last ate meat in Amsterdam. And here you can have as much as you like, all you need to do is hold up your plate.

The meal goes by silently and quickly. Things I was never allowed to do at home seem quite all right here: they put their elbows on the table and bend their heads low over their plates. They watch me eat. I have difficulty getting the food down and have to swallow hard. I try to put down my fork without being noticed, but I can feel the woman’s eyes upon me. The smell of the food gives me a queasy feeling in my stomach. Now and then someone says a few words, then they go on eating in silence. I listen to the sound of the forks and the swallowing and chewing.

My stomach gives a spasm and before I can hold it back I belch violently. I feel myself turning red with embarrassment, but no one seems to pay any attention to my lapse. Only the woman halts her fork halfway up to her mouth and stares at me.

When everyone has finished eating, the father says ‘Diet,’ in the same brief tone. The girl in the tight dress gets up and takes a book from the small sideboard.

‘Moses commanded us a law, even the inheritance of the congregation of Jacob.’

The book lies open on her lap and she reads in a monotonous drone. I try to understand what she is saying. Every so often she reads a few words twice over, the second time altering the pitch of her voice and allowing the sound to die away. That means it’s the end of a sentence.

‘And he was king in Jeshurun, when the heads of the people and the tribes of Israel were gathered together ... ’

Her voice is high-pitched.

‘Were gathered together.’

Her voice falls and tails off.

During the recital, everyone seems to be dropping off into an after-dinner nap. The girl claps the Bible shut and puts it back on the sideboard matter-of-factly. The mother looks pleased and satisfied, even with me. When they all fold their hands again, I know that the meal is over. Suddenly I am hungry. Will we be getting a cooked meal tonight as well?

A shiny strip of sticky paper in twisted coils hangs over the table, strewn with little black lumps of flies. One of them is still buzzing, so violently that I can feel the vibrations all along my spine. Dear God, please let me go back home soon. Help them in Amsterdam and protect them. I shall do whatever You say. Do I have to pray for the fly as well?

The chairs are pushed back from the table and everybody gets up. What next? Something new, or back to my chair? Meint and the disabled girl leave the room. I feel a push from the father’s hand. ‘Go have a look outside, why don’t you.’

When I step out of the door the wind hits me with great force, taking my breath away and nearly blowing me over. I lean into the billowing wind, gasping for breath, and am driven two steps back.

The two children disappear around the corner and sit down on a small bench by the side of the house where the wind is less fierce. I trail after them awkwardly and try to think of something to say or to ask.

‘Behind that dyke,’ says the boy, ‘is the harbour. That’s where our ship is.’

He points. The girl makes room on the bench. Carefully I push in next to her. The long grass on the dyke is forming fanciful shapes in the wind, rippling like swirling water.

‘I’m Meint,’ says the boy and holds out his hand. ‘We are brothers now.’

Where the road runs up along the dyke, I can see several masts between the roof of a farmhouse and the top of a tree, bobbing to and fro in an ungainly dance. ‘Are you allowed to go sailing in the boat sometimes?’ I ask the question carefully, but the girl explodes into derisive laughter as if I had said something stupid.

Meint gives her a prod so that she tips over onto the grass. ‘She had polio, and now she’s got a bad leg. She isn’t used to city talk,’ he adds apologetically. The girl hobbles restlessly to and fro in front of us as if trying to wear herself out. A little bird in a cage.

<br>

Our house is the last in the small village. It stands apart from the other houses that lie at the foot of the dyke. Quite a long way past the village I can see how the dyke slopes gently upwards in a tapering hill, with cows grazing there, small as toys. ‘That’s the Cliff,’ says Meint.

The sound of clogs can be heard from behind the house. The father and the boy called Popke walk outside. ‘Meint, will you bring the bucket along for us?’ Meint jumps up and races to the shed. He swings the bucket through the air, then runs and catches up with the two men. The girl lets herself slide off the bench and limps wailing after them through the meadow. She comes to a halt at the gate which the boy has climbed over and kicks angrily at the bars. Then she lets herself fall down on the grass.

I see the woman in her dark clothes rushing across the meadow. Her size and the speed with which she is moving have something intimidating about them. She drags the girl to her feet and propels her back to the house. By the shed she gives the sobbing child a slap round the ears and shakes her furiously. ‘You scamp, always the same carry-on. Things can’t be the way you want them all the time.’ She thrusts the shrieking girl in my direction. ‘Whatever must this boy be thinking, he’s never heard the likes of it.’

The girl scrambles back up on the bench next to me. I notice she has two big new front teeth. On either side there are gaps and the rest of her teeth are brown and uncared for. She kicks against the bench and the iron brace around her thin little leg makes a clanking sound with each kick. With every clank I shut my eyes. What would my father be doing now in Amsterdam?

I feel misery closing in on all sides, in the emptiness, in the outbursts, in the aimless sitting and waiting: it rushes through the pastures, it howls and screeches, it disappears behind the dyke swinging a bucket. Suddenly I am dying to find out where the woman has put my suitcase, I long to open it and to hold the familiar things from home in my hands. I must stow all of it safely away so that no one can find it. But I don’t know whether I’m allowed back in the house. Should I go and ask?

The oldest girl is busy cleaning the pans in the shed. The mother is nowhere to be seen. I walk back to the gate. Is every day going to be like this one? I can smell the sickly sweet smell of manure blowing on the wind. Everything around me is green and remote. When the sun breaks through the clouds fora moment, I suddenly see lighter patches and the brown of the roofs, the wall of our house lit up bright yellow. The girl is sitting sulking on the bench, her head hunched between her shoulders, and she is making angry kicking movements with her leg in the air.

<br>

Towards evening the men come back, their hands reeking of fish and petrol. With a triumphant roar Meint puts the bucket down on the little stone path. Dozens of gleaming, grey slimy eels are writhing together in apparent panic. They have pointed heads and staring, cold little snakes’ eyes. I look spellbound at the squirming tangled ball.

<br>

Half an hour later the father holds a wildly wriggling eel between his fingers. With one deft movement he cuts off its head, as you would snip a flower from its stalk. He throws the suddenly severed body into a bucket of water where to my horror, it continues to move. The bloody stump of a head disappears into a newspaper.

I turn round and walk away, but a moment later I am back squatting by the bucket again, staring at the desperately writhing mass of pain.

<br>

I stand shivering in the little shed, waiting to wash in a tub the woman has filled at the pump. When I had got up in the morning, she had first inspected the cupboard-bed suspiciously, patting the mattress with the flat of her hand. I felt deeply ashamed.

On my first night here she had opened two small doors in the living room to reveal a cupboard-like space fitted out as a bed, all safe and snug. When I was getting undressed and had started to take off my underwear, she had objected. ‘No, we keep that on here, else it’s much too cold. Just put your pyjamas on over them.’ It had felt strange, going to bed with two layers of clothes, one on top of the other.

Meint and I slept in the same cupboard-bed. I shoved over as far as possible towards the wall so that there was room between us. The small doors were closed, leaving just a chink. The voices in the room had sounded far off and yet very near, as if someone were mumbling into my ear. I thought of home, submerged under the bedclothes, immersed in warmth and hush, in the grip of thoughts that kept me from sleep. And yet this had been the happiest time of the whole day: I didn’t have to say anything or see anybody. I lay tucked away safely in the dark and for the time being no one was going to come and take me away. This warm spot was where I belonged.

The three girls sleep in the cupboard-bed next to ours, the other one in the corner by the small window is for the parents. ‘You’ve got Popke’s place,’ Meint said, ‘he’s better off sleeping up in the loft. There’s more air up there.’

We listened to the voices in the room. Was Meint cross with me for taking his brother’s place? I tried to tel from the sound of the whispering voice which cracked hoarsely and huskily. ‘I’ve got to go to school tomorrow. You’re to come too, Hait says. We’ve only just started again.’

School... It had never occurred to me that I would have to go to school here as well, that the teaching, the testing, and the homework would go on as usual, unchanged, just like at home.

<br>

When the light in the room went out — the woman gave a noisy blow and I smelt the penetrating oil fumes that cut sharp as a knife through the dark — I could hear, besides Meint’s breathing and the creaking from the other cupboard-beds, the wind sucking along the walls of the house, a persistent, swelling, threatening noise. I was lying in a little boat that was being tossed about on strange seas, slipping through dark tunnels and tacking across unfathomable deeps, moving further and further away from the familiar mainland.

I woke up with a start in the night. It took a little time before I knew where I was. I had dreamt of home: my mother was sitting bent over forward in a corner of the room, her face buried in her hands. My father was standing by the window and I was suspended just outside, floating in the air, He was trying to get hold of me, but whenever his hands nearly touched me, I swerved away. ‘You must get in Frits’s car,’ he shouted, and I could see that ‘Frits’ was down below in front of the door, hanging out of the car window and gesturing upwards, grinning. My mother dashed out onto the balcony, hauled me in like a balloon on a string and wrapped her arms around me protectively. The two of us were crying, and my clothes gradually became soaked through...

I woke up with a start. A strange boy, breathing audibly as if short of air, was asleep next to me. It was cramped in the bed, the cupboard seemed filled with stale air: I had to get out or I would suffocate.

Through the chink between the small doors I could see a bit of the dark room, gleaming black and still. Something was wrong, but what? I touched my pyjama trousers and felt that they were wet. How was that possible, how could that have happened? My clothes were soaked, and feeling round me I discovered that the mattress, too, was damp. Without making a sound, I edged as close as possible to the wall to find a dry spot, and then cautiously, so as not to wake Meint, pushed my wet clothes down to my ankles.

Did you have to fold your hands when you wanted to pray, wouldn’t it work otherwise? I laced my damp fingers together: Please, God, let everything be dry by tomorrow.

<br>

But it isn’t dry next morning.

The woman turns down the bed while I stand guiltily by, one bare foot on the other ‘He’s wet his bed,’ she calls out in horror. She smells the sheets and a moment later walks out of the room, her arms full of bedding, a disgusted expression on her face. ‘Do you do that at home all the time? You really should have warned me. Your mother could have sent a note, couldn’t she?’

No, I did not wet my bed at home. I had done it once for quite along time, we had always kept it a secret, my mother and I, even from Daddy... But that must have been at least five years ago.

At breakfast I make myself as small as possible and I’m sure they are all giving me a wide berth, they are all disgusted with me. I get the feeling I shall never be able to put this right, that I have spoiled everything. Wetting the bed, not going to church, not praying aloud, losing my registration card — I really am a terrible failure.

‘What will become of you?’ my mother had often said when I came home with bad marks from school. ‘You’ll end up a pigswill man.’ Then I would take a good look at the stinking wagon that drove through our street at noon. Jute sacks hung down from the back of the cart, filled with something undefined and horrible, and oozing long dribbling sticky threads. The man who collected the baskets of potato peel wore pig’s wash-stained overalls and large rubber boots, as he sat, legs wide apart, on the box, slapping the skinny back of his horse with the reins. Would I be sitting up there next to him one day?

I wash my face over the tub, scrubbing desperately, but tears of humiliation keep coming.

For some reason or other I do not have to go to school that day. The woman is so annoyed that she doesn’t deign to glance in my direction, and Jantsje and Meint go off to their lessons without me. She tidies up the room in silence. I miss the father, but presumably he had gone very early out to sea.

I sit about on the chair by the window for a while, then trail aimlessly through the house. I am profoundly miserable. The morning seems as if it’s never coming to an end, but finally it is noon, everyone is back home again and there are large pans of food standing on the table. They all seem to have forgotten the bed-wetting, they seem even to have forgotten that I am here, they keep talking to each other and pay no attention to me. Right in the middle of the meal, I suddenly have to rush out to the w.c. behind the house. When Diet comes to see what has happened to me, she finds me slumped over sideways in the little wooden privy.

‘He’s got the runs,’ she says, leading me back inside. ‘He’d better lie down for a bit.’

From inside the cupboard-bed I can hear them muttering about me. ‘Townsfolk aren’t used to proper food any longer, they’ve hardly got anything left. His stomach has got to get used to it first.’ But the mother protests vociferously. She is convinced now that she’s been landed with a freak of a boy when what she’d asked for all along was a girl for her Pieke!

Feverish, I doze off.

## 4

IN THE MORNING the grass is silver and the dew makes my socks wet. Lifting my knees high, I walk through the meadow. Meint is standing by the gate, his hair standing up in unruly tufts, a crease from his pillow running across his cheek. It is a quarter past eight, my first school day.

‘You need clogs, man, shoes are no good around here.’ My shoes are shabby and down-at-heel, the wet leather speckled with grass seed. I stamp the seeds off on the road.

‘Aren’t you going to wave to Mem?’ asks the girl. I can see the woman standing behind the window and warily raise my hand to her. I know this twisting and turning road we’re on now. Two days ago, I passed along it on a bicycle, perched behind the stranger. A road of unending loneliness.

The farmhouses are large and self-contained, noble fortresses. Every so often the wind carries the smell of smouldering wood and the sound of voices from a stable. A woman walks through a farmyard and calls something to us in a piercing voice. Meint points out the cows looming up through veils of low-hanging mist, their backs suspended mysteriously, ghostlike above the ground.

During the walk, I have to stop a few times. My breakfast comes spurting out in slimy white clots that land back onto my clothes because of the wind. I bend over, with tears in my eyes, fretting with anxiety as Jantsje and Meint look on in amazement.

The school is still a long way off, more than half an hour’s walk. Sick to my stomach, I walk along the village street. We pass the church and suddenly I recognise a small structure.

‘The Sunday school,’ says Meint and Jantsje pulls a face. For a moment I look up hoping the lorry may still be there, tucked away in some corner hidden from view. Or maybe it will be coming back to deliver the next batch of children. I must keep an eye on this place, I mustn’t let any opportunity slip by.

There is a low building just past the crossroads with a small yard in front. Resignedly I follow Meint and Jantsje through the waiting children. There are curious glances and Meint looks proud: ‘He’s come from the city to live with us.’

The low building has just four classrooms, tall, stark places with grey-painted walls, no pictures or drawings, nothing. Plain and empty.

The windows start high up the walls and the window-sills are bare. It is as if everyone abandons this place as quickly as possible after school. I think of our White School in Amsterdam, the sun and the plants which the teacher carefully tends, pinching out the overblown flowers.

I stop at the door and watch as the schoolmaster comes in and walks up to the window. He tugs at a rope and a small window at the top swings open with a big bang. I catch my breath. He beckons me imperiously with a crooked finger and points to a seat at the back. In front of me I see Meint’s familiar head. There are some eight or ten children in the class, each with a desk to himself. It’s a strange school: between the two classrooms is a door that stays open so that the master can give lessons to two classes at once. I hear his voice through the door, and another window being swung open. When we pray — the master standing in the doorway, head bowed — the silence of the village floats in over us through the open windows. The class is taking dictation while I look on. There is a girl who isn’t doing anything either, I know her from the lorry. You can tell from her clothes that she’s from the city because her dress is colourful and gaudy in comparison with the other girls’. It’s just as if she and I were wearing our Sunday best for school. Now and then she gives me a reproachful look. I’d like to get to know her, but have no idea how to set about it. Should I give her some sort of sign perhaps?

The master delivers his lesson slowly and drowsily, there seems no ending to it. The incomprehensible sound of his voice makes me tired and I try to smother my yawns and pretend to be looking for something in the little locker under my desk.

We troop outside in small, silent groups. Break. There is no shoving, no shouting, no laughing. Everything is orderly and grown up.

We walk up and down the yard for a while, some walking with the master, others waiting patiently by the school wall until they can go back in.

There are no houses behind the school, you can look right across the fields as far as the sea-dyke. I can see the hilltop of the Cliff rising upwards. The bleak landscape, open and without secrets, the emptiness blowing in to meet you.

When we go back into the classroom Jan has suddenly, mysteriously, appeared out of nowhere. I leap up at my desk and try excitedly to attract his attention. Jan is my mainstay, the two of us together will be able to run away from here and get back home. And if we come across Greetje from Bloedstraat we can take her along too. I can just see it, three children roaming through the countryside in search of their home. Like in a book.

Jan is put at the desk in front of me. His self-assured eyes glance briefly in my direction, but there is no recognition in his look or pleasure at our unexpected reunion.

‘I think all the evacuees are here now,’ says the master. What has happened to the rest, I wonder, where have they got to? Swallowed up in the far reaches of these lonely parts? We have to write down our names and ages on a piece of paper and the names of the families who are putting us up. Without so much as glancing at them, the master puts the papers on his table and goes through into the other class.

To the amazement of the others in the room, Jan immediately turns around in his seat. He smiles at me and starts to talk. I shrink back and signal ‘sh!’ with my finger. We mustn’t draw attention to ourselves straight away. That could ruin our plans.

What a filthy walk it is to this place. I couldn’t find it at all at first. They won’t be seeing me here very often, believe me.’ He looks around the classroom. ‘Backward dump. What on earth do you think we’re going to learn here, nota lot, that’s for sure!’

I look at his insolent expression and jerkily-moving head. As he talks he puckers his freckled nose, and his tongue darts rapidly across his lips as if he is gulping something down.

‘I‘m on a big farm. It’s great, lots to do. They’ve got two small children. I’m going to ask them to let me skip school. I’d much sooner help with the animals.’ He gives a snort. ‘Where have they stuck you? Here, in the village?’ He pulls me over towards him and whispers in my ear, ‘I’m sure you could come and stay with me. I’ll ask them at home.’

The master appears in the door and glowers into the classroom. His eyes are dull and disapproving. ‘I can see three new faces,’ he says. ‘From the city, from Amsterdam. Perhaps you are all used to something else at school there, but here there is no talking during my absence. If you don’t understand something, you put up your hand. And I think...’ he seizes my collar and marches me to a desk at the very front, ‘I think it is better if you don’t sit too close together.’

His footsteps echo emphatically through the classroom. He draws the curtains to dim the sunlight which is streaming through the open window. ‘I take it you come from a Christian school?’

I make a movement with my head that I hope can mean ‘yes’ or ‘no’.

‘Where did we get to two days ago, Jochum?’

A boy with very short blond hair and dressed in blue overalls stands up. ‘Deutonomy, master.’

‘Good try, my boy. The Book of Deuteronomy. Moses’ last sermon.’ He reads outa piece from the book, keeps silent fora moment while he looks around the class, and says, ‘Moses is the Old Testament, Jesus the New. Which one of you can tell me the names of the Apostles? I ask because I would like to make willing apostles out of all of you, propagators of the Holy Word and of Our Lord’s Gospel.’

It is quiet in the classroom. I think of Jan, I want to turn around and look at his familiar face. The teacher’s finger points in my direction. ‘You over there, the new boy. What’s your name?’

‘Jeroen, sir.’

‘I’m no sir, we don’t have any sirs around here. I’m called master, so call me that in future, if you don’t mind.’ He looks for the paper on which I have written my name. ‘Oh, with the Vissers, in Laaxum,’ he reads out. ‘Lucky for you, boy, a fine family. Not so, Meint?’

Meint goes red in the face, his answer is hoarse.

‘Well, Jeroen, so you’ve been to a Christian school. Can you tel me the names of the Lord Jesus’ disciples then?’

In Amsterdam I went to Sunday school a few times round about Christmas time, because they used to give you some sweets and a small present. Sometimes I came home with a small coloured print that had a text written on the back. I had always stored such treasures away in a metal box for safekeeping. ‘Holy cards,’ Mummy had laughed, ‘we were given those too, once upon a time.’ I look around the classroom. The children are staring at me curiously, except for Jan who sits at his desk with a vacant smile, legs spread wide, his hands tightly gripping his bare knees.

I take a leap in the dark, as one might jump into the sea, nostrils pinched tightly together. ‘Joseph,’ I begin, for that is a name I remember clearly, ‘David, Moses, and Paul, uh...’ But that makes just four. Ought I to have added Jesus as well? I hear nervous sniggers and can see Meint looking down at the floor in embarrassment.

The master walks to the communicating door and says to the other class with a triumphant ring to his voice, ‘Jantsje, that new member of your family doesn’t seem to know very much. Can you tel him the names of the twelve apostles?’

From the other room, sounding hollow and as far away as the bottom of an echoing well, I can hear Jantsje’s light little voice smoothly reel off a list of names.

‘And who betrayed Jesus?’

‘Judas, master.’ A chorus of voices.

I hope that Jantsje and Meint won’t tell on me at home. I am terribly ashamed, my ears are burning. Judas, I ought to have known that name. And curse my luck, right in front of the class. Now I’ll always be picked on first, you wait and see.

The master stands in the doorway between the two classes. ‘Let us pray.’ Uncertainly I fold my hands under the desk. I can feel the master watching me closely. It’s as if I were telling a lie just by folding my hands.

‘Oh Lord Our God,’ I hear, ‘we thank You for this morning, in which You have once again allowed us to be together. We beseech You, oh Lord, to bless us, and our new classmates also. And we beseech You, Lord, to bless the families of these children, families who are suffering hunger and want, who are sick and dying for lack of food, lack of succour and lack of hope. Grant this, oh Lord.’

Behind me, I can hear stifled sobs and I become aware of an achingly desolate feeling breaking loose inside me.

‘And who are suffering the blackest and most bitter circumstances. Remember them, Lord, and lend them Your infinite, never-ceasing strength and succour.’

It is beating up inside me in waves, slamming through me with deafening booms. It will not be held back. In jolting heaves the despair erupts from my mouth, my eyes, my nose, retching waves of dribbling, snivelling sorrow. I am like some alien invalid, someone who no longer has control over his body and is in the throes of grotesque and humiliating convulsions.

I can hear my sorrow raging through the astonished silence of the classroom.

‘You two stay right where you are.’ I can tel that the master is speaking in my direction. ‘The rest of you may go.’

Now the master will give me a friendly and understanding talk, he will console me and tell me that it won’t be as bad as all that, that Amsterdam will be spared sickness, hunger and death. And he’ll forgive me for the apostles as well. The master stands right in front of me. In vain I wipe my nose on my sleeve but the snot keeps on coming.

‘You have just proved that you lack faith in the Lord,’ he says brusquely. He looks at the girl and at me as if there is something repugnant about us. ‘That was wicked of you, a bad example to your classmates. And it is also most ungrateful to the people here who have taken you into their homes so lovingly. If anything like this should ever happen again,’ he sticks his hands into his brown dustcoat and nods curtly, ‘then I shall feel obliged to talk to your foster parents about it.’ He gives an angry cough. ‘That’s all for now. But don’t you forget it.’

We slide out of our desks and disappear from the room. The girl’s reproachful gaze is red and tear-stained now. We say nothing to each other and walk across the playground and up the road without a word. I look for Jan.

Warmth seeps down between the branches of the trees, and I breathe in the strong summer smells: grass, dung and fat, well-fed cattle. The light is dazzling. Meint is standing at a corner waiting patiently for me. I run up to him with relief: the first sign of brotherhood!

## 5

Dear Mummy and Daddy,
 
Best wishes from Friesland. We got here all right and it was a nice journey. I’m fine, and being well looked after.

I am with a family with seven children.

Five of them are at home. The oldest daughter works on a farm near here. She lives there too, but sometimes she comes round here. She is very nice.

They have a little brother but he doesn’t live at home either. He had to be boarded out with another family when one of the girls here got polio. After that the family refused to let him come back home because they’d grown too fond of him. Isn’t that strange?

I eat a lot, plenty of everything. They want me to grow big and fat. Tonight we’re having a duck that got caught in Hait’s nets. Oh, I forgot, the father here is a fisherman. He goes out to sea every day in his boat.

I call the father and mother here Hait and Mem, that’s Frisian. Luckily I knew that from Afke’s Ten and other books I got out of the library. Frisian is a very difficult language. When they talk to each other I can’t understand a thing.

Jan Hogerforst lives around here too. There is a large farm and that’s where he works. I can see his farm from here, far away in the distance. We are good friends and often play together. I’m glad he lives close to me, because now I can visit him a lot. We talk about home. Jan says that Amsterdam is a long way from here, but that isn’t true because I looked it up in the atlas at school. If I got into the sea at Laaxum, all I’d need to do would be to swim across at an angle, and I’d be back home with you.

If it lasts for a very long time, the war that is, then we’ll escape from here. Jan has it all planned, how to get back to Amsterdam. That’ll be a surprise for you, won’t it?

Jan is the nicest boy I know, and so is Meint (my Frisian brother).

Jan hardly ever goes to school. I do. It’s a good half hour’s walk away, in a different village, and if we have to go back in the afternoon as well we walk nearly two hours a day.

School isn’t difficult. I can keep up with everything easily and we hardly ever get any homework. The master is very nice and is pleased with the way I’m getting on.

They use oil lamps here, and the water comes from a pump. There are sheep here as well and in the mornings before we go to school we have to clear up their droppings.

And I also have to make butter, in a bottle. You have to shake the milk for a very long time until it gets thick.

We have a rabbit and yesterday Meint and Jhad to take it in a sack to another rabbit. They had to be put together in a hutch and tomorrow we’ll fetch ours back.

I’ve lost my registration card, and the coupons as well. Is that going to get you into trouble?

The weather is fine and we often play outside. By the harbour is best. I help quite a lot with the nets as well. They stink of fish. Luckily we don’t get all that much fish to eat.

How are you? Did Mummy bring a lot of food back with her? I hope so. Just as soon as the war is over I’l be coming back to you.

I miss you.

Write soon. Bye for now, lots of love, Jeroen.

P.S. I sleep in my underwear, you have to here.

<br>

I seal the envelope carefully. I’ll post it tomorrow on my way to school.

I didn’t say anything about my little brother, nor about my wetting my bed.

## 6

I DON’T KNOW why, but Sundays are the hardest. On the one hand it’s all very nice: Sunday breaks the dull monotony of ordinary weekdays and is the only day when the constant bustle in the house lets up for a little bit, as if all of us need a chance to get our breath back.

But it is difficult to say whether or not that makes up for church and Sunday school. Some Sundays are downright awful, but on others I get the feeling that I am being nicely uplifted and helped to look at everything through different eyes. When that happens, I step out of the church service with a lovely feeling, as if wings were growing under my shirt: God is merciful and everything will turn out fine in the end, including even me!

At meals we all sit down together, and most weeks Trientsje, the daughter who works at the farm, comes back home on Saturday evening and spends the night. She is like a gentle, warm-hearted mother, smoothing away all the sharp corners of my life. I can feel her deep care and concern. Jeroen, have you had enough to eat?’ ‘Don’t fret, things at home are certain to be going well.’ ‘Meint, leave the boy in peace just for once!’

On Sunday morning we are all allowed to get up a little later. Hait stays in bed the longest of all, so we speak in hushed voices and tread cautiously through the room. The girls lay the table without a sound and cut the doughy wartime bread into crumbling, crustless slices untidily arranged ona shiny white plate. Mem brings a wooden board up from the cellar with a piece of cooked ham on it and a stone dish with a wet and sagging sheep’s cheese she has made herself.

Everyone is wearing their Sunday clothes, neatly creased and smelling of camphor. We are all scrubbed clean with slicked-down hair, spick and span, as if we were waiting to pose for a family photograph.

I am wearing Meint’s clothes because Mem says that the things I brought with me from Amsterdam are unsuitable for going to church. ‘Much too gaudy.’ I sniff at the sleeves: moth balls, sheep’s cheeses and God, all of them go inseparably and solemnly together, it is the smell of Sunday.

We wait around the table, my eyes travelling greedily over the mouthwatering display of food. My first dislike of fatty things has made way for a kind of gluttony. We wait for Hait, no one touching anything. He comes through the door, glossily shaved. Because he does not put his shirt on until just after breakfast, he is in his vest, his thin yet strong arms sticking out of the short sleeves. He closes the door with care and takes time to survey us all with a gentle smile. This is the first rite of Sunday, the return of the father to partake of our food with us.

‘Right, children, eat up now.’

With an approving look he watches his oldest daughter handing out the slices of bread and pouring tea from a grey enamel kettle. Grace has descended, it seems, I can feel it deep down inside me.

Mem sits with her arms folded across her heavy breasts. This is her free morning and she radiates contentment because of it. Every so often she shifts her false teeth about a little, her way of signalling that all’s well with her world and that she is allowing herself to drift away into pleasant reveries. Fascinated, I watch out for the moment when she pushes her lower teeth out, for then it looks just as if she were sticking her tongue out at us and smirking at the same time. Whenever she catches me spying on her like this she nods at me with her head on one side and closes her eyes for a moment. For all I can tell, she is winking at me.

That’s the best thing about Sunday, Mem’s transformation: she turns into a benevolent and uncomplaining mound of peacefulness from which all the furious weekday wrangling has slipped away.

After breakfast we go to church with Hait, the girls carrying hymn books, the boys walking in front, side by side. When we are in the road opposite the house, we wave to Mem who, large and dark, fills the whole of one of the windows and raises a languid arm, as if we were a ship that has put out to sea leaving the safe harbour behind.

In Hait’s company the walk to the village seems much shorter. He tells us stories about where he used to work and points out who lives where. If Pieke comes along, Jantsje pushes the disabled girl along on the bicycle next to Hait while he tells her stories and makes jokes.

We wait among the people in the church porch to be let in. I inhale the smell of their Sunday clothes and the eau de Cologne of the older women, trying to keep close to them for as long as possible because the smell reminds me of the time my grandma came to visit us in Amsterdam, when she would open her handbag and give me an acid drop.

Going into the church makes my heart and my throat throb, as if I am about to go on stage. To the low humming of the organ I walk up the aisle, my hands crossed reverently in front of my stomach. If, as I move past, I catch the eye of people already seated in their pews, I nod to them gravely and they nod graciously back, while the prelude-playing organ fils the church with a familiar sense of fellow-feeling. I have the impression that I have been singled out to do something heroic, God looking down on me from the heights and thinking, ‘Any minute now I shall work a miracle through him.’ Gooseflesh shoots across my arms and back and my neck tightens with suspense. We slip into one of the pews. There are small cushions lying about and at every place there is a black hymn book. I look up to see if I can spot Hait who had disappeared outside across the churchyard and round the back of the church. Trientsje opens her psalter for me and places it silently on my lap. She points with a stiff, proud finger to the words scribbled in the front:

‘For my daughter Trijntje. On her sixteenth birthday. Hait.’

At the back of the church the doors are closed noisily and the organ falls silent. I can hear the sound of the bells outside dying away, thinning out raggedly to a whisper. Through a small door at the front of the church a group of men walk inside in a row, one behind the other. Immediately the shuffling and coughing is hushed, which means that these men must be very important people. With a jolt of pride I see that Hait is with them. I watch him sit down in the pew beside the pulpit, next to the schoolmaster and the man who brought me to Laaxum on the bicycle.

‘Who are those men? What are they going to do?’

‘Elders,’ whispers Meint. ‘They’ll be collecting money in a minute.’

The shirt Meint is wearing is frayed at the edges and has been patched with pale blue material. He looks ill at ease in the buttoned-up shirt: instead of just his head he turns his whole body towards me as if his back hurts.

The minister has a young, unwrinkled face above a chalk-white pair of starched bands. He wears thin, gold-rimmed spectacles which he pushes up his nose with a finger. He has come in without my noticing, and stands there in his full, black garb like an apparition. Without looking at anyone he walks quickly in a half-circle around to the short flight of steps to the pulpit, places a small book next to the large open Bible and looks out over the congregation attentively.

I wonder if he has seen me, if he realises that I am new. Perhaps ministers know everything. I make myself small and cast my eyes down. Just don’t let him call me out of the pew.

‘Dear people, let us pray...’ All this praying has become torture for me: we pray before and after every meal, at school and before we go to bed. I entreat food and health for those at home, I implore for a letter from them, I invoke their deliverance from death. The most repellent and shocking images pass before my shut eyes, visions I conjure up and no longer know how to banish.

The minister reads out passages from the Bible and after that he holds forth, endlessly and incomprehensibly. Interminable, dreary boredom.

I look at the tall elongated windows through which I can see branches and a piece of the sky and a swallow that swoops twittering in through one window and out of the other. Do the others notice as well, do they see it, or am I the only one watching its nimble flight? Unerringly the little bird darts out through the narrow gap to cut through the blue sky, and I wait patiently to see through which window it will return.

‘Woe unto them! for they have gone in the way of Cain, and ran greedily after the error of Balaam for reward, and perished in the gainsaying of Core. These are spots in your feasts of charity, when they feast with you, feeding themselves without fear: clouds they are without water, carried about of winds; trees whose fruit with ereth, without fruit, twice dead, plucked up by the roots...’

Streams of disjointed words without meaning, pouring out unstoppably, phrases that baffle me and turns of speech that make me first giddy and then sleepy. How much longer will he go on, surely he can’t keep talking for ever? Clouds move by in the blue sky and the leaves on the branches are beginning to rustle. It won’t be long before it rains.

If I stare at the minister long enough perhaps that will make him stop, perhaps he’ll realise then that enough is enough. I glance at the faces all around me, weather-beaten, tired, rapt. They listen with a kindly, childlike attention that looks like wonder. Do they really understand it all, and is that because they have heard these words all their life?

They sing: dour, dragging melodies played through first by the organ, then fumbled through routinely by the congregation. Meint holds the text in the little book up to me and I try to sing along looking as natural as possible. Sometimes my voice goes suddenly in the wrong direction, an unexpectedly loud sound, an incongruous and blatant discord. I grope for the right tune, floundering helplessly among the notes, and in the end do nothing but move my lips industriously. Perhaps the minister will not hear my mistakes, though the master may well have told him about them already. Moses, Joseph, David...

With the next bit of the sermon I am suddenly back home in Amsterdam, looking stealthily at a book with an exciting illustration: a woman without clothes is sitting down and bending forward, her white flesh making fat, soft, curves. Behind her, two old men are standing with surly, furtive expressions. One holds his chin reflectively, the other places a surreptitious hand on the woman’s bare belly. ‘Susanna and the Two Elders’ it said underneath and I hear these very words in the sermon.

The men at the front of the church are called elders. How can that be? Could those men, and could Hait...? I can’t imagine that, him doing it to Mem, those bony hands on a round, bare belly. Why do they talk about that sort of thing in church?

At the end of the sermon the elders walk one behind the other down the central aisle. They stick out black pointed velvet bags on long handles and people put their hands into them. The men shake the little bags and the jingling of money rings through the church.

I put in the cents Mem gave us when we left home. At first I had thought I would keep the money, it would prove useful for our escape, but then God might have seen it and punished me. When my coins jingle I look up: You see, God, my money is inside.

The sticks move on through the church like scythes, making greedy, poking movements, fingers pointing at each successive victim. An ill-omened, avaricious dance of death.

I squeeze out of the church and breathe in the smell of the meadows.

<br>

When we are walking back home, back to Mem, back to dinner, my life seems happy, light and carefree. I see everything through brand new, clean-washed eyes, as if I were looking down from a cloud and watching everyone, myself included, walking down the road like small, contented insects.

When we leave the road and start to cross the meadows, I can already smell the food. Mem stands waiting for us by the door and as soon as we are inside she begins to move pans and dishes about. On Sunday her cooking is always a little bit special: there are gooseberries in a bowl, or cooked apples, and sometimes warm buttermilk porridge with syrup.

If it is buttermilk porridge then my Sunday is very nearly spoiled, the smell alone makes me feel sick. And if, following persistent urging, I take a mouthful, all my good resolutions carried away from the sermon vanish together with that spoonful of sour-sweet, slimy sludge.

‘Go on, eat up,’ says Mem, ‘it’s good for you, it’l make a man out of you.’

Heavy and full of food we go back to church in the afternoon. Most Sundays Mem comes along as well. It is the only day in the week she ever leaves Laaxum.

After the afternoon service, which seems even more incomprehensible than the one in the morning and spreads a sleepy boredom over us all, the children go on to Sunday school while the parents visit friends or promenade up and down the village street. Downcast, and with a bitter taste of helplessness in my throat, I sit in the gloomy, damp little building with the cast-iron windows. I look at the spot where I sat that first morning, at the cupboard with the folded clothes, the table where the driver sat. It is a classroom filled with the memory of the group of waiting children with troubled, grubby faces.

Weary, grumpy or resigned, we allow a fresh stream of religious learning to wash over us. All the while I keep thinking of Jan, of how he left this place without so much as a glance in my direction. Why did my father ever allow me to leave home, to end up with this lonely existence, stumbling about as I vainly try to find my feet? And how can I be sure that things won’t go on like this for evermore, that the war won’t continue for years and years?

I am filled with anger and resentment against my new home, against this life with its endless sanctimony concerning eternity, sin and redemption, trickling its way drop by drop, word by word, into my weary head. Dispirited and beleaguered I try not to let anything more filter through to me, try to cut myself off from the monotonous voice, the booming bellow, the echoing drone.

‘The panting hart, the chase eluded, thirsts as ne’er before for joy...’

<br>

When Pieke sees us coming back home she runs staggering across the field, her little arms flailing the air helplessly.

‘Just look at that,’ says Jantsje, ‘she can’t wait.’

Rebellion and impotence grate inside me. Bells keep ringing in my head, my eye sockets feel hollow and empty. I no longer want to live. Why do I have to go on with it all? Drained, I cross the road and watch Pieke, that maimed bit of life who, shamelessly disregarding the day of rest, is making her way towards us with flailing arms, distracted with joy.

## 7

THE RED CLIFF rises from the flat land like a strange growth. From Laaxum the path runs along in the lee of the sea dyke and then up the slope of the Cliff to the top. At the highest point you can look far across the countryside, you can see Laaxum and Scharl, where Jan lives. Much further towards the horizon, the steeple of Warns church sticks up, and to the left, where the dyke makes way fora mass of tiny roofs, masts and treetops, lies Stavoren. Everything seems small and still. The sea, which makes up the other half of the view, puts an abrupt end to the all but treeless stretches of pasture, the dyke creating a sharp divide between the green land and brownish-black water.

As you stand there, right on top of the rise, you are sometimes filled with a sudden feeling of freedom, a tingling of happiness and adventure. The sea breeze is clean and bright and everything appears well laid-out and clear-cut: roads, fences and ditches form regular patterns and connections, one leading naturally to the next.

That unforgettable moment when enlightenment seizes you only to vanish again just as inexplicably, the moment for Which you search as for a dream that has been swept away and yet is still present, locked away deep inside yourself.

<br>

It is coming up to the end of September but the weather is still warm and sultry. The walk from Laaxum to the Red Cliff has made us so hot that our faces are damp and sticky. We have been chasing each other along the way, shrieking with laughter, gasping for breath, up dyke, down dyke, and now we are clambering up the path feeling guilty: we are sure to be too late.

Jan is sitting waiting for us at the topmost point of the Cliff, looking unconcerned. I sense the annoyance behind his imperturbable air. ‘I thought you were never going to turn up,’ he says, looking past us over the landscape and yawning. ‘Do you lot still want to do something?’ He says ‘you lot’ but looks straight at me with a mixture of mockery and contempt. ‘I‘ve been sitting here for half an hour. There was work I could have been doing in the stables.’

Shamefaced, we say nothing, even Meint keeping his mouth shut.

Jan takes a few steps away from us and looks down the slope. I cringe: now he’s going to go away, a precious afternoon with Jan has been lost. ‘Let’s see if the water’s still nice. First one in,’ he calls out even while he is racing down.

Boisterously we career after him, Pieke sliding on her bottom, shrieking with laughter.

Jan stands waiting for us below. He has kicked off his clogs and now struggles quickly out of his trousers. Jantsje stops in her tracks and turns round to Pieke, who is trailing far behind. Our voices ring out clearly in the warm air. I sit down in the tall grass and look at Jan. He has a yellow spot on his underpants. The sea makes cool, alluring noises along the stones.

<br>

An hour later I watch them walking down the sloping path again. Their blonde hair sticks out in spikes and I can clearly see wet patches on their backs and bottoms. Pieke is hobbling wearily between Meint and Jantsje, wailing for the support of an arm no one gives her. In any case, they’re off home and I am finally alone with Jan.

‘Why don’t you all go back,’ I had suggested. ‘Jan wants to talk something over with me. About Amsterdam. We want to go back there one of these days.’ I had made it sound important on purpose, almost whispering the last few words, like a secret message. Then I had raced back down the Cliff, taking reckless giant’s leaps, uncontrolled, half falling over, the grass lashing my knees.

Jan sits motionless on a slab of stone, his face towards the sea. He swivels a length of rope through the air, but it is as if his arm doesn’t belong to him and is leading a circling life of its own. For a while we sit silently, side by side, Jan a few paces away from me. I am afraid to interrupt his swivelling game and wait patiently. The sun burns my shoulders and makes me feel drowsy and languid. I listen to the buzzing of insects flying from flower to flower. The water laps idly between the stones.

‘Take off your shirt, it’ll dry more quickly,’ says Jan.

He has put his own in the grass behind him. His bare swivelling arm is thin and wiry. Leaning back he tries to dig up some grass with his toes, then kicks the grass away in a small arc. Slowly he stands up and gives me a searching look. ‘Did you see Jantsje in her underwear?’

I pretend not to hear and fiddle with my toenails.

‘When she was wet you could see everything. Or were you too scared to look?’ He walks a bit further off and pees into the sea in a wide arc, legs far apart. ‘Did you see how far I got? That’s muscles, boy.’ He flexes his arm, shows me a small round bump and pinches it with obvious satisfaction.

All five of us had been swimming and Jan and I are now trying to get our soaked things dry before we go back home.

When I had run up the slope with the other three, I had put on my shirt, hoping that my vest would dry underneath all the same. I find it suddenly odd to expose myself half-dressed to his pitiless gaze. A little earlier, when he had been shooting through the water on nimble, supple, frog’s legs, Jan, spluttering and tossing his hair, had made fun of my skinny body. Jantsje and Meint had been splashing about clumsily at the water’s edge and I had stayed close to Pieke, who was clinging tightly to a big rock, letting herself down into the water little by little. She had shrieked each time she touched something unexpectedly with the soles of her feet, and needed our constant attention.

I had looked at Jan’s glistening arms and legs as he came up out of the water and waded dripping wet towards us. Every part of his slender body displayed burgeoning strength, and his bearing was self-assured. With surprise and envy I watched him acting like a much older boy, swaggering about as he demonstrated the backstroke, He waded closer still and blew snot out of a nostril with a practised air. ‘Come on,’ he said, ‘spindleshanks.’ He grabbed hold of me and tried to push me under the water, one knee placed firmly on my chest. I had come up half-choking and fled to the shore.

<br>

‘See that over there?’ Jan waves his rope in the direction of the sea where two thick round shapes in the distance are sticking above the surface of the water. ‘That’s the wheels of a plane. British. Shot down by the Jerries. The pilots are still inside, I’m not kidding.’

I turn my head away. We’d been swimming in that water only a quarter of an hour ago, and Meint had said, pointing to the two mysterious curves in the water, ‘Who dares swim out to that?’

No one had dared, of course. Not even Jan.

In my imagination I can see the two Englishmen sitting upside down in the water, still wearing their caps and mica goggles. They rock and sway about slackly with the current, like seaweed. One still clings to the joystick, his mouth hanging open while fish swim in and out. Their eyes in their goggles stare at the shore and watch our legs splashing exuberantly through the water...

<br>

Jan saunters back and turns his shirt over in the grass. ‘Must get a bit more of a tan.’ He drops down full length, his brown knees sticking out above the tall green grass, his eyes screwed up tight against the sun.

‘Lovely Sunday tomorrow,’ he chants. ‘Lovely day off tomorrow.’ He turns over onto his stomach and stays there, arms and legs stretched out wide. I feel envious; he never seems to think of Amsterdam any more and looks completely at home here. I want to talk to him about our street, about our friends, but I am scared to. He is sure to say something sarcastic and laugh at me.

Once, when I had gone over to Scharl to find out what was wrong with Jan who hadn’t been to school for a whole week, the woman I met in the stables called out ‘Jan, a visitor for you from Holland!’ Jan had come out dressed in dirty blue overalls and wearing muddy rubber boots. His hands were covered with cuts and scratches and he had a silly little cap on the back of his head that gave him a brash, old-for-his-years air. ‘Hullo. What are you doing here?’ He had given me a slightly incredulous look, as if he thought I was not quite right in the head.

‘Nothing. I was in the neighbourhood. So I thought to myself, why not see where Jan lives?’ I said it apologetically, as if I had done something that wouldn’t quite stand up to examination.

We were standing in the large, sunny farmyard, Jan plucking weeds out from between the stones with quick expertise, as if the yard belonged to him. Suddenly, I felt a townie all over again. What on earth was I doing here, what business did I have to be in this place? Jan had turned in to a different person, taciturn and grown-up. He stood there with his hands in his pockets, looking around the place like a real farmer.

In Laaxum I had kept thinking about Jan, the mysterious, inaccessible Jan, the boy from my street. We would have so much to tell each other when we met, about our parents, the friends from our street, about how rotten it was for us here...

‘D’you want to see around the stables?’

We had walked through the big doors and Jan busied himself raking some dry grass together with a pitchfork. Somewhere in a corner there was a goat, pulling impatiently at her rope, bleating like a crying child. Jan had squatted down to feel her udders.

‘Inflamed,’ he said, ‘that kid of hers bites the teat too hard.’

He scratched the animal lovingly between the horns and I felt a stab of jealousy.

For a while I stood there in the middle of the barn, totally redundant, looking on as Jan raked the fork to and fro, clouds of straw flying up in the air, ‘Well, so long then,’ I had said and Jan had raised a hand without looking up. ‘See you.’

‘Traitor,’ I had thought to myself as I walked outside, biting my lip hard. ‘Dirty, filthy traitor.’ I could have cried out with mortification.

<br>

Jan had stopped moving. I jump to my feet and race up the slope. The silence gives me a sense of great excitement: this is our afternoon. Jan is asleep and I am keeping watch, guarding him from danger.

At the top of the hill I look down at the sea which is covered with small white waves. Jan’s almost invisible body makes the shape of a cross in the grass. I let out a whoop, a shout like the ones we gave in the street in Amsterdam to call to one another in the evenings when we played cops and robbers or prisoners’ base. I see Jan raising his head.

‘Come on!’ I wave my arms and start leaping about like a madman. ‘Hey, hey, hey!’

He comes slowly up to the top, balancing with bare, outstretched arms. His body slanting forward and his head looking down, he takes huge steps.

‘Gosh, you’re so brown already.’

Jan wipes drops of sweat from his nose and lifts his arms into the air. Brown, scratched arms. He looks out over the yellow-green landscape, which he seems to be keeping at bay with a movement of his arm, like a general. He hitches up his sagging trousers and takes stock of the slope. ‘Let’s roll down.’ He gives me a playful shove. We had rolled down once before, but I had finished up at the bottom retching into the sea while the whole world seemed to spin in large circles around me.

I go and lie down at the edge of the slope. ‘Ready, steady... We’ll see who wins.’ I no longer care if it makes me sick, so long as we are together, so long as Jan plays with me.

‘Hang on, we’ll do it doubled, like this.’ He lies down on top of me and puts his arms around me. I can smell his acrid sweat. ‘Ready?’ He laughs and without any warning squeezes the breath out of me. Over we roll, slowly and lop-sidedly, then faster and faster. Over and over I see Jan’s face against the blue sky and then against the dark grass. Our bodies bump into each other and I hear Jan catching his breath with excited laughter. I shut my eyes tight and cling to him as we drop like a stone into an unfathomable deep.

Stop, I think to myself, stop! and then we are lying still. Grass rustles and grey circles spin around in my head. I break into a sweat, feeling Jan’s body pressing stiflingly and stickily against me. He pants heavily into my ear. ‘Jesus,’ he sighs, ‘sweet Jesus.’ Are we never going to move again? Jan sits himself up and pins my arms to the ground. ‘Wanna fight?’ He smiles menacingly, his panting mouth half-open. He has straight teeth and abroad, glistening lower lip. ‘Wanna fight?’

I know about that game all right. I try cautiously to free myself from his iron grip. I realise that movement by stealth is more effective than fierce resistance. His arms at full stretch, he stares at me triumphantly. I twist about in vain and am ashamed of my cowardliness, of the humiliation. I don’t want to fight, I want Jan as a friend. But I also know that I can only be his real friend if I take up his challenge.

‘Mercy’, says Jan, ‘ask for mercy. Else I won’t let you go.’

‘Come on,’ I plead, ‘we were having such fun.’

With a lightning-like movement I try to wriggle out from under him, but Jan jerks my legs apart with his knees and starts to rub his body against mine. His smile has disappeared and he now has a look of deep concentration as he makes impatient, insistent movements with his hips. He is frightening me. ‘Hey, Jan. Listen...’ He isn’t listening to me. I can see his face above mine, his teeth clenched firmly together and his eyes shut. He has clamped his fingers around my wrists as if he wants to force my hands off. Suddenly he rolls over and kneels next to me. He pulls his trousers down and a stiff little shape swishes up with a slap against his belly.

The two of us stare in silence at the pale thing, raised like a warning finger. I can see the white belly between Jan’s dropped trousers and his pulled-up vest, a white, vulnerable belly. Jan’s prick looks strange, hard and straight with a shiny red rim on top. I wonder if it hurts and give aloud, convulsive swallow.

‘They say you’ve got to push and pull.’ Jan has suddenly become communicative. ‘It works if you do that.’ He begins to tug violently at the swollen thing. I have the feeling that I have ceased to exist, and turn away. What is he doing, what is the matter with him? I feel sorry for him. Is he ill, does he often go on like this? I press my forehead against the ground and inhale the sourish smell of the grass. What exactly was he trying to do? ‘It works if you push and pull,’ the words shoot through my head. What works? He has a secret he won’t tel me, won’t share with me because he thinks I am too childish, because I piss in my bed. When I turn back to him, Jan is standing up, tucking his shirt into his trousers. He holds a hand out to me and pulls me to my feet. ‘Let’s go.’

We trudge up the Cliff. There is no sign of the secret — Jan is his normal, easy-going self.

‘Have you heard anything from home?’

I did get a letter from Amsterdam. Mummy was back, my father had written, and all was well. Was I having a good time, was I staying with nice people? The lady with whom I lived had written to say that I had wet my bed. How had that come about, when I hadn’t done that at home for such a long time? And best regards to Jan, it was nice that we lived so close together. We ought to be pleased that we were away in Friesland because there was hardly anything left to eat in Amsterdam.

But all I can think of is the mysterious event that has just taken place. I am dying to know more, to ask questions. But Jan has suddenly turned tired and surly, he seems to have forgotten the whole thing.

Above, on the road, he stops. ‘So long,’ he says, ‘I’l take a short cut here.’ He climbs over the fence beside the road and stands still on the other side, as if he were having second thoughts.

‘Come here,’ he says, beckoning brusquely. I walk over to the fence and Jan grabs hold of my throat with both hands. ‘Don’t you dare tell a soul that you’ve seen my prick,’ he whispers, ‘or you’ve had it.’ He pushes me back and runs away across the fields.

On the way back home I keep thinking of Jan’s vulnerable white belly and of the drowned airmen, suspended upside down in the water. Now and then I look around to see if Jan is still in sight. I feel like running after him. I have to protect him, make sure no one dares lay a hand on my friend. No one must ever get to know our secret.

A leaden stillness hangs over the meadows. The cows stand about listlessly on a bare, well-trodden patch where the farmer is about to milk them. It’s late, almost half past five. Hurriedly I race along the dyke.

## 8

THE MINISTER’S WIFE is dead. Mem raises her arms up to heaven with a hoarse cry when she hears the bad news — someone from Warns had come running straight across the fields to our house and banged on the window like one possessed — and then stumbles back into a chair where she sits gasping for breath.

‘Go and fetch Hait,’ she calls to us children, as we stand speechless around her chair. But Jantsje has already rushed off, over the fences and up the road. Her clogs fling lumps of mud up in the air and in her zeal she nearly falls over several times.

At the midday meal Hait prays aloud for the deceased — ‘our dearly beloved and respected late friend,’ he calls her — and for the minister as well.

I had never seen much of the minister’s wife, who had always remained a mysterious and admired stranger in the village. The minister lived across the street from the church in a stately house, a house with stiff white curtains that hung there daintily without creases or stains, and with two flowerless plants on the windowsill, one in front of each pane of glass, exactly in the middle. Behind, you suspected cool rooms, no doubt spotlessly tidy at all times and smelling of beeswax, with gleaming linoleum floors in which the furniture would be reflected.

I had seen the lady occasionally in the garden, cutting roses or raking the gravel. We would make a point when we saw her of walking close alongside the fence and greeting her loudly and insistently. If she greeted us back we would feel a little as if some Higher Being had wished us good morning. She rarely came to church, which I thought a bit strange, but then she probably spoke to the minister so much about God during the week that she had less need for church than the rest of us.

I had learned — thanks in part to her —to tel when people looked ‘townish’. The minister’s wife had been townish: always in proper shoes, always in a smart Sunday dress, and her hair — symmetrical little waves lying like a kind of lid over her head instead of the farmer’s wives’ knot more usual here — was always impeccable. She looked older than the minister, grander: sometimes I thought she could easily have been his mother rather than his wife. The minister would occasionally make a little joke with us and, although his hair was grey, his face looked younger, smooth and carefree, with quick eyes behind gold-framed spectacles.

In Amsterdam, in our street, twins were born not so long ago in Kareltje’s house, next door to Jan. Kareltje’s father was in the police. Two weeks later the twins were dead. ‘On account of the war,’ said my mother, ‘it’s all those rotten Jerries’ fault.’ Two men in black had carried the tiny white boxes out in their arms, with Kareltje and his father and mother the only people following behind.

Appalled, I had watched the mystifyingly sad spectacle, half-hidden behind the front door because I was afraid to be seen staring so shamelessly from the front steps. Dying, what was that?

<br>

The day the minister’s wife is buried we are let off school and nobody does any work. We wait by the low church wall until the cortège, black and threatening, approaches, slowly, to the sound of frenziedly chiming church bells.

All the women walking behind the coffin have long black pieces of cloth hanging down from their hats to their waists. We can see Mem in the cortège, wearing her black church clothes and a black straw hat with a veil. I can make out her face through the black cloth, dim and wan. She seems overcome with grief and doesn’t look at us. ‘Mem,’ says Meint and I hear awe in his voice, ‘our Mem.’ I nod respectfully, and swallow.

The minister looks straight at us, to our surprise. He even gives a brief nod in our direction and smiles. I cannot conceive of Mem changing back into the usual loudly chattering, bustling housewife in our overcrowded little living room. I fully expect this death to have turned her, too, in to a silent, motionless figure for good.

The children wait outside until the cortege is in the church, then we go in last and sit down at the very back. The church is so full that we can see almost nothing of what is happening at the front. ‘She’s next to the pulpit,’ says Popke, ‘there’s going to be a service first.’ I want to look for Hait and Mem, but am too ashamed to be seen peering around the church while she is lying there dead: what would she think of me?

When everyone stands up as the minister walks in, I look up tensely at the pulpit, unable to imagine what the minister is going to do next. What do you say when your wife has died? But a different minister gets up in the pulpit, a strange, ordinary-looking man who makes nervous gestures with jerky, disjointed waves of his arm. He keeps sipping water from a glass standing ready next to him. At one point he starts to cough convulsively, barking like a dog, and afterwards he looks out into the body of the church in consternation, as if thinking, What on earth was I saying just now?

I am disappointed, no, furious: what is this man doing in our minister’s place? Does he imagine he can take over his job, or that any of us are going to believe anything he might tel us? I watch him carefully. It looks as if his Bible is about to fall off the pulpit, and he can’t even preach. The lifeless, listless mumbling carries no further than the front pews.

Although I have never actually spoken to our minister, I have the feeling that we know each other well and that many things he says in church on a Sunday are meant, not for the whole village, but for me alone, to console me or to give me courage or simply to let me know that he understands me.

I would sometimes glow with pride and excitement when he looked in my direction during a sermon: you see, he’s making sure I’m there! During the hymns I often looked straight up at him to let him see that I was singing along at the top of my voice. I thought that the minister, just like myself, felt a bit of an outsider in the village, a duck out of water. Townish people are treated differently, they don’t really belong.

Sometimes I indulge in fantasies. I see myself rising suddenly out of my pew as the organ starts to play by itself, and all the swallows come flying in through the windows. Then I float up high in the church on beams of golden light, my arms spread wide. I am wearing flimsy white clothes that flutter in the wind and there are voices raised in songs of praise. I am drifting on these voices. They lift me up while the swallows circle about in glittering formation. Everyone in the church looks up in awe as I hover there between heaven and earth, and some people fall on their knees and stretch out their arms towards me. ‘That boy from the city, the one who’s staying with Wabe Visser — he isn’t just an ordinary boy, he is someone special in God’s eyes.’ The singing becomes louder and louder and I call down to them that they have no need to be afraid, that everything will turn out for the best. Filled with impatience I await the day when all these things will come to pass. Sometimes I feel that it is near at hand, and there I’ll be, up on high. But something always intervenes. No doubt some arrangements still have to be made. And when the time comes I shall first of all do something for my parents, and for Jan. But also for Hait and Mem, and for the minister’s wife. And the minister. That’s quite a lot of people. I have only to ask and things will change, will get better. God will see to that.

<br>

We never saw our minister again after the funeral. For a while we were saddled with that clumsy interloper, and Sundays become an affliction. I made the journeys to Warns with bad grace and on the way back home I would tear to pieces everything that we had been assailed with from the pulpit, every last why and wherefore.

Then another minister came, a long-winded, boring old man. Religion lost all its appeal, and there was no longer any question of floating on high in the church.

<br>

A small black and white object is drifting in the waves under a cloudy, threatening sky. Sometimes I can see it clearly, occasionally disappearing until I spot it again a bit further away, bobbing up and down in the swell. Armed with a stick I crouch on the shingle and wait patiently for the tide to bring it in.

It is a kitten, its bulging belly floating on the water like a balloon, its limbs and head drooping down. I try to fish it towards me, thrashing about in the sea with my stick. It must be rescued from the cold water, it must be taken to safety, it must not be allowed to go on endlessly rolling about in the water. Finally it is in reach and I lay it down at the water’s edge, a crumpled coat, limp little paws and a grinning vacant face. I go to look for a piece of wood to carry it to the small beach on the other side of the harbour.

Just beneath the shingle I dig a hole in the wet sand and place the kitten there on a little bed of grass. Miserably wrinkled and creased, it lies on its side, a pitiful little toy. I pray as the minister might have done, using his words and intonations.

When the hole has been filled in again, I go back home to fetch a small paper flower Mem keeps in a drawer, and place it on the grave. On this spot, unknown to anybody, I can hold acts of worship. From here I can send up my prayers for my mother and for the minister’s wife. I can make offerings: pieces of broken pottery, a peeled-off ten-cent postage stamp, the lid of an old slate pencil box. If I am kind to the kitten, my prayers are sure to be heard. That’s the way it is. Fora time, it absorbs my attention so much that even my memories of Jan and the afternoon on the Red Cliff become blurred.

## 9

IN THE MORNING when I look out of the window with sleepy eyes I can see that the cloud cover is beginning to break up in the distance. For days the sky has been covered with low, dark clouds, like a lid on top of a pan. I had been feeling dejected and listless: the daily journey to school had suddenly seemed an insurmountable obstacle course to us, and the house, so crammed with people and their doings, had felt too small and threatening as if everything had been conspiring to drive me into a corner and curb my freedom.

‘Now we’ve had it,’ Mem had said, stamping her clogs in the stone yard, ‘winter is coming.’

The land lies chill and washed clean under a leaden weight, pools of rainwater sparkling in the grass and the cows moving through the sodden pasture on hooves black with mud.

When we were getting up, Hait had said to me, “Today we’re going to take a sheep to Stavoren, in the boat. It looks as if it’s going to be a fine day, so if you want to come along, just tel Mem. You can skip school just this once.’

Besides, it’s Saturday, and if I do go along I won’t be missing anything that matters. Saturday mornings at school seem to be nothing more than preparation for the doldrums of our Sunday rest.

I try to keep my balance as I walk up the wobbly gangplank to the boat where, tied to the rail, the sheep already lies meekly waiting, panting heavily.

It is still quiet in the harbour, a few fishermen are busying themselves with buckets and flat wooden crates, their voices echoing along the quay. A bucket of water, emptied beside a boat, has the tumultuous sound of a plunging waterfall. Though I have been on the boat several times, I have never gone out to sea in it. Either the boat had already left, or I would be missing school, ‘And your parents wouldn’t want that,’ Hait would say teasingly. There was always something to keep me on land. I didn’t really mind, except that Meint kept teasing me with, ‘Never been out? You’ve honestly never been out to sea? Ha, what sort of a man are you?’

All four of us sit down in the protective shell of the clog-shaped boat as, making little plopping sounds, it slips out of the harbour. The water behind us is split open into gently undulating lines that grow wider and fainter the further away they get. Hait is at the tiller, hunched up and looking unexpectedly small, while Popke and Meint are busily engaged with a tangle of ropes and canvas.

I look out from the little seat at the back and note how, all of a sudden, it is possible to take in the village with a single glance and how, faster and faster, first the small houses and then the quay wall, the workshed and then the pier, are being sucked away from us.

Once out at sea, the boat starts to pitch in the waves. My stomach churns. I grip the edge of the boat tight and feel the first splash of water, slap, in my face. The quay wall is far away by now, too far away...

When we swing around towards Stavoren, Popke and Meint hoist the sails. They look like large brown wings filled by unruly powers, time and again catching the wind with violent smacks. The boat lists suddenly and I topple over, to be caught by the sure arm of Hait who looks on calmly from the tiller as we draw a swirling curve of foam through the water.

Shrieking gulls dive behind us, emerging from the surf with fish in their beaks. The harbour has become a tiny doll’s harbour. Small and brown — a stripe, no more — the wooden sea wall follows us up the coast for a while. The large basalt blocks protruding from the water look like toy building blocks, paltry playthings scattered along the coast. The boat rears up more violently and the sea flings yet more waves of ice-cold water over us. I catch my breath and with chilled fingers cling convulsively to the edge. When Hait looks at me I smile back politely, a petrified smile. My boat trip is an excursion into a world of violence, of thrust and counter-thrust, a fight with chilly, flapping demons who are consigning us to the depths of the sea with explosive salvos of laughter. I look back with longing at the safe land.

Already, I think, I want to go back already... Never been out to sea, what sort of a man am I? Gasping for breathI try to talk unconcernedly to Hait, but he pushes me out of the rear seat with a firm hand. ‘Go and sit in the middle and hold on tight. And look out for that sail up there!’

He shouts the last few words, pushing me down forcefully at the same time. With a deafening clatter and the creaking of ropes the boom slams over our heads. The boat almost ships water. Meint, who is standing opposite me, is suddenly high above me, a moment later sinking steeply into the depths with a gigantic swoop. It is as if a pump in my stomach were squeezing the contents down with all its might. Don’t think about it, it is sure to pass, this is fun, this is an adventure... I mustn’t be sick, or they will laugh at me. But I struggle for air and feel the pressure under my throat grow stronger.

I cling to the dripping edge of the boat and work my way towards the middle. There I sink safely to the bottom, my back against the side, my feet against the crate in which the catch is stored. The sheep is falling from one side of the deck to the other, floundering about on blundering legs. Meint calls to me and laughs into the wind with forced heartiness. I admire him, he moves with quick agility across the slippery deck and his spirits are high. Gamely he staggers towards the sheep and ties the rope tighter and shorter. The sheep does not bleat, falling patiently backwards and forwards, but I can see the panic in its unseeing, staring eyes. I try to outshout my own fear and call something back. I can hear my voice bawling and sounding false. I laugh noisily and feel cowardly and insincere.

So this is what sailing is like, having a good time at sea and not being at school. My body is ice-cold and my fingers are numb. A strongly rising wave of nausea and the realisation that I am powerless to do anything to stop it makes my jaws begin to chatter uncontrollably. Desperately, I clench my teeth. Don’t vomit, not now...

And we used to sing about sailing at school, ‘Sailor, sailor, we’ll go with thee-ee, out to sea-ea, out to sea...’

Popke kneels down beside me and points to the shore. With effort, reluctantly, I get up on my haunches and see something on the shoreline that looks like a small hill. I give him a questioning look. ‘The Red Cliff,’ Popke shouts in my ear, ‘don’t you recognise it?’ Another wave of water sweeps over me, making me duck. Was that miserable, paltry little hillock the Cliff where Jan and I played? Was that the happy, warm, green hill that you could see rising so majestically above the land from our house? The sails again catch the wind with the sharp crack of a detonation.

Meint comes leaping towards me and leans overboard by my side, staring into the water. Hait, too, bends over, looks, cuts down the engine, and beckons to me. I hear a weird scraping and scouring noise, as if an animal were scrabbling and gnawing at the bottom of the boat. We are making almost no headway, but are pitching crazily up and down. The engine sends up impotent little puffs of smoke that make me feel sick, filling my nose and throat with a bitter taste. The thudding under the boat grows more insistent. When I get up and lean overboard my knees feel limp and weak.

Right next to me I can make out two gigantic rubber shapes sticking up out of the waves like ghostly apparitions. I can touch them if I put my hand out. ‘The aeroplane,’ Meint shouts excitedly into the wind, ‘can you feel it, we’ve sailed right on top of it!’

He slaps the heavy rubber tyres with a smacking noise, once, twice, and then he can’t reach them any longer. Eyes open wide, I stare at the terrifying objects, phantoms from another, mysterious, world. Beneath me there are men hanging upside down. I feel I know them, that they are familiar beings, drowned friends. So close by, surrounded by fish and seaweed, hanging, swaying, in filmy, heaving veils of water. Their arms move forwards, reaching out, beckoning: a swirling dance of death.

A moment later the contents of my stomach gush out into the waves, in a broad, yellowish stream.

<br>

When we reach Stavoren Hait says sympathetically that I needn’t go back in the boat if I don’t want to: I can walk back, it isn’t all that far to Laaxum. But an hour later, with timorous resignation, I allow myself to be hauled back on board, go and lie down in a corner under some sails and hope for the best. Think of it: anything could have happened on the walk back. I could have lost my way, or something else might have gone wrong, then I could have said goodbye to my new home as well.

In Stavoren I had immediately spotted the three German soldiers. They were standing about in the middle of the quay, and though they didn’t stop anyone their look was alert and keen, as if they were trying to see through your clothes. Their presence brought Amsterdam and the war startlingly closer again. The grim threat of shining boots, drab green uniforms and voices that sounded cold and hard was something I had almost forgotten here in Friesland, but suddenly it all came back again, that haunted feeling of suspicion and fear and of things that had to be done on the quiet, unseen.

I had made myself as inconspicuous as possible at the harbour, and had in no way betrayed the fact that I was seasick. If the Jerries had noticed that I was no fisherman but a boy from Amsterdam, they might have picked me up straight away. I had the feeling that they had been waiting for me there in the harbour. Maybe they had found my registration card and knew my name and number and had been looking for me all this time...

<br>

The return trip passed amazingly quickly. I lay dozing under the canvas, half sick, being tossed about in an unresisting heap. Before I knew it, the wind dropped, a benign calm descended over the boat and all the tightness ebbed out of my stomach. The sound of the engine grew faint and the sails came rustling down. I crept out from under the canvas and felt the warm air caress my face. Close by, craggy and familiar, lay the quay wall. I stepped onto it — the hero returned from his first sea voyage — and felt the ground sway under my feet: the land, too, seemed to have turned to water. No doubt that’s what they call ‘sealegs’.

A fisherman came striding along, his clogs sounding hollow on the stone path: it was music, dear, familiar music! Back to Mem now, back quickly to the house standing firm
and foursquare in the meadow, to sit down once more at a table that does not move!

<br>

An hour later I am sitting in front of the house, a late, fading sun casting pale patches of light across the fields. I have a hymn book on my lap and am trying to learn a psalm by heart for Sunday school tomorrow.

All the time I have been here, all these weeks and months, all these Saturday evenings, I have hit on no better way of getting these incomprehensible passages into my head than reading the stupid verses methodically over and over again until they stick in my memory.

> ... My soul cleaveth unto the dust: quicken thou me according to thy word. Ihave declared my ways, and thou heardest me: teach me thy statutes, Make me to understand the way of thy precepts ...

Far away, almost directly opposite our house, hidden among clumps of trees, I can make out the roof of the farmhouse where Jan lives. In my mind’s eye I see the spacious, empty farmyard, now muddy and rain-washed, the tall stable doors, the living room with the kicked-off clogs outside the door. Jan is there, my saviour, the friend with whom I am going to escape. Does he sometimes look over this way as well, does he ever think of me?

I have already worked out the plan: one Sunday afternoon I’ll skip church and go and meet Jan by the harbour. I’l have been able to take some food from home because they’ll all be at the service. We’ll make good our escape, and we’ll have a headstart of several hours before they start to miss us. Jan said we could sail across in a boat, but I’ll talk him out of that idea. I’l never go out to sea again. Much better to make for the big dam, once we’re over that...

‘... You have declared my ways...’

Wrong.

‘... I have declared your ways ...’

Wrong again.

‘... I have declared my ways, and thou teachest me ...’

Dammit.

White rage wells up inside me, I’d like to smash the book against the wall or tear the barbed wire on the fence apart with my bare hands. Stinking, putrid life...

All the unfairness, all the uncertainty, all my sickness and fear, all the incomprehensible and unbearable longings, everything I yearn for that is unfulfilled ... Lousy, stinking mess.

‘... My soul cleaveth unto the dust: quicken thou me ...’ Jan, if only you were here, pushing my arms into the grass again or putting your hands around my throat; anything is better than this, this nothingness, this emptiness, this hopelessness.

‘... And thou heardest me: teach me thy statutes. Make me to understand the way of thy precepts.’

## 10

THREE DAYS AGO it happened to me for the first time. In bed at night, the only place and moment in the day when I can think about home, quietly and undisturbed, I turn to the wall and draw lines from me to Amsterdam, channels of communication between them and me. But the last few nights I have been thinking of Jan all the time. I try to push him away, but he stubbornly forces himself upon me, manipulating my thoughts like a tyrant, whether I want it or not. What bothers me most is that I do want it, that I am happy to give in to my fantasies. I touch my body, with shame and caution, as if someone were watching me.

This is me, this is my chest, my belly, these are my legs and the warmth I give off is mine. And the small, unseasoned twiglet that grows and stands up under my touch, the thing that seems anchored in my insides by deep roots, is that mine as well, do I have any influence over that swelling and that shuddering bulge?

I allow it to happen again and again, I examine it time after time. When I start out of my sleep at night, it is looming up ominously at the back of my vague dreams: I have it, too, and it is bad, it is a sin.

<br>

A dark, rainy afternoon. I had felt completely superfluous in the little house, imprisoned, caught in a trap. There had been no post for weeks, a fact that had burned a hole of uncertainty inside me. And though Hait had explained that the post in Holland was no longer working because everything had been closed down on account of the war, I had secretly kept looking out of the window every morning to see if anyone was bicycling up from Warns to bring us our post.

It was also pointless for me to write to Amsterdam, Mem had said, because no letters were arriving there either, a waste of paper and stamps. But I kept writing all the same, secretly.

I would take the envelopes from the small chest of drawers but I had no money to buy stamps. So I would drop the letters in the post box without stamps, having written the address in big fat writing across the envelope, on both sides, hoping that might help. Or else that my prayer might help, the one I uttered as the letter disappeared from my fingers into the dark slot: God, make them get it, please. If only You wish it, Thy will be done.

For a long time I obstinately stuck to the ritual of writing, putting letters in the box, and waiting hopefully for a reply.

That rainy afternoon I was convinced that a letter would be coming from Amsterdam: on the way to school I had seen three herons standing by the ditch, and that meant good luck, and the Bible passages we were told to read in class seemed to contain a hidden message too: despair not, salvation is nigh! In the village a farmer’s boy whistled a tune that resembled a song my mother always sang, I had recognised it with a start. That too was a hint, a sign meant for me alone.

But there was no letter. I searched everywhere in the room for that small white rectangle that had to be waiting for me on the chest of drawers, or on the mantelpiece. There was none, and I was too scared to ask Mem.

But something simply had to come. I couldn’t be left waiting and hoping and guessing like this for ever, surely they must realise at home that I was pining for some sign of life? What were they up to, then? Suddenly I rushed out of the house, telling Mem that I had forgotten something, that I had to go back to school.

On the road to Warns I turned off to the left and took the muddy side-road to Scharl, to Jan. The farmsteads looked gloomy in the bleak landscape, there were still a few lone sheep, but most of the stock had been taken from the meadows to spend the winter months in warm stables. All activity seemed reduced to the wind bending the trees and a wet dog yelping piercingly to be let in. I walked hurriedly, my eyes fixed on the path. I did nothing to avoid the pools of mud, but waded grimly through them, stamping my clogs into the water with wild delight and feeling the mud splash up my legs.

They had simply forgotten me, that’s what it was, that’s why there were no letters, they were glad to be rid of me. Now they could have a good time alone with my little brother, that was what they had wanted all along of course. I drowned the idea that something terrible, something irrevocable, might have happened to them under a wave of self-pity and baseless reproaches. But deep down I knew how unreasonable I was being and that there was nothing anybody could do about it: not my parents, not the people in Laaxum, not anybody. It was just the Jerries and the lousy war. But I was stuck with it, and that was that.

A farm labourer hurried across the road, squelching through yellow-brown puddles of manure, his boots sucked firmly down into the sludgy morass. ‘Weather’s a right shit!’ he shouted in my direction. ‘Not fit for dogs! Why don’t you get out of the rain?’

The land and the farmyards seemed to have turned into a swampy quagmire in a matter of days. I could feel rain-water seeping into my clogs and running down my face. I would it lick away, together with the snot from my nose. I was drowning in the dismal melancholy that came dripping out of the sky, drenching everything and everybody.

The family with whom Jan lived were busy in the stables, dragging buckets and noisy milk churns about. A small oil lamp was burning on the wall, and a little greyish light came through a low, arched window. The farmer himself was half-hidden under a cow, his cheek pressed against her loins as he grabbed at the pale pink udders, monstrously bloated with veins fatly swollen as if they were about to burst. I looked at his hands squeezing the cow’s dangling teats roughly and uncaringly. A white jet shot through his pumping fingers and frothed into the bucket.

‘Have you come to see Jan?’ The woman was suddenly standing behind me and I turned round, as if caught in the act. ‘He’s inside. Go and look in the house.’

The house lay to one side of the stables. I walked through a garden where red cabbage and leeks stood among a tangle of plants gone to seed. I spotted Jan through one of the windows. He was sitting at a table with his back towards me, his legs stretched out on a chair. I put my hand against the glass and peered inside. I was spying on a completely different way of life, one in which everything seemed matter-of-fact and calm, where a boy could stare into the fire glowing peacefully in the stove. I wondered if he was asleep.

Oh Jan, mysterious, strong Jan whom I wanted to see so desperately, for whom I longed. There he sat so close to me, almost within reach, but even now, a yard away from me, he seemed unattainable, many lives removed from me. Did I have the right to bother him with my tearful despair? Could I disturb his private, dark tranquillity?

I wanted to turn round and make off before I was seen. The window-pane rattled when I withdrew my hand and the boy in the room turned his head in surprise, squinting in my direction. I raised my hand and pulled a face. To my relief, I saw Jan’s face light up in a pleased smile.

The room swam in black shadows, the furniture floating on a deathly still, dark sea. In a corner an oil lamp made spluttering noises and from the stable adjoining the room came the subdued lowing of a cow, tormented and in pain. Occasionally there was a clanking like heavy chains being beaten together, and it seemed that Jan, motionless, was listening out for that sound.

Feeling my way with my feet I stepped into the dark room and moved towards the chair. ‘Don’t you have to work today? I thought you always helped?’

‘No, I’m not allowed to do anything for a few days.’ When Jan got up from the table I could see that he had a bad limp. His face was pale and his reddened eyes, strained and weary, had a troubled expression. His initial pleasure at seeing me seemed to have vanished. He made his way awkwardly across the room, overturned a cup on the windowsill with a clatter and looked out of the dark window.

I had wanted to look the place over, to see how he lived, what sort of things were part of his daily life, but I was afraid to. As if rooted to the spot I remained standing in the middle of the room and kept my eyes timidly on the sullen figure by the window.

‘Come on, let’s go upstairs, I’ll show you my den. Or would you rather stay here?’ He opened a door at the back and walked in front of me up a steep little flight of wooden stairs. The stable sounds were very close now through the plank wall and there was a sweetish scent of hay. Excitedly, I followed him up the blue-painted stairs. I was being admitted to the room in which my hero slept, where he dreamed, a world of mysterious deeds I had only been able to guess at in my imagination. It was sure to be a place of unlimited promise, of friendship, shared secrets, of silent touches and discreet meaningful glances.

The little room was small, more a partitioned corner of the attic, with almost the same dimensions as my cupboard-bed. Even so, I thought, it’s his own room. Here he can read, lie on his bed, here he can be alone. He doesn’t have to share anything with anybody.

Jan pointed to a small window which did not look outside but gave onto the hayloft. You could see the haystack, dug into on one side, and beyond it a corner of the pigsty. A heavy wooden beam ran in front of the window, covered with thick layers of grey cobwebs to which dark pieces of dead insects were attached, and the planks beyond were covered with a grotesque layer of bird shit.

‘Shall I show you what I’ve done?’ Jan tried to pull up a leg of his overalls but the material caught in a lump around his knee. Annoyed, he sat down on his bed and began to undo the top of his overalls, his finger tugging impatiently at the buttons.

This little room is like a nest in a tree, I thought, suspended between dark branches and protected by cobwebs. Even though you couldn’t look out you could feel it was getting late, you could hear the darkness closing in on us, caressing our limbs. In the gloom of the room I could see a pale torso emerging one shoulder at a time from out of the overalls and hanging there luminously, fragile and tender.

I felt myself grow hot with excitement: this was a hidden place that no one knew about. What was about to happen here was something I would discuss with no one else, a silent, wordless happening between Jan and me. I had the feeling that I was about to discover the real Jan, that he would strip off the layers of indifference before my very eyes, revealing at long last who he really was. I heard him get to his feet and saw him push the overalls down. What was he really up to, what did he mean to do? Was he going to do again what he had done at the Cliff, the bare belly and the sticking-up thing?

‘Come here, take a look at that. Closer.’ The pale shadow sank back onto the bed, the voice sounding hesitant, almost embarrassed.

I went up to him, my heart beginning to thump rapidly and chaotically, the blood throbbing violently against my throat. ‘I can’t see anything, it’s much too dark.’ I could hear how toneless and laboured my voice sounded. Jan stood up, his feet creaked across the floor. I peered at the movements of his white arms and suddenly a softly surging light flared up, casting a path in the darkness. ‘Bother it.’ The match went out. More creaking sounds and a moment later an oil lamp flickered.

From the dark Jan pulled a chair up to the table and sat down. ‘Look,’ he pointed to his leg, ‘look at that cut.’ I saw a gash, a dark line running almost all the way from his knee to the top of his leg. ‘My knife slipped. Really stupid ’He gave a muffled chuckle. ‘And it’s deep, too. I ought to be sitting with my leg up all the time.’

I crouched on the floor beside Jan and looked at the elongated, scabby wound and at the smooth, clean line of the legs in the knitted underwear that flopped loosely about his body. Jan sat bent forward, peering closely at the wound as if he were short-sighted. His fingers pushed and pressed along the dark stripe. Suddenly he placed his hand over it protectively, as if he were afraid that I might hurt him. ‘It’s red-hot, just like I’m boiling. It’s full of pus which has to come out. Have a feel, see how much it’s throbbing.’

I didn’t move. The wound was like a thin snake that might suddenly wriggle away at great speed.

‘Would you mind squeezing it out for me? Otherwise I’ll have to go to the doctor’s.’ He looked at me anxiously, I had never seen him so uncertain and afraid before. ‘Just have a go, I don’t care if it hurts.’ I edged a little closer and laid my hand warily on the white leg. I could feel the throb of the feverish skin under my awkward fingertips. I was unable to talk, to breathe, to swallow, frightened to death lest I made the wrong move and caused him pain.

‘Get on with it, then.’

Carefully I slid my hand across a soft, smooth surface until it came up against the crusty, rough, injury. ‘Ouch, careful.’ The leg moved and pushed into my chest. ‘Watch it, you hear?’ He pointed to a whitish little swelling at the edge of the scab. ‘Feel that? The pus is under there, that’s where you have to squeeze.’ He pushed one of his hands under his thigh so that I could have a better look.

A wild sensation shot up inside me, something I could not control, like a flame spreading with lightning speed. I wanted to press my face between those two white legs, push my hands up inside the loose underwear so that I could feel his body, breathe upon it, exorcise it. I wanted to absorb it, greedily and hungrily, to kiss it and caress it.

Dizzy with fear I gripped his knee tightly and bent my head, my tongue moving spasmodically in a dry mouth. My body was jerky and stiff. Horror-struck I felt it happen: like an insect breaking out of its tight cocoon with strange, compulsive thrusts and then freeing its wings. Something had burst within me. I stood up awkwardly and shifted my clothes, in a panic lest Jan noticed.

It roared inside me: I had it, too, I was having it too, just like Jan. It was like a sickness you could hide from nobody. Everyone could see it and would talk about it. I was betrayed, lost.

I pinched the skin on Jan’s leg between my fingers, that mouse-soft, butterfly-wing, tender warm skin. I kept pinching wildly and despairingly until I heard Jan say ‘Ow’, and tel me to squeeze with my fingers closer together.

I felt the wound break open as you might feel a gooseberry pop under your shoe with a sudden snap. Jan bent forward with his eyes screwed half-shut and wiped the wound with the sleeve of his overalls. I straightened up in the little room and tried to control my panting. ‘Bloody hell,’ I heard Jan say, ‘that hurt all right.’

I was already by the stairs, the doorknob in my hand. ‘I have to go.’ A moment later I had stumbled outside through the dark living-room, and was tearing home without stopping, to a house full of busy, warm and familiar people.

## 11

THE DAYS CRAWL by on hand and knees, drab and without prospect. Bewildered I sometimes look at the calendar and see how many days have gone by. Those mysterious rows of figures only make me despondent; one long day compressed into one little number, and there have been so many little numbers, and there are so many freshly mustered columns still waiting, drawn up in serried ranks.

I feel locked up in time. It’s been more than five months, too long to remember my home properly — sometimes I think with a shock, What did they really look like? How did Mummy laugh? — and too brief to feel properly at home in my new surroundings. I wander about in an illusive no-man’s-land, a kaleidoscope of chill, fluctuating forms. Sometimes the little figures jell and I can make out my mother’s face, but they fall apart at the slightest movement, shockingly, into everyday, tangible objects.

Who can say how much longer the war will go on — for months, for years or perhaps for the rest of my life, so that I shall never see my parents again, my friends, our street. Everything is too confused for me even to cry about, I just take things as they come, day after day, week after week.

‘Be a brave boy, now,’ my father had said to me that time when we packed my case, I had been kneeling to one side, looking on enthralled at the way all the bits and pieces for my journey were being fitted together: face-cloth, towel, a set of underwear, toothbrush, my last pair of scuffed shoes, the pyjamas Mummy had patched together from a discarded pair of my father’s...

‘Be a big boy, then you’ll please your Mummy a very great deal.’ My father had lifted my face by the chin and smoothed my hair. A big boy? I had certainly grown taller and sturdier. Mem sometimes seized me between her strong hands and fave me a Satisfied squeeze. ‘Soon they won’t believe their eyes, your father and mother! They’ll hardly recognise you any more!’

I had acknowledged her approval with a feeble smile. Soon? I thought. How soon? I certainly have grown, she is quite right there. I can’t get into most of the clothes I brought with me from Amsterdam, and instead have to wear Meint’s things quite a lot of the time, while he wears Popke’s castoffs. But really big? I still cry a lot, when no one is looking, and I often think of home, and of my mother. Mother’s darling, they always used to call me at school.

Sometimes I look over my old clothes, so many folded away memories. All stored away in the suitcase: the check shirts, the blue linen shorts, the plus fours ‘for best’ (‘Oh, no, you can’t possibly walk about in those here,’ Mem had laughed uproariously, and I had never worn them once), and the woollen cable-stitch sweater that my mother had made herself, just like the shirts.

‘Had made herself.’ I take the folded garments carefully in my hands and feel how smooth, how fine they are. I sniff at them because in the early days the smell used to take me straight back to the bedroom at home, to the wardrobe where our clothes used to hang and to the sweetish scent of Mummy’s slip and stockings. But even the smell of my clothes has faded or changed. Or can it be that I have forgotten what it smelled like at home?

The lorry near the Royal Palace, the nocturnal drive over the big dam, and above all the bicycle ride with my father through the deserted Rozengracht — all have been etched into my memory and are at the same time irretrievably dim and far away. Wiped out, it would seem, the way the master with a peremptory gesture passes a cloth over my slate at school: ‘That sum is wrong. Do it again.’ I have accepted the loss, I have never been brave enough to protest. All I have done is mourn.

I have buckled down, I have conformed. On Sundays I go to church twice, on Saturdays I learn the psalm for Sunday school and on Sundays the Bible text for school on Monday. Early in the morning I help Jantsje and Meint clean the sheep droppings from the meadow, I churn milk in bottles into butter, listen outwardly unmoved to the praying at school, help Hait sometimes to mend his nets and trudge about in clogs like a real country boy.

In short, I have become one of them, outwardly that is, because when no one is looking, when I feel I am not being watched, I often sit at the far end of the stone seawall and look out over the capricious sea, the driving, moving masses of rebellious grey. Then I hope that, some bright day, I might suddenly be able to see over to the other side, there, far away, on that clear, sharp line between sea and sky. Let all go well with them, dear God, I pray, please keep them alive. Make them think of me and please let them come soon, to take me away from here.

The winter months are overwhelmingly bleak, and we spend most of the time inside the small house. We are like animals in our lair, crowded together and taking shelter in the warmth.

A razor-sharp wind blows straight across the bare land and the stripped dykes. Rain lashes the hedges and ditches, snow piles up in spotless layers — it all forces us to live a futile, cramped existence. When we step shivering outside in the mornings the grass is like a crackling sheet underfoot and every step taken sends silvery needles of ice splintering apart with delicate little sounds. Winter crickets. Faces around steaming pans of food, climbing early to bed and the smoke of the blown-out oil lamp: Mem, regularly the last, sighing as she gets undressed.

Sometimes I blow a hole in the ice of the frozen windowpane and stare across the white fields to the dark smudge which I know is where Jan lives. In the spring we shall roll down the Cliff again, our bodies close and warm together. And then I shall ask him straight out — something I have wanted to ask him hundreds of times, rehearsed in every combination of phrase and tone, but never allowed to cross my lips — if I may see his belly again, if the two of us may get undressed together. He will pull down his trousers and I will be able to touch and hold him there. I feel a compulsive and inescapable longing to lay my head against his body and look at that stiff, painful swelling.

How strange, I think, I’ve become like an animal, like one of the cows here, or the sheep. Licking, sucking, biting, wolfing my food down, a ravenous, greedy beast. It doesn’t surprise me, and I am no longer upset by it either. Aloof and lonely, hidden away in my bolthole, I am becoming more aware of myself.

And so the winter crawls by, on hands and knees.

# Liberation

## 1

AND THEN, SUDDENLY, the days grow lighter and warmer, the land seems to brim over with blossom, birds and frogs and the breeze is laden with scents and sounds. Young calves scamper among the calmly grazing cows and spring in the air without warning as if taken by surprise at the touch of the sun on their skin.

Each day we make our way stolidly to school through the burgeoning landscape: there in the morning, back in the afternoon, our daily treadmill. We still go about in our shabby winter clothes despite the tumultuous renewal of life trumpeting its arrival everywhere around us. To us everything is as it has been, the men working the land and the women hoeing the weeds in the vegetable garden or stoking the fire in the stove. Though the land grows busier by the day, to our eyes everything looks unspeakably dull, and most of the time we walk silently along the road paying no attention to our surroundings. Occasionally there is a sudden brief remark or a laugh, and we career around after one another playing tag in a sudden outburst of energy.

It is a cool morning, the sky steel blue and almost cloudless. On the way to school Meint and I trudge along on different sides of the road while Jantsje walks purposefully in front. All three of us, in our own way, are wrapped in thought, reacting huffily to any intrusion by the others. Then, as we approach Warns, we notice that the monotonous everyday pattern seems to have been broken. Something unusual is going on in the village: there are villagers standing about everywhere along the road talking to each other and people have taken to the street whom you don’t normally see about on a weekday. Everyone is walking in the same direction towards the crossroads by the church, where they congregate in an excited huddle.

‘Bet you it’s something to do with the Germans,’ says Meint, ‘they’re up to their dirty tricks again, for sure.’

‘Betting is against the Bible.’ Jantsje gives her brother an injured look, but her voice sounds triumphant. ‘Hait won’t even let us say that, ‘‘betting’’. The Germans can bet, if they like, they’re heathens. But not us.’

I have become used to the rivalry between these two over who is going to have the last word. Meint sometimes strikes out at her, suddenly and feebly, but somehow or other Jantsje always seems to get the best of the battle. In any case she gives the impression that she is coolly and level-headedly putting our affairs to rights.

A good thirty people are milling about at the crossroads, talking excitedly, gesticulating vigorously, and moving from group to group. Some of the women look as if they have just rushed out of their houses, and everything suggests that something unusual is going on. When Meint and I walk up to the villagers Jantsje stays obstinately behind. She makes an attempt to keep me back as well, but I can sense that really she wants to come along too and see for herself what is happening.

From snatches of conversation we gather why so many people have come out on to the street: the Germans have blown up the bridge across the canal, at the end of the village; it’s kaput, gone, destroyed, all blown to smithereens!

We run back to Jantsje. ‘The Germans, what did I tell you? Come on, let’s go and have a look!’ Meint runs ahead of us. ‘We’ve got to see.’ I am torn between wanting to stay with Jantsje and running after him to the bridge. I have learned not to overreact to things but to let them take their course: whatever happens is bound to be a disappointment. But you don’t see a blown-up bridge every day. Reluctantly at first, then faster, I follow Meint.

‘Just so you know, I’m going on to school!’ shouts Jantsje, ‘and I’m going to tel Hait everything!’

I take my clogs in my hands and run exuberantly through the village street: something is really happening at last. More and more people are walking up the road, singly and in groups, in an obvious confusion which in many of them has turned to fear and uncertainty. I am aware that my rushing along is adding to the confusion, but, as far as I am concerned, the more commotion there is the better. The village is coming to life at last, everything is astir and on the move. I stop when I reach the next knot of people. Meint beckons frantically and seems to be giving a kind of victory dance. ‘The bridge has been blown up, but the Germans have gone, sent packing by the Americans, Really and truly!’ He talks in gasps, half shouting, and balls of spittle appear in the corners of his mouth. ‘They say that there are American cars over there, army cars. And as soon as the bridge is repaired they’ll be crossing over to this side of the village to liberate us!’

What shall we do, go on to the bridge or go to school? It seems silly to go back now. ‘Come on,’ says Meint, ‘let’s go and see if we’re too late for school. If everyone’s gone inside, then we’ll carry on to the bridge.’

But all the children are still standing in the small yard in front of the school and a boy comes out, leaves the doors wide open and shouts, ‘No school today, we’ve got the day off!’ His voice cracks with excitement, and this excitement spreads like wildfire across the yard. Everyone is suddenly moving, an ant-heap of shouting and curious children. Just as we are about to leave, the master calls us back; he stands solemnly on the steps in front of the open doors and slightly raises his arms.

‘Silence!’ Everyone stops talking. ‘The school will remain closed for the day, but be sure you all go home quietly now, all of you, and don’t hang about in the village. The bridge has been blown up by the enemy, but you must on no account go there, there may well be more shooting.’ He looks across the yard like a general, ‘We do not yet know what else the day may bring, but that...’ — he points to the other side of the street where a man has firmly planted a red, white and blue flag outside his house — ‘that is forbidden anyway. So be careful: the Germans haven’t gone yet.’

A Dutch flag in the garden! We crowd up to the fence to look at the man who is defying all the regulations so openly. ‘They’ve hopped it,’ the man laughs, ‘so we can do what we like again. Down with the Jerries!’ He spits contemptuously and I am shocked by his open disrespect.

‘Come on, we can do as we like as well. Let’s go and have a look.’ Meint grabs hold of us excitedly and pulls us up the road.

‘Mem,’ says Jantsje, ‘we have to tell Mem, and Hait. Someone has to go back.’ Both of them look at me.

‘Yes, you go, you don’t come from here after all. It’s our bridge.’

I feel the unfairness of what they are saying, I feel it deeply and woundingly and yet I can also see their point: the bridge is theirs, my life is in Amsterdam. And anyway, I’m too keyed up to protest and on top of that I don’t really know what I want right now: to be alone or to tag along with them to the bridge.

Without saying a word, I turn around and begin to run back home. On the way I yell like a madman to anyone who comes out of the isolated houses I pass, ‘The Americans are here, on the other side of the bridge, the Germans are beaten. Hey, hey, hey, we’ve been liberated!’

Before I know it I have reached Laaxum. I clamber over the fence, race across the meadow and find Mem and Pieke alone in the improbably quiet and brightly gleaming living-room. Panting, I lean against her chair. ‘Mem, the Germans have gone, there are American cars...’

Mem has sat down — her predictable reaction to any big news — and she looks at me with eyes wide open.

‘I know, my boy, I know. Hait has already been round to tell us. It’s some news, isn’t it? Pieke...’ She gropes for the girl and hugs her with rough affection, ‘Did you hear that, what Jeroen said? The Germans really have gone, he’s just been in the village. Jantsje and Meint haven’t gone to the bridge, have they?’ she adds suspiciously, her voice now carrying a threat in anticipation.

Ten minutes later I am running back, amazed that Mem hasn’t abandoned her work for the day, amazed too that she hasn’t come with me to Warns to see for herself what’s going on. But she had said that there might well be some more fighting — hadn’t I seen any guns? — and that I must go and find Jantsje and Meint at once and bring them straight home, or else they’d be sorry.

Fighting for air, arms flailing, my breath rasping painfully in my throat, I run back to the village, exhausted yet ecstatic. A few more flags have made a cautious appearance along the village street, like timidly raised hands. The sight of the road adorned with unexpected splashes of colour heightens my excitement. It is as if everything were happening for me alone, the flags, all the commotion and suspense. This is the final stage of my exile, the arrival of the soldiers means the end of my separation from my parents, they’ll be fetching me home soon!

After the grey sleep of winter this is the culmination for which we seem unwittingly to have been waiting. I feel I’m ready to burst, everything inside tingling and swirling. My feet are making strange, light clattering sounds under my body, beating out an irregular rhythm on the paving stones. One of my clogs, which has split, is held together with an iron band and makes a different noise on the road from the other, so that a little tune is sounding away in my ears: lib-er-at-ed, lib-er-at-ed...

There is nothing at the crossroads any longer. The junction of the three roads looks bright and still in the morning sun and it feels almost like a Sunday. I walk past the school, where is everybody? At the bridge, I find the answer: gathered there is the largest crowd I have ever seen, with the possible exception of the funeral of the minister’s wife. I work my way forward through the people and look for Jantsje and Meint.

Shoving against backs and wriggling past dirty overalls I suddenly see the bridge, the bridge that is no longer there.

Torn in half, the two ends are hanging in the water, a shocking and pathetic picture of violence in this slow, quiet landscape. Snapped-off railings, planks broken in two and twisted struts overturned in the water, it seems beyond repair, destroyed once and for all I look at it in dismay. The bridge is like a great animal, blown to pieces and crumpling as it dies. It will never be put right again, suddenly I know it, it can never be rebuilt. I have an immediate foreboding that the war may never come to an end.

I push in among the other children standing close together at the edge of the water, silent and dejected. Uncertainly, I stop still, afraid to move any further; my elation has made way for panic.

There is Meint, right in front. But where has Jantsje got to? I have to take them back home, they must be dragged out of here quickly, back to Laaxum, back to the stillness by the sea.

‘Have you seen the Americans? Over there, that’s them!’ A village boy pulls me forward and points, ‘Our liberators, now we’re free!’ He waves across to the other side and then cries out with a sob, ‘Long live Wilhelmina!’

<br>

A few green trucks have been parked on the other side of the canal and soldiers are walking about among them. There are also some odd-looking open cars, small and innocent like toys. Disappointed, I look at the peaceful scene, a scene that seems so unconnected with war, a handful of soldiers moving about calmly, almost lethargically, between the cars, carrying poles and piling moss-coloured bags onto the grass. They don’t appear to be giving us a thought, but just go about their business imperturbably, looking neither left nor right.

Armies as far as the horizon is what I had thought the word liberation meant, masses of soldiers carrying heavy guns, rifles and banners to the blare of bugles; an incalculable host of julibant, heroic fighters, muddy, tired and dirty, but still singing triumphant songs. The entry of heroes...

Two men get out of one of the trucks and begin to put up tents. One of them, who is stripped to the waist, walks to the edge of the water and waves: the first liberator to look at us! An awkward arm goes up stiffly here and there in answer to his greeting, but only the children cheer out loud and respond to the wave with exuberant leaps and yells.

‘Come on,’ I say, ‘we’ve got to get back, Mem is worried, she doesn’t want us to hang about here. It might still be dangerous.’

Just as well, I’m hungry,’ Meint says matter-of-factly. ‘Don’t those Americans eat? Where do they do their cooking?’

That question occupies us for most of our homeward journey: what and where do the soldiers eat, and where do they come from? ‘Americans come from England as well, of course,’ I maintain, because I have always been told that we would be liberated by the English. My father had always listened in secret to the English radio, so these men simply had to be English, and that was that.

‘Do they have to live in those little tents?’ Jantsje asks, and, ‘Will they be coming to liberate Laaxum as well?’

‘Liberate us? What from?’ asks Hait, shifting about impatiently at the table. ‘No one has ever set foot in Laaxum, they don’t even know it exists.’ He looks at us cheerfully. ‘Everything stays here the way it always has been, and just as well too.’

<br>

We are sitting side by side on the quay wall, captivated by the mysterious darkness of the water beneath us. A fish darts through a strip of sunlight, a brightly glittering star thrown off course. I follow the twisting movements, the almost complete halt and the flashing away at lightning speed, but my thoughts are on tomorrow, on the bridge, the soldiers and the fact that what is happening there is the liberation.

Mem had forbidden us to go back to the village: ‘There’s nothing for you there, it’s grown-ups’ business.’ It seems incredible that, half an hour away, something so important should be going on while we are hanging about the harbour dangling our legs with nothing to do.

Jantsje lifts her head, looks across the sea and points vaguely at the horizon. ‘Perhaps the Americans have crossed over already, and your mother and father are being liberated as well.’ I make no reply and stare at a school of minnows slipping from boat to boat. Carefully I work a small pebble to the edge of the quay and suddenly let it fly in their direction. There is a violent rush, as scores of fish vanish as one like magic in the shadow of a boat.

I had wanted to ask Hait a lot of questions over lunch, but for a long time I had been afraid of talking about home and the war, a fear that had eventually turned into glum resignation. The answers they gave me were always evasive: ‘How long will the war go on?’ ‘Young ‘un, we have no idea.’ ‘Is there any food left in Amsterdam?’ ‘Let’s just hope so, and don’t you pay any heed to what people are saying, they know no more than we do. One day they’ll come to fetch you, after all they brought you here.’ ‘When?’ ‘It depends on the war, and who knows how long that’s going on for?’

No letters and no news from home, for months now, only those vague non-committal evasions which were absolutely worthless and merely added to my confusion. This morning, when I had watched the soldiers across the canal, I had felt my courage ebb away: how could that handful of men possibly liberate all of us, they would never make it...

The school of minnows slips back along the hull of the boat and the little fish start to sip with small, fastidious mouths. Pieke, who keeps hobbling up and down along the quay wall, kicks my hand as I search for another pebble to throw. Whimpering, she cries that she wants to see the Americans, that she is always being left out and that she wants to go to the village right now.

Meint has clambered into the boat, and, getting down on his knees, begins teasingly to throw small sticks from the deck in our direction, ‘Why don’t you ask Mem,’ he mocks, ‘she’s sure to let you.’

Then, unexpectedly, Jantsje takes her little sister under her wing and says she’ll borrow a bicycle and take Pieke along to Warns. I go on sitting where I am, suddenly feeling sleepy and worn out, just wanting to curl up in the warm sun and to think of nothing at al. Still, it might be a good idea to go and look up Jan, he might not have heard anything yet about the soldiers by the canal.

I remember how the boys in Amsterdam always said to each other, ‘Just you wait, when the English get here they’ll finish Jerry off in no time.’ And now our liberators have turned out to be American, and not English at all! While I’m still feeling just a little triumphant, I’ll go over to Jan’s and tell him what’s happened, Jan, who always knows everything. He won’t know what’s hit him this time though!

But I am afraid to go to Scharl. Imagine if the woman there says that Jan left some time ago, that he’s gone back home and hadn’t told me. I would be all on my own then, deserted, betrayed. Whenever Jan doesn’t turn up at school for a little while the thought creeps over me, dimly at first and then more and more insistently until the shattering truth dawns: Jan has gone, I am all alone... Then I can think of nothing else, my longing for Jan assumes frightening proportions, and I am given over, heart and soul, to homesickness.

‘Hey,’ says Jantsje, bicycling over the dyke with Pieke on the luggage carrier, ‘we’re off, Jeroen.’

‘Wait for me.’ I slip into my clogs and run up the path. Jantsje has already bicycled away and I run after the girls at a jog-trot.

<br>

A miracle has happened by the blown-up bridge. A narrow gangway of brand new planks is in place across the water. Three soldiers are working on it, hanging down over the canal, sawing and knocking in nails with loud, determined hammer blows. Behind the army trucks small green tents have sprung up right across the fields, and a kind of storage depot has risen next to the largest of them, with boxes and tins and a mountain of kit bags half-covered with a tarpaulin.

Close by, on a bench, a few men are having a relaxed conversation, so relaxed one might think they were enjoying a Saturday afternoon chat in their front garden. And a bit further on, to our amazement, a couple of them are swimming in the canal, laughing and shouting like small boys, disappearing under the surface, now and then sticking their legs up to the sky and spitting water out of their mouths in small arcs when they come up for air. Large swathes have been cut through the duckweed, showing where the swimmers have been.

One of them climbs out on to the bank, hoisting himself up between the reeds, and as he stands up I realise with a shock what Jantsje is saying out aloud — ‘See that, it looks as if he’s got nothing on at all.’ Suddenly silent and sullen, the girls turn their heads away and I, too, take a startled step backwards. I can’t tell if the soldier is actually naked, but it’s a strange enough sight anyway to see grown men being so playful and high-spirited, trying to push one another into the water, giving each other bear hugs, wrestling and kicking out at one another with loud yells, tearing exultantly through the water like young animals, as if neither war nor liberation meant anything.

Jantsje walks back to the bridge and I push the bike with Pieke behind her, hoping she doesn’t hang about there too long; it would be just as well not to get too close to those soldiers. I can see Pieke peeping secretly at the swimmers and suddenly I feel cross. What is happening over there is nothing to do with her; Mem was quite right, it’s grown-ups’ business.

One of the soldiers on the bridge is singing and the hammer blows sound loud and clear. Someone puts down a tin from which they take sandwiches. The war seems a peaceful and friendly affair.

I feel ashamed as I catch myself peeking at the swimming soldiers, just what I wanted to stop Pieke from doing. What is it that fascinates me so much about them, why does my mind keep wandering back to them, and why does that make me feel ashamed? The swimmers have wrapped towels around themselves, and tilt their heads to shake the water out of their ears. One of them does a handstand in a free and fluent movement, poised athletically like a fairground showman, a strange contrast to this sober landscape.

‘Come on, we have to get back,’ says Jantsje, taking the handlebars from me, ‘or Mem’ll realise we’ve gone.’

Then to my own surprise I take an unusual decision and suddenly give voice to my own will. ‘You two go home if you like, but I’m going to stay here a bit longer.’

Unexpectedly, Jantsje raises no objections, gets onto the bike and pedals off without sparing me another glance. As she rides away I hesitate; Mem will know now and be cross. Behind me I can hear the singing voice and the rhythmical tapping of hammers, an enticing new world. I drop into the grass next to a couple of village boys, put my arms around my knees and, avid-eyed, settle down to watch.

The swimmers are standing by the tents, one is combing his hair in a small mirror, the others are drying themselves. Now that they are closer I can see that they are not much older than Popke, eighteen or nineteen. How do you teach people so young to fire a rifle, how can such youngsters make war? Can they really have beaten the Germans, these playful, boyish swimmers? But they are here in Friesland; they’ve got this far, anyway. I look at all the little bursts of activity in the camp with growing interest, at the toiling men, at the comings and goings of cars, at an oilcloth being spread out on the ground and one of the boys sitting in front of a tent and cleaning his rifle. Imagine if these soldiers were going on to Amsterdam and they offered me a lift, and we fetched Jan and we sat together in the back of one of those cars and we drove right through villages and polders straight on to Amsterdam...

I am startled out of my daydream by a noise that makes all of us on the bank leap simultaneously to our feet. An armoured car is driving out of the village street, making straight for us, hooting loudly for our attention, solid and outlandish as an elephant.

It stops at the canal with its engine growling. A soldier beside the driver leans out of the window and calls something to the men on the bridge in an incomprehensible language.

A curious crowd has poured out of the village to take a closer look at the car and the soldiers, a large circle forming around the unwieldy monster, at a safe distance not only because it might easily start firing at us from all sides but also because the people feel uneasy about the foreign language and the unfamiliar behaviour of the men. When the soldier beside the driver opens the door and steps out, everyone falls respectfully silent. Then there is a crush and the circle closes in.

I take a few steps back because the craning and the jostling scare me. I’d really much rather go back home, now that everything is getting so much closer and more real. I remember Mem and the clear warning she gave us.

As I try to break away from the circle someone puts an arm around my chest from behind and pulls me back hard. Popke, I think, or Hait. Now I’m for it... Another arm is thrust out past my head, an arm I do not know, strong and tanned, with a down of blond hair and a watch on the wrist. Between the fingers is a little oblong packet wrapped in blue and red paper with a thin silver edge.

I stand there in a state of terror, numb and cowering, staring at the hand: what does it all mean, what is happening, who is behind me holding me so tight? The people around me move back a little and look on curiously, some laughing as if it is all a great joke. I should have gone back with Jantsje, now I am all on my own with no one to help me. I am taken by the shoulders and turned around. Behind me a soldier is squatting and looking straight at me with piercing grey eyes. He has rough, cracked lips that are open in a smile and the corner of one of his front teeth has been broken off, Close-cropped, sandy hair with a bit of a curl, a white undershirt and a small chain around a powerful straight neck; I take all this in with one glance, an intense moment of guarded acknowledgment.

I try and pull away. Leave me alone, I think, let go of me.

It’s all up with me now, I’m for it. Fear wells up and floods through me, I ought to start yelling and kicking. But I remain standing stock-still, clasped in his arms.

He seems surprised and pats me gently on the head, takes my hand and presses the oblong packet into it. His hand enclosing my fingers is warm and reassuring. ‘For you,’ he says in English.

I hear the words, sounds that mean nothing, and I turn my head away, tongue-tied. What should I do? Why are they all staring and why does nobody do anything? The soldier gets up from his haunches, his voice still friendly and gentle, but making me dizzy with fear. I hold the hand with the little packet in it as far away from me as possible and see that all the boys around me are looking at it. But that hand does not belong to me, I have nothing to do with it, it’s just a mistake, surely they realise that?

The soldier walks over to the car and leans through the window; there is a fresh burst of hooting and an arm waves from inside.

‘He’s hooting for you.’ The boys push me forward. ‘We can go and look at the car, hurry up!’

The soldier lifts me on to the running-board. I can feel two strong hands against my ribs pushing me forward awkwardly, as if trying to squeeze me through the door. In the car, behind the wheel, sits a black soldier, a Negro with shining dark eyes and a big, happy-go-lucky smile. A narrow cap is perched on his frizzy hair, the point pulled down in front to just over his eyebrows.

‘Hello!’ He almost sings the word, in one long soothing, drawing-out of breath. ‘Hello, mister.’ He puts out his hand but I don’t take it. ‘What?’ He pats my cheek, then I watch his hand sliding along the inside of the car and pointing things out with a brief tap of his fingernails: knobs, round glass panels, little handles. He is smoking a stream-lined, snow-white cigarette — quite unlike the crumpled butts that Hait smokes — and puffs the smoke with an emphatic noise out of the window. Every so often he gives a high-pitched, childish laugh and gestures to a soldier on the bridge. The soldier who is holding me steps up on to the running-board behind me and bends over into the car, pushing me tightly against the door, pressing my throat against the top of the rolled-down window. Above my head a cigarette is being lit. I hear them talk and feel the painful weight of his body burning against my back. His voice reverberates through me, as if I were a sounding-board.

I’ll wait without moving until this ordeal is over, it can’t go on for ever. He coughs and my body shakes with him. He has placed a hand next to me on the window and I look at his short broken nails.

Before I know what has happened, I am suddenly back on the ground next to the car and the boys are pushing me out of the way to have their turn on the running-board. In the ensuing confusion I make good my escape as quickly and unobtrusively as possible through to the back of the circle. The soldier looks round for me, but when he spots me and starts to move in my direction I take to my heels. Quick, I think to myself, back to Laaxum, back home! Halfway to Laaxum I slow down a bit, the oblong packet still in my sweaty hand. Dispassionately, I examine it, as if it were something dangerous. The blue and red coloured wrapper contains thin strips neatly packed in silver paper. They have a sharp pepperminty smell. I peel back the silver paper and find a soft pinkish-grey slab inside which bends easily and seems to be lightly dusted with powder. It is a small, delicate miracle of precision and smell, streamlined and perfect.

As I chew the little slab it turns into an elastic, pliable little ball that I can work between my teeth and mould with thrusts of my tongue. The more I allow it to wander slowly through my mouth, tasting, testing and savouring it, enjoying its sweetness, the more it is brought home to me: I was given this present by an American soldier, this little packet is for me alone, he picked me out specially...

They will hardly believe me at home, it all seems like an incredible adventure story, a voyage of discovery through strange lands: the car, the black soldier, the packet pressed into my hand... I forget how passive and terrified I had been, how, paralysed with fear, I had just allowed it all to happen, and how I had run away as fast as I could, What remains is the memory of a glorious adventure.

I squeeze the other little slabs carefully back into the wrapper and push it deep into my pocket, my hand covering it protectively. I am no longer so sure that I shall tell them about it at home.

## 2

NEXT DAY A surprise is waiting for us at school: the only thing we have to do is to write out the words of the Frisian and Dutch national anthems because, the master tells us, we shall be singing them at the liberation celebrations. Using exaggerated flourishes, he writes the words on two separate blackboards and our two classes, joined into one for the occasion, dedicatedly copy his ornamental letters into our exercise books.

> William of Nassau, I,
>
> a prince of German blood...

One of the anthems begins. How can we sing that now: ‘a prince of German blood’? Shouldn’t we be going and wiping out all the Germans, now that the war is over? I had taken it for granted that we would all be marching straight into Germany with pitchforks, sticks and rifles to give them a really good hiding in their own rotten country.

‘Anyone finished copying may go home, there are no more classes today. The school will remain closed for a few days, until after the celebrations. Off you go and enjoy yourselves.’

It is drizzling, and we hang about undecided in the school porch. Should we go back home or shall we make for the bridge? We walk, awkwardly because of the strange time of day, through the village street, hugging the house fronts for shelter.

A moment later Jantsje catches up with us, panting, her exercise book with the anthems pressed protectively to her chest. ‘Jeroen, the master wants you, you’ve got to go back.’ She is looking at me gleefully, has it got something to do with that packet of chewing-gum? I never told anybody at home about it, but this morning as we filed into school Jantsje suddenly whispered to me in passing, ‘I know what you’ve got!’

I feel in my pocket where the packet is tucked away, warm and pliable. They won’t be getting any, it’s just for me and no one else is going to touch it, I’ll make sure of that! I run back to school, walk down the stone passage and stop when I hear voices. I can see the master in the classroom with two men I know vaguely by sight. They live in a part of the village we seldom visit and they are standing stiffly and awkwardly beside the blackboard.

I walk up to the master’s table, wondering what it is this time, what they can want with me. ‘This is one of the pupils from Amsterdam,’ he says. ‘Shake hands with the gentlemen, boy.’

Silently I put out my hand, my eyes riveted to the tabletop. Am I about to get news from home?

The master’s voice sounds friendlier than usual and I listen in amazement. He says that the visitors are members of the festival committee for the liberation celebrations and that they are looking for a pupil who can draw nicely.

From his desk he brings out a drawing I once made of a wintry scene, a farmer pulling a cow along by a rope. ‘You didn’t know I’d kept it, did you now?’

The men bend over the drawing and then look at me. Do they like what I’ve done? You can’t tell from their faces.

‘You will understand, of course, why we thought of you. The drawings have to be about Friesland, a fishing boat or something to do with our dairy products. And a national costume never comes amiss either. We shall have the drawings copied on a larger scale and then hang them up in the Sunday school.’

‘It would be very nice if they were in colour,’ says one of the men in a drawling voice, ‘that’s always more cheerful. After all, it’s for the celebrations. And that’s why we also thought of putting these words underneath.’ He fetches a piece of paper from out of his pocket on which WE THANK YOU and underneath v = victory are written in big capital letters in English.

I can’t believe it: they need me, they want me to do something for the liberation celebrations! Everyone will look at the drawings and know that it was me who did them...

‘I’ll do my best. When do they have to be ready?’

I am back in the porch, a box of coloured pencils they have given me in my hand. The school yard is deserted, the drizzle has turned to rain. The houses across the road are reflected in the smooth pavement.

he emptiness of the yard seems to increase the solemnity of the moment: there I stand all alone on the steps with my newly commissioned assignment, ready for a fresh, unknown start. Yesterday that present from the soldier and now this. My luck has changed!

A green car drives through the deserted street, the tyres making a lapping sound over the rain-washed stones. A piece of canvas hanging loose and blowing like a flapping wing in the wind looks like a ghostly apparition, lending the village street, in which I have never seen any other car since our arrival from Amsterdam, a completely different appearance. The houses seem smaller and the road, with the car in it, looks suddenly narrow and cramped.

Like a messenger from the Heavenly Hosts, of whom I have heard so much about in church, the vehicle whirrs past me, an imposing, combative angel. I slip into my clogs and start to run after it. At the crossroads the car stops and a soldier leans out. He calls. I stop and look behind me, but there is no one there. Surely he can’t be calling me!

‘Hey, you!’ He leans out of the window and waves to me, I look up into an intently inquisitive face. It is the soldier who gave me the packet of chewing-gum yesterday, the same chapped lips, the same searching eyes.

He opens his mouth as if he wants to say something and then realises that there is no point. Perhaps I ought to say something, but what? ‘Thank you’, or ‘V = Victory’, the English words the master wrote in my exercise book? But I don’t know how to pronounce them, even if I were brave enough to try.

The soldier looks inside the car for a moment and then holds another red and blue packet with a silvery sheen up above my head. ‘Yesterday,’ he smiles suddenly with a friendly grin that makes all my heaviness of heart fall away. He is like a divine apparition enthroned above me against the grey, massed clouds, as he raises the little packet up to heaven in what might be a gesture of blessing. Small beads of drizzle glisten in his hair, and a thin gold chain gleams under his throat, moving gently as if he were swallowing his words. It is in strange contrast to his strong hairy arms and unshaven chin. His face is like something I had forgotten and am seeing again after a painfully long time. It is almost with gratitude that I recognise the tooth with the chipped corner and the sharp groove setting off his mouth. The door opens; a leg jerks nervously and impatiently, as if there is a hurry. ‘Hello, come on in.’ I understand because he pats the seat beside him invitingly and nods his head. Should I really get in, can I refuse to? I ought to be getting back home, so I can’t really... He whistles impatiently and spreads his arms out in a questioning gesture. Before I know what has happened, he has leaned out and seized me by my coat. Reluctantly, I step onto the running-board and allow myself to be pulled into the seat, where I edge as far away from him as possible. Why am I so frightened, he has a friendly face and won’t do me any harm. On the contrary: with a few determined movements he beats the rain off my coat and puts an arm around my shoulder.

‘Hello,’ I hear again. I try to say the same thing back, hoarsely and timidly. They are right, I’m a scaredy-cat, too frightened to say boo to a goose. I clutch the box of coloured pencils as if my life depends on it and look at the door.

‘Okay.’ He stretches across me and slams the door shut. ‘Drive?’ he asks. ‘You like?’ His hand goes to my thigh and gives my leg a reassuring pat.

The car judders and then we are away. I hold tight to the seat, and whenever I see people I make myself as small as possible, no one in the village must see me sitting in a car belonging to the Americans.

While we are driving he looks at me, his face half turned towards me and his eyes veering from me to the road and back again. Why is he watching me like that, doesn’t he trust me?

We have left the village behind and are driving towards Bakhuizen, on a road I do not know. I have never walked here and the unknown surroundings frighten me as the familiar ground disappears from under my feet.

In the middle of an open stretch he slows down and then stops the car by the side of the road: that’s it then, now I’ll be able to get out and hare back to Laaxum. But the soldier lights a cigarette, leans peacefully to one side, and gazes across the rain-drenched land and at the shiny wet road. Carefully, I move my hand to the door handle and try to turn it without his noticing: I can barely budge it. The soldier takes my hand from the handle with a smile and continues to hold it.

‘Walt,’ he says pointing to himself. ‘Me, Walt. You?’ and digs a finger in my coat. I quickly free my hand and mumble my name under my breath, ‘Jeroen.’ I am ashamed of my voice.

‘Jerome?’ He puts out his hand and pinches mine as if trying to convince me of something. ‘Okay! Jerome, Walt: friends. Good!’ His lips make exaggerated movements as they form the words he utters with such conviction.

It is as if his hand has taken complete possession of me. I feel the touch run through my entire body. He has narrow fingers with bulging knuckles, the clipped nails have black rims. His thumb makes brief movements over the back of my hand in time with the windscreen wiper.

We are moving again, he gestures around the cab and gives me a questioning look, ‘Is good, Jerome, you like?’

I nod, and I’m beginning to feel a little bit easier. When the road makes a sharp curve, I lurch sideways and fall against the soldier. He puts an arm around me and stretches out the word ‘okaaaaay’ for as long as it takes to go round the bend.

We pass houses that seem to be bowing under the downpour and bedraggled trees that appear more and more often until they have grown into a wood. Everything slides past me through a veil of rain, whisked away from the windscreen with resolute flicks of the wiper. The arm around me feels warm and comfortable, as if I were enfolded in an armchair. I let it all happen, almost complacently. This is liberation, I think, that’s how it should be, different from the other days. This is a celebration.

His hand lies lightly on the steering-wheel, now and then making a few routine movements with it. That too is a celebration, this fast and trouble-free progress. I surrender to pleasant thoughts: he is my friend, this soldier, he will see to it that I get back to Amsterdam soon. Walt, what a strange name, just like Popke or Meint. Foreign. It’s a wonder he picked on me of all people, that’s surely something to do with God, the reason why so many sudden changes should be happening to me...

His hand, one finger having slipped inside the collar of my shirt, is kneading my neck, casually but deliberately. The landscape has become hilly and for a long time we speed along a forest road. I have never been so far from Laaxum. We stop at a clearing where a few other cars are parked, all looking like ours, green and with canvas roofs. A small group of soldiers is hanging about, some on their knees looking under one of the cars while another is holding a coat over their heads to keep the rain off.

‘Wait,’ says my companion, ‘just a moment.’ He winds down the window and calls out to the other men, then jumps out of the car and winks at me as if we are sharing a secret.

His boots grate across the quiet road and a moment later he is leaning against one of the cars talking with the others. I hear their voices and see them gesticulating vehemently and pointing to our car. The patter of the rain on the roof is lessening and a bright band of sunlight drifts across the road and the encircling trees. I look around the cab and put the coloured pencils in a groove between the seat and the backrest, clearly visible so that I shan’t forget them. Beside the wheel, under the buttons and the little dials, some strange words are written. They’re bound to be in American: I must try to remember them.

A small chain hangs down from the little mirror above my head, a silver cross and a few coins dangling from it, and next to it someone has jammed a shiny coloured print of a lady with bright yellow curly hair, smiling with a tight-set, red mouth. Her neck is long and bare, the yellow curls fall in gentle waves onto her shoulders. Do such women really exist, so sleek and brightly coloured? My eyes keep returning to that shimmering face with its carefree come-hither look.

A chill sun breaks through every so often. The talking on the road that had seemed endless to me fizzles out, the soldiers climb into their various cars. At last! My soldier jumps cheerfully behind the wheel and holds a chilly hand against my cheek. ‘Cold!’ He presses on the horn, making a loud noise, and the other cars sound their replies promptly through the still air. As they pass us one of the men whistles shrilly through his fingers and bangs on the roof of our car, giving me a fright.

‘So long!’

Then silence descends between the trees. We are alone again. The soldier opens a packet of cigarettes and holds it out to me. Me, smoke?

He chuckles and lights one for himself. The smell of wet clothes and cigarette smoke fils the cab, making me drowsy: it’s high time I went back home.

‘Tomorrow?’ he asks then. Seeing my look of incomprehension, he points to his watch and makes a circling movement on it. ‘Tomorrow,’ that same gesture again, ‘you’, a finger in my direction, ‘me’, a finger towards himself and then he makes steering movements.

Tomorrow, I think, another ride, and I hesitate. Nice though it is, I don’t really want any more.

‘Tomorrow-tomorrow,’ two little turns around the watch, ‘Jerome, Walt, yes?’

When I act as if I don’t understand, he points in answer to his mouth and makes a gesture of despair. He tries again, but I am suddenly tired and feel limp and listless. All I want right now is to go back.

He takes a map from a small compartment and studies it, then lays it open on my knees. We are again driving along stretches of road that seem to have no end, houses, woods, a patch of water. Is this the right way back to Warns? As we take a narrow side-turning, the soldier gives a whistle. Between the trees a small building comes into view that bears a slight resemblance to our Sunday school. He drives the car to one side of the house, jumps out and helps me down.

‘Come,’ he says when I hesitate, and lifts me to the ground like a little child, carries me to the side of the house where it is dry under a protruding eave, and gestures ‘wait’. From the back of the car he fetches a few tins and disappears with them round the corner. The car makes ticking noises, hard and dry, an accompaniment to the leaves that are rustling in the falling rain. I wish I knew what time it was...

A moment later loud voices come from the upper floor, interrupted by short bursts of laughter. I recognise Walt’s voice. There is no one to be seen anywhere. A twig snaps and every so often the smell of damp moss, earth and mushrooms drift across to me. I listen in surprise to the bursts of jollity in the house: has the soldier forgotten all this time that I am waiting for him and want to get back home?

I walk to the back of the house. The rain has stopped, but water continues to drip from the edge of the roof and there are puddles in the drive. A bright patch of sunlight appears on the wall and at that very moment someone flings open a window upstairs, casting a glaring reflection of light over the rain-soaked treetops. Is he still up there, how long is he going to be?

Behind the house a few neglected shrubs are growing and in a dark corner a lone hyacinth adds a shimmer of blue.

There is a wooden barn and a bit further away sits another car coloured the same green. A railway track runs behind the barn, looking as if it has not been used for a long time, grass and tall weeds growing between the sleepers. Beyond that, trees and silence. Carefully, so as not to make too much noise, I move back across the crunching gravel and go and sit down on the running-board. Should I just run away by myself?

The sounds upstairs have ceased. All I can hear now is a rhythmical, slight knocking noise, mingled with the sound of a tinkling bell.

‘Jerome?’ The soldier is standing at the back of the house and beckons to me. We walk around to the back of the barn. It is raining again. I press up against the wooden wall while the soldier steps onto the rails and smokes a cigarette, balancing on one leg. Swaying, he keeps his body poised, looking straight at me while he does so. What are we hanging about for, what is the point of this childish game? I feel a mixture of impatience and boredom.

Suddenly he sets his cigarette flying into the bushes in a wide arc and squats down in front of me. I can’t avoid his gaze, try to smile, but can only feel a nervous, uncertain tug around my mouth. What is he up to, is he teasing me? Then, moving fast, he stands up, looks around the side of the barn and with a quick grab pulls me to him. Paralysing fear shoots through me, my fingers tingle and a blindingly bright white veil is drawn over everything. My body is clenched as if a scream were about to burst out, but nothing happens. I feel the hard fabric of the uniform jacket against my forehead and smell a bitter tang of rain and metal. Awkwardly and tensely I stand there in his harsh embrace. He could kill me, here, behind the barn, he could throttle me or rip me apart without my uttering a sound, breathless and paralysed as I am. I’ll just let it happen.

From far, far away, I hear his hoarse voice: ‘Jerome, you okay?’ and feel something warm on my hair and then against the side of my face. He is kissing me. I do not move a muscle, hoping I might disappear and dissolve into nothing.

When he lifts my head I can see his eyes close to mine, a fierce, haunted look. His breath is coming fast, as if he is anxious. Then I am aware of real fear, a panic that pierces straight through me: I should never have gone with him. I ought to have known that something would happen. Unspoken and formless, it had been present from the very start, it had been lying in wait all along and now it had pounced...

His mouth moves over my face. I begin to tremble, uncontrollably and convulsively, and lose my balance so that he has to push me against the barn to keep me on my feet. Did I fight back without realising? There is brute force in any case, my body bangs several times against the clapboard and my elbows and shoulder-blades hurt. My head is bent backwards and I can feel rain on my face and then his face which seems to melt into mine. I seem to be drowning slowly, his suffocating weight enveloping me and pulling me down. I claw my fingers into the wood of the barn until my nails tear and I try vainly to push myself high up against the clapboard wall as my clogs slip away in the soft mud.

His hand gropes in my clothes. Restlessly and feverishly, he tugs at my coat and pushes his fingers up my trouser leg. Like an empty, helpless object I am glued to his mouth, an empty balloon, a trickle of slime. The grating surface of his jaw rasps across my skin, crushing my eyes and tearing my mouth. I try to jerk away and to make a noise, but all I can produce is a furious gasping.

The tip of his tongue moves between my lips, which he keeps parting stubbornly every time I press them tight together, slipping like a fish into my mouth. Then, when I relax my jaw a little, he suddenly pushes his tongue between my teeth and fills my mouth. We melt and fuse together, he turns liquid and streams into me. I look into strange, wild eyes right up close to my own, searching me. I am being turned inside out, shaken empty.

He breathes words against my face and when he lifts me out of my clogs I hear them clatter against each other with a dry, bright sound. ‘Listen,’ he hisses and twists me awkwardly against him, ‘listen. Is good, is...’

Voices sound from the house, footsteps pass over the gravel, for a moment I am free and catch my breath as if coming up from deep water. Then he pushes me against the wall by my shoulders and tilts his head to listen. Nearby an engine starts, but the sound moves away from us and disappears.

Gently he wipes away a thread of spittle running down my chin, as if to console me. I am standing in the mud in my socks looking for a clog. Everything feels grubby to me, wet and filthy.

‘Jerome,’ he asks, ‘Jerome, okay?’

It happens all over again, the mouth, the hands, his taste which penetrates me, which I can’t shake off, and the rough lunges that suddenly turn gentle. I feel cold, my face is burning and hurts; from my throat comes a strange, strangled sound. I am afraid that if he lets go of me I shall fall down and will never be able to get up again, that I shall die here in the mud and the rain, forgotten behind an old barn. He puts an arm around my waist and pushes me quickly through the wet shrubs alongside the barn. His car starts silently and quickly. A little further along the road he stops and pulls me to him. I start to cry, softly at first then harder and harder, my shoulder-blades, back, arms, knees, everything shaking and trembling.

<br>

Why is he smiling, how can he be so friendly and act so ordinarily? Why didn’t he do what I thought he was sure to do, leave me behind trampled flat in the mud?

He takes out the map and opens it, his finger follows a red line and then points to a dark spot: he is looking for a lonely place to throw me out at the side of the road.

‘House?’ I hear him ask, ‘Jerome house?’ I bend down over the spread-out map, which rustles on my unsteady knees.

‘Jerome, look.’

When I start to cry again, he holds me against him and lifts the map up in front of my face.

‘Warns,’ I read beside the small dot where his finger is, I point to Laaxum, printed there clearly in small letters. Is he taking me home, do they have to see me step out of an American car?

He kisses my eyes dry and to my horror licks away the threads running from my nose with a mouth that is coaxing and soft and cleans me lovingly.

We drive on again, the soldier stroking my back continuously. I no longer cry.

‘Say Walt, say it,’ he begs, ‘Jerome, come on, say Walt.’

In surprise I listen to my voice reacting hoarsely as it is asked. I feel an overwhelming urge to lie down on the seat, but stay sitting up, looking straight ahead with smarting eyes: I must know where we are. He takes my hand and places it on his leg where it remains limp and lifeless. The rain has stopped. His taste is in my mouth and refuses to go away. Will it stay with me for the rest of my life, this sense of being immersed, of being steeped in filth and fingers? I sag sideways on the seat while the soldier lights a cigarette.

<br>

He lets me out at almost the same place I got in. Awkwardly I remain standing beside the car as if waiting for something more, a sign or a command to disappear. He raises a hand and winks, then the door swings shut and I take to my heels without looking back. I race through the village, along the edge of the road, my head hunched between my shoulders so that no one can see my face. But the street is quiet, it is raining and there is no one about.

I can’t go home, that much is certain, I have to be alone and think what to do. Beyond the village I crawl down the side of a ditch, to the bottom where the ground is wet and the chill penetrates my clothes. I can feel his taste in my mouth, in my throat and on my tongue, a bitter smell clinging to me, a mixture of metal and hospital odours. When I sniff at my sleeve, the smell of the soldier is so strong and immediate that he might be sitting right next to me. I taste my spit, let it travel down my throat, dividing it with my tongue and gathering it together again. I want it to go away, I must be rid of it. I spit and it drips in a long stream into the grass.

Mem will know straight away what has happened to me, she’ll be able to read it in me as if it were written in large letters all over my body. I am sure that my eyes are bloodshot and swollen, and my face feels battered, grazed and smarting.

Without thinking any more I take the turning to Scharl and trudge towards the isolated farmsteads in the distance, pushing against the strengthening wind. Muddy pools sometimes cover the entire road, reflecting the yellow-white streaks that now and then break through the black clouds. A gust of wind blows a pair of screaming birds across my path. I keep an eye on Jan’s farm, wondering whether I should go over there and see him. He is the only person I can tell, but perhaps he, too, will just tease me and then go and tell everybody else. I stand still, in two minds. If he talks about it, then I’ll be utterly lost. Quickly I walk past his house and hope that no one has seen me. I must remain invisible, unnoticed.

A moment later, there is a downpour. I run past the dark slope of the Cliff and clamber across the dyke. The sea is a grey, tumultuous mass into which the torrential rain is drumming small punched holes. Near Laaxum, where the sea wall begins, I huddle close to the piles and sense the water under me, hurricanes of furious spray, foaming as it streams between the blocks of basalt. I run my fingers over my face. Everything appears askew, pulled apart. Can present myself at home like this? I do not move and slowly start to stiffen.

<br>

At home they are still at table, a closed circle under the oil lamp. Faces look at me, all except Meint who continues to eat, bent over his plate.

‘Where on earth did you get to, why are you so late?’ I have no answer. ‘Don’t just stand there, take off your coat.’ Mem pushes her chair back and looks appalled. ‘You’re making everything wet.’

‘School. I had to stay behind at school, do some drawings.’ With a shock I realise that I have left the precious box behind in the car. Now everything is bound to come out.

Hait takes me by the shoulder and ushers me out of the room. ‘We must have a bit of a chat, my boy.’

So that’s it, he knows everything already, I won’t be able to hide anything. Leaning against the wall, I strip off my wet clothes and pretend to be busily occupied.

‘Is it true, Jeroen, what Jantsje tells me?’

I try to look surprised and innocent. ‘What?’

‘Jantsje says a soldier gave you sweets, the boys from the village saw it all.’

Relief leaps up in my throat, I bow my head and nod.

‘If you’re given something, you mustn’t keep it to yourself, surely you know that? It isn’t very nice to keep things for yourself.’

I look into his thin face, a tired man trying to look severe. I am not his child but a guest, a refugee from the city. He stands awkwardly facing me in the dim little room and almost smiles at me. I want to go and stand next to him, I want him to hold me close.

Dispassionately, I lay the little packet in his hand. ‘One of them is gone. I’m very sorry, Hait.’ Now I’m crying all the same.

Hait tears the blue and red paper open and holds the four thin silver slabs in his hand. My secret, my precious secret...

‘Four. One for each of you. Go and hand them out.’ He steers me gently back into the room.

I lay one slab each next to Meint’s, Jantsje’s and Pieke’s plates, that’s how Hait would want it. But the fourth, is that for me? I put it down beside Trientsje but she pushes it back gently. ‘Keep it for yourself.’

I leave it on the table for them to decide who gets it, Diet or Popke. In any case I’m rid of it, I don’t need it any more. It came from the soldier and he has to be banished from my memory. I sit down at my place, but don’t eat. ‘Don’t you want anything? Are you sick?’ Please don’t let them ask me any more questions, please make them leave me in peace. I listen to the others cleaning their plates, the smell of the food and the heat making me dizzy.

When I swallow I can taste the soldier, strong and sickly. I must eat something, I think, then it will go away, then I’ll be rid of it. But suddenly, inexorably, I know that I don’t want it to go.

<br>

In the morning, Mem comes to look at me. She places a large searching hand on my forehead and says, ‘Much too hot. You’d better stay in bed for the day.’

Behind the closed doors of the cupboard-bed, waking and dozing off again, I am aware of scraps of the day passing by: distorted voices float up from breakfast, and a penetrating smell of tea gives my stomach cramps from thirst.

As I clasp Mem’s hand and take greedy gulps from the mug she holds to my mouth, I notice that the soldier’s taste has vanished, dissolved in my sleep, gone. I keep swallowing.

One dream later, the soldier is pressing me against the wall, his tongue an eel that creeps into my ears, into my nostrils, into my throat. The soldier reaches out for me, grinning and winking. I can feel the eel wriggling around my feet and then slithering up my legs. When I pull away, the soldier jumps into the car with a soundless scream and drives wildly after me. Behind the car rushes a bloodthirsty, furious mob led by the minister’s wife. I hide beside the sea and they all fall into the water, one by one...

Then I hear the sound of peeled potatoes plopping into a bucket. The cupboard-bed is unimaginably large. I lie like a dwarf between the blankets, the narrow walls miles away.

‘Mem?’ Am I making a sound or am I merely moving my mouth? Tea, I’m thirsty, I want to say, but I can’t talk, I have no voice. Eternities later, I look into Mem’s anxious face.

‘What’s up with you, what are you talking about? Coloured pencils, we don’t have any of those here, you must have had those in Amsterdam.’ She pulls the blankets up. ‘Just you lie quietly now and try to get some sleep.’ Her hand is stroking me gently. Or is it him, suddenly back?

...the soldier pushes chewing-gum into my mouth, a gag that grows thicker and thicker. He shoves it down until it’s stuck in my throat and I can’t draw breath any longer. I fight grimly to free myself and thrash about wildly...

Befuddled, I look into a small basin Mem is holding in front of me. A long thread of slobber is hanging from my mouth and stinging morsels burn in my throat. Mem pats me patiently on the back.

When I try to clutch Mem’s hand so as to have somebody to hold on to, she has already gone. I can hear her clogs in the lane and a dragging sound as if she is moving something, or somebody. Who? I sit up.

But perhaps it’s just the wind...

In the evening, Trientsje softly opens the cupboard-bed doors. ‘Aren’t you sleeping?’

I can see the others in the room having a cold meal, hear the clicking of knives, distant voices, and I see Hait nod to me. Mem feeds me porridge, her hand rises insistently each time to my mouth, and dutifully I accept the spoon. Afterwards, I feel better, the warmth settling like a cossetting wrap around my body. Content and babylike I let Mem get on with the business of wiping my face and neck with a wet cloth and then she tucks me in firmly under the blankets.

‘He’s on the mend, his temperature is down,’ she reports to the room.

Could I ask them for my coat? I want to smell the sleeves and have the soldier near me. I must know what he smelled like again...

<br>

When I feel somebody beside me I sit bolt upright, in a panic.

‘I shan’t eat you, you fool,’ Meint chuckles.

In the dark I listen to the creaking of the floorboards and feel him tugging at the blankets.

‘Can you get anything you like from that soldier? Even chocolate?’

I pretend to be asleep and say nothing. If only I had my coat with me, I long for his smell. I turn to the wall and try to recapture his face, the slope of his cheek, the broken tooth.

Meint is asleep, I can hear him breathing gently and regularly.

Did I get sick because I had somebody else’s spit in my mouth, is that dangerous? Then I remember with a shock that he had wanted to take me for another drive today. My body grows hot and wet: will he be cross with me, did he wait for me? I can feel small drops on my forehead and my heart is beating fast.

The soldier crouches on the rails behind the small barn. He looks at me, holding me prisoner with his eyes.

## 3

BECAUSE SHE THINKS I am still sick, Mem tells me not to go to church next morning. The dirty breakfast things stand on the table in chaotic confusion. The living-room is deserted.

I step outside and watch Hait, who is walking down the road to Warns with the children. It is glorious spring weather, the girls’ coats are unbuttoned and Popke and Meint are gaily sporting white shirts. An army car comes towards the small group of church-goers, hooting loudly as it passes them, tearing a hole in the Sunday morning stillness.

The car turns off on to the sea dyke, stops a bit further on and a few soldiers get out and walk along the dyke. They look around before disappearing out of sight down the other side. I stroll back indoors and look through the window: the car looks lost on the road, like a motionless decoy, frozen in silence. I am in two minds whether to go up to it or stay at home. Mem is sure to find it suspicious if I’m suddenly well enough to go to the harbour.

I take a few faltering steps. ‘I’m just off to the boat, to see if everything’s all right.’ Obediently I put on my coat. ‘Else you’ll get sick again,’ Mem calls after me.

There’s not a soul at the harbour. A white goat is grazing at the end of a rope and gives a piercing bleat as I pass. The boats’ masts rock lazily to and fro, gleaming like knitting needles. The quay is deserted. I kick a stone into the water: placidly ebbing circles. Should I go back? My longing for the safety of my cupboard-bed into which I can creep and shut out the world becomes stronger with every step I take: the darkness and the seclusion, and the caring hands of Mem bringing food and straightening the blankets.

I walk to the other side of the little harbour. Seagulls flying over the sea wall retreat when they see me, still gobbling helplessly struggling fish in their hooked beaks.

Further along the dyke sheep are grazing. From a long way off I can hear the regular sound of their grinding jaws tearing out the grass.

Silence and wind, not a living soul, no soldier, nothing. A metal drum makes ticking noises as if the heat were blasting the rust off it in flakes. I walk up to it under the nets hung out on poles and lay my hand against the hot surface. A hollow sound. I look at the reddish brown powder that has stuck to my hand and smell it. Iron.

I am vaguely reminded of the smell of the soldier; how is it that I can remember the smell of his touch? On the pier which protects the little harbour from the sea I walk alongside the beams of the weathered sea wall, placing my feet carefully on the rocks, afraid to lose my balance. In the shelter of the timber boarding the heat is suddenly overwhelming. I sit down, my head swimming.

A broken beam gives me an unexpected view of the sea, a greenish-grey surface moving restlessly and filled with light. The wind blows straight into my face and when I open my mouth wide to catch the current of air it is as if the inside of my head is being blown clean, and the last remnants of my lingering sickness are swept away.

The turbulence of the water washes away my lethargy. I stick my head through the opening between the beams and look out over the wide, empty expanse of the coastline. On the distant rocks the vivid, irregular shape of a human figure intrudes upon the splendour of the view, a man lying on his back in the sun, a completely isolated, insulated being.

I quickly turn my head away as if my mere look could disturb the perfect stillness and privacy of that distant man. My gaze settles on the quiet tedium of the harbour boats bobbing against the quay and roofs dipping away behind the dyke. But the sunbathing figure behind the sea wall draws me like a magnet, pulling me with invisible threads that tremble with tautness as if they were about to snap.

Without hesitation I climb over the fencing and am suddenly quite alone at the mercy of the hurrying waves and the solitary sunbather. Holding tight to the beams, I balance across the stones towards him. Every so often, the water splashes up between the rocks and makes white patches of foam in the bright air.

The sunbather has heard me and turns towards me, calling out something cut short by the wind: it is my soldier. He lies stretched out on a rock and smiles at me with his eyes screwed up. When I am quite close, he sticks out his hand. I take it and immediately let go of it again. ‘Hello, good morning.’

Best walk on calmly now, as if I had to be getting on, as if I were on my way to somewhere else. But his hand clasps my ankle so that I can’t move. ‘No, no. Where you go?’ He tugs at my leg and pulls me closer. He is wearing nothing but khaki shorts. ‘Sit,’ he says, ‘sit down.’

He moves up to make room for me and then stretches out in the sun again. His arms are folded lazily under his head and he surveys me with an almost mocking look. The front of his shorts gapes open a bit, confronting me with a part of his body I am not supposed to see. Embarrassed, I turn my head away and look out across the sea as if something in the distance were suddenly attracting my attention. Even his hairy armpits, exposed so nonchalantly, make me feel that I should not be looking: there is something not quite right, something that doesn’t fit.

The soldier pulls me down next to him on the rock. Now and then I throw a quick glance a this half-naked body in the hope that his flies will have closed again. ‘Jerome,’ I hear him say. ‘Sun. Is nice, is good.’

He yawns, stretches and slaps himself on the chest with the flat of his hand, hollow, resounding slaps. I feel relieved when he sits up. His legs dangle down from the rocks and his feet have disappeared in the water. His back is a smooth, unblemished curve with a pronounced hollow down the middle. The unevenness of the rock has left white impressions on his skin and some sand and pieces of dirt have stuck to his shoulders. He makes splashing sounds with a foot in the water, but every so often, as if to take me by surprise, he turns his head towards me; trapped, I quickly avert my gaze. From out of the corner of my eye, I see him get down and stand in the water, one hand on his hip and whistling a tune between his teeth. A jet squirts straight and hard from his body into the water making a frothing patch of little bubbles and foam in the waves. Before he sits down again, he walks up to the fencing and looks over it, searchingly.

The water has a salty tang. A small white fish drifts about between the rocks, staring up with a dull, sunken eye as it continually collides with the dark volcanic rock. Casually the soldier taps a slow rhythm out on his thighs. Spread over the edge of the rock, his upper body moves about jerkily, and he hums softly to himself. Above us two gulls hover on motionless, outstretched wings, their heads turned inquisitively towards us as they keep us under close scrutiny.

Suddenly he stretches his arms and makes flying movements, uttering screeching noises in the direction of the birds. I make an attempt to laugh. The gulls slip away, but keep their heads turned suspiciously towards us. The soldier stands up, then squats down by a small pile of clothes and lights a cigarette. I lay my hands on his trail of wet footprints on the rocks. It suddenly seems important to me to do this and I want to memorise the contact: the foot of a liberator I have touched, have felt with my fingers.

The soldier leans against the sea wall, puffing hard at his cigarette and smiling at me. From further away he seems less overwhelming, the feeling of oppression he gives me and the memory of what he did with me have begun to fade.

He sits down beside me and puts a cigarette between my lips; by way of demonstration he takes in a deep breath, but the smoke has already reached my throat so that I splutter and cough.

He lies back again and blows smoke rings in the air that float away leaving a sweetish smell behind. The more I try to hold back my nervous coughing the more violent it grows, as if about to choke me. He takes my wrist and brings it hesitatingly to his face, watching my reactions intently. He blows smoke teasingly at me, his lower lip stuck far out, while I try convulsively to stifle a cough.

He bites softly into my fingertips and licks my palm; I clench my fist and he laughs. Then my hand is led to his throat, and I feel a swallowing sound transmitted to my fingers. He guides my hand across his chest, an uneven and mobile surface radiating warmth. Under the skin, where little hairs curl under my fingers like crawling insects, I feel a heavy, hidden heart-beat, a booming that tells me that I am not alone, another life, real and throbbing, next to my own. My hand travels across the small hills of his ribs, then over an even stretch, across a little hollow into which my fingers sink and where they remain: I am a blind man being helped from place to place, standing still, waiting, going back. The soldier is taking me on a guided tour of his body, inch by inch. I am his doll, a plaything with which he does as he pleases.

I look at the sky, a vivid blue flag over the circling gulls. Why is this happening to me, why has my life changed so suddenly, so utterly? I feel his stomach, smooth and flat. When he presses my hand down, there is scarcely any resistance. But when he inhales smoke, the surface hardens, remains taut for a few moments until he blows the smoke out again, when it all gives and goes soft again.

Bouncy little sand heaps, that’s what it feels like. In Amsterdam we used to jump on the pumped-out sand at the building site until the ground turned elastic under our feet and the water would come up. We would jump and jump until our shoes sank down in the mud. Then we would go home with wet socks: Mummy would be furious...

Unexpectedly my fingers encounter the edge of his pants, which he raises and pushes my hand inside. I pull my arm back and sit up straight. Silence. The soldier looks at me sideways. He fumbles with his pants and his fingers manoeuvre a floppy brown shape out through the flies, a curved defenceless thing. He lays his hand over it protectively, pulls me towards him and presses his mouth to my cheek, His voice is soft and soothing. What is he saying, what is going to happen now?

My ear grows warm and wet and a slopping sound seeps into my head, stupefying me. I pull my shoulders up and goose-pimples shoot in sudden shivers down my back and up my arms. He is licking the inside of my ears, I think, and they’re filthy, when did I last wash them? I am filled with shame, not because of the tongue licking my ear, but because of the yellow that sometimes comes out of my ears on to the towel and which he is probably touching with his tongue right now.

He lifts his hand from the thing and starts to turn it around slowly. It falls between his legs, then rolls across his stomach and comes to a standstill between the folds of khaki. As he rolls it around I can see it grow larger and stiffer. It seems to be stretching itself like a living creature, a snail slipping out of its shell. My dazed eyes are glued to this transformation, this silent, powerful expansion taking place so naturally and so easily.

‘When you grow up, you’ll get hair on your willy,’ the boys at school used to say. I had had a vision of some misshapen, hairy caterpillar, and I had hoped that they were wrong or else that I would never grow up.

The thing is now sticking up at a slant from the soldier’s body. There are no hairs growing on it, though I can see some tufts through his open pants. He makes a yawning sound, lazy and relaxed, as if the way he is lying there were the most natural thing in the world.

‘Is good, Jerome,’ he says, ‘no problem, Okay?’

The thing has a split eye which stares at me. I try not to look at it, but my own eyes seem riveted to that one spot. It is a clenched fist, large and coarse, raised at me. The soldier grasps it roughly and a drop of liquid seeps out from the split eye in the pink tip.

My own prick and Jan’s are small and thin, slender, fragile branches of our bodies. When the soldier lays my hand against his belly, the lurking thing suddenly springs up higher still, flushed out of its lair by the touch.

Under the rocks the sea surges to and fro, eddying and making gargling noises. Suddenly I can see my little room in Amsterdam quite clearly, the books placed in orderly array on the ledge of the fold-away bed, the framed prints on the wall and the place where I like to sit, under the window, out of sight from the neighbours. The sun falls across the rough surface of the coir mat where I have put a toy donkey out to grass. The voices of children can be heard outside playing by the canal. It is after school and I can smell food and feel the summer languor in my stomach.

It is far away, almost forgotten, a steady way of life that often seemed boring, a monotonous stringing together of days.

Mummy, I think, where are you?

The soldier presses himself closer to me. I can smell his nearness. Like a blanket, his smell spreads over me, protective and threatening all at once. His quick breathing unsettles me and he pants as if he were in full flight. But I am the one who should be running, running away...

I try to get to my feet, but the soldier suddenly uses force. We have become opponents, doughty and fierce. ‘Come on.’ He holds me firmly in his grip, his voice curt and impatient, squeezing my fingers more firmly around the knob and throwing his leg across me, the painful pressure of his knee pinning me to the rocks.

‘Let go of me!’ I shout the words out in a panic, and days later I can still hear their shaming sound. I know he can’t understand me, but the weight of his body grows less and his grip weakens. My arms are trembling.

‘Sorry, baby.’ He relaxes but goes on moving my hand up and down. What does he want, why does he make me do this, this horrible clutching?

I had heard stories about men who kill children; perhaps that’s how it happens, just like this. The thrusting inside my hand grows fiercer, he again bends over me and I turn my head away. On the rock beside me lies the stub of his burning cigarette; the smoke climbs up in a straight line and then tangles and disappears. When voices can suddenly be heard, clearly and close by, I pull my hand away like lightning, paralysed with fear. The soldier has already stood up, trying hastily to hide the awkward shape in his pants. Two unsteady steps and he is standing in the water. He falls forward and swims a few side strokes.

Two soldiers have climbed onto the sea wall, one of them whistling shrilly through his fingers and shouting. Walt turns round and stands up in the waves on strong, tensed legs, the sun casting luminous patches over his wet skin. The water barely reaches his knees. ‘Cold,’ he calls out.

He lets himself drop over backwards and swims off, sending up fountains of splashing water. One of the soldiers strips and wades in after him, the other one sits down a little to one side of me and lifts up his hand to me indifferently; he has bright blond hair and broad, round shoulders which he fingers constantly with obvious satisfaction.

I squeeze against the sea wall and hope that Walt will swim far out so that I can slip away unnoticed, but the two swimmers clamber onto the rocks nearby and then walk into the grass.

The blond soldier points to the small pile of Walt’s clothes, waits for me to pick it all up and then runs ahead, leaping from rock to rock. Close to the dyke we put the clothes down in the grass. Walt has slung a shirt over his shoulders and shakes himself dry. The blond soldier disappears behind the dyke and comes back with a crate of small bottles in a folded piece of oilcloth. He puts the crate down in the grass and whips the tops off a few of the bottles with a knife.

‘Coke?’ Walt passes mea bottle. The contents have a strong smell and at the first gulp a charge of bubbles fly up my nose.

With tears in my eyes, I hand the bottle back and shake my head: I am convinced I have been drinking beer.

In Amsterdam I once saw drunken Germans, with beer bottles in their hands.

‘Swine,’ my mother had said, ‘what a nation...‘ and walking quickly had dragged me away.

The soldiers lie chatting on the opened-out oilcloth, smoking and rapidly emptying the bottles. Now and then Walt turns around and calls something out to me. Then he sings the song he hummed earlier and the two soldiers whistle along shrilly.

‘Jerome, sing. Come on.’ He walks across to me, pulls me up and does a few dance steps making me stumble over his feet. The others laugh. ‘Sing!’ He gives me an almost pleading look.

What am to sing? Every song I ever knew has been wiped instantly clean from my memory. Walt holds my shoulders tight and seems to be trying to force it out of me.

‘Come,’ he says and squeezes me against his hard body, ‘sing.’

Confused, I look at the soldiers, what must they be thinking? One of them puts a finger to his lips and gives me a conspiratorial look. He steals closer and pours the contents of one of the bottles down Walt’s back.

Then, shouting, the two of them chase each other up and down the dyke. I don’t know if it’s in earnest but whenever they catch each other you can hear resounding slaps. I go and sit a bit higher up on the dyke to watch the fight from a safe distance.

Walt comes running back out of breath and both of them fall into the grass, fighting and dealing out blows to each other, yelling like schoolboys. Then both the soldiers grab Walt’s arms and pull the wet shirt off him. Walt struggles to break free and kicks out with his legs. Yet he is laughing. I can feel myself losing my temper: what are those two doing, are they hurting him? Why don’t they get back into their car and leave us alone? I am scared that I may be forced to join in the fighting, to take sides and defend myself. Will I be able to help Walt?

Suddenly, the scuffle is over and there is an ominous silence. Walt is lying on his back on the oilcloth, the other two sitting next to him, talking in subdued tones and looking around. Are they expecting somebody?

The blond soldier gets up, walks past me, goes up the dyke and stands there. Walt leans back on his elbows, tilts his head, and looks at me.

‘Jerome, come here,’ he says. His voice is soft and coaxing. What is it now, what do they want from me? I don’t stir, and they seem to forget all about me once more.

The land smells of earth and manure, insects buzz low over the grass and the air near the Red Cliff is shimmering. I narrow my eyes to small slits. Below me I hear mumbling and a short laugh.

The mysterious goings-on worry me; I feel left out. The soldier sitting beside Walt has bent down low over him, as if to take a close look between Walt’s raised knees. Walt has spread his arms and keeps turning his head to and fro. I look on with half-shut, smarting eyes: the soldier’s head is still bent greedily over Walt. I get up. I’d like to run over to them and kick the other soldier furiously out of the way; I feel ungovernable hatred welling up inside me. But with a stiff smile on my lips I work myself backwards up the dyke instead, my eyes fixed on the two men. The soldier is pummelling Walt’s stomach with quick short thrusts, grimly and silently at work as if giving him artificial respiration.

I know exactly what is happening, I know it from my suspicions and vague fantasies. And yet these baffling and ominous goings-on make me ill at ease: why does Walt let him do it, has he forgotten that I am here?

I race up to the car parked on the other side of the dyke and look inside. My coloured pencils are still on the seat, fallen half under the back. The blond soldier has run up after me; he reaches into the car and hands me the box. ‘Here.’ When I try to go back up the dyke, he grabs hold of me, giving me a look that is both kind and aloof. ‘No,’ he says firmly and points to the road. ‘Not now. Go.’

I want to see Walt. I want to know what is happening to him. Why can’t I go to him? The soldier pushes me down the slope and disappears behind the dyke. A moment later the other one comes up, looking right and left as he buttons up his shirt and stuffs it into his pants. Then he sits down in the grass and starts to whistle.

Why hasn’t Walt come yet, should I wait for him?

All is quiet, nothing moves. The sun bakes the road and fils the stillness with unbearable oppression. I turn on my heels and run home. When I reach our fence I hear the car horn in the distance. I look back: more hooting. For me?

Laboriously the car turns around on the narrow road, but I am already over the fence. Mem is standing on the grass beside the house, her hand shielding her eyes as she watches the vehicle slowly approaching.

‘The Americans,’ she says, ‘did you see them? Were they at the harbour?’

When she realises that I have been running fast, she adds, disparagement in the tone of her voice, ‘What’s the matter with you? Surely you’re not afraid of them? Do you think they’re going to hurt us?’

The car drives past, a raised arm sticking out of the window.

‘The liberators,’ she says, ‘look, they’re waving.’

She doesn’t dare return the wave, but nods her head graciously at the passing car.

I walk to the other side of the house and place my head against the wall. The box of coloured pencils tips open and the pencils fall out, vivid little streaks of colour in the grass. I hear the sound of the car dying away gradually in the Sunday stillness.

## 4

ON THE DAY of the celebrations the sky is stretched tight, a blue sheet flecked with birds. The shrubs are in blossom and the trees dressed in young green shoots. Today everything seems to be on its very best behaviour.

We set out early from Laaxum, excited but silent at the thought of what is to come. Hait and Mem are joining us later. There is to be a procession with a band, a street display with pictures of the queen, and a tribute to the soldiers when we will sing the songs we were taught specially at school. For the soldiers, our liberators!

I keep thinking of Sunday: the soldier on the rocks by the sea, my hand on his body and the strange things that happened after that at the foot of the dyke. I try to shut it all out because it bothers me, but the image keeps returning.

Don’t think of it, today is celebration day...

Meint is carrying a small, shiny trumpet which he is going to be playing soon with the brass band, and he holds the instrument in his arms like a trophy, proudly and triumphantly.

Just don’t let me see the soldier in the village. I must try to stay inconspicuous and not get into any more trouble. But secretly what I am hoping is that I shall be seeing his face again and that maybe he’ll want to talk to me.

The road is a line of people all making for Warns. Clearly something out of the ordinary is going on there. Meint jumps exuberantly on to my back and waves his trumpet. ‘Keep walking, you’ve got to carry me all the way to the church. Gee up!’ I push him off and join the girls. Trientsje takes my hand. ‘It’s going to be a lovely day, poppet,’ she says, ‘you’ll see. Everybody will be there.’

The village is all decked out: flags have been hoisted in many of the gardens and in front of one of the houses there is an arch decorated with paper flowers and orange streamers fluttering in the gentle breeze. The street is one long colourful festoon.

A narrow piece of cloth has been strung up across the road between two poles with ‘THREE CHEERS FOR THE LIBERATORS’ blazoned on it in big letters. We stop and look at the writing above our heads, solemnly, as if we were in church. Walt is one, too, I think proudly; he is one of them. Three cheers for my liberator...

There are a few carts standing next to each other by the crossroads, the horses’ backs gleaming in the sun. Every so often as light odour of horse manure drifts through the village and mingles with the strong smell of mothballs and eau de Cologne given off by the Sunday best of the crowds out in the village street.

‘You must come with me to the Sunday school,’ I tell Trientsje as soon as we have reached the village, ‘to see if my drawings have been put up.’

We push our way through the crowd and I hold her hand tight.

‘First the exhibition.’ She pulls me to the side of the road where picture postcards and photographs cut from newspapers have been set up on display in a booth.

‘Our queen and the princesses,’ says the man running the stall. I see pictures of a short, stout woman in a long creased overcoat and a funny hat, her arms hanging limply by her side in sleeves that are too long for her. She is looking into the distance with a vague, rather haughty smile.

Is that her then, our queen? Up with Orange, long live Wilhelmina? She must have been made poor by the war.

A large picture, coloured with garish greens and blues, seems to be the main attraction. Trientsje has to elbow her way in to get close to it. We see a woman sitting on a sloping meadow, her arms folded around her knees, with three girls in white dresses and bows in their hair smiling happily beside her. Behind them is a white house, and just to their left is a man in spectacles with his head held on one side, his shiny hair plastered down and smoking a pipe.

Juliana with the little princesses,’ says Trientsje, and swallows. ‘Lovely, aren’t they? What little darlings.’

I look at the well-cared-for children on that improbably green grass. ‘Do they have soldiers to protect them?’ I ask, but Trientsje is busy talking to the man from the booth.

A picture postcard with scalloped edges is lying under a sheet of glass among some sombre prints. The Royal Palace In Amsterdam, it says in white letters underneath. I bend forward to get a good look: that was the street where the lorry was waiting, that’s where my journey to Friesland began. A boy is standing there beside a delivery bicycle, and a woman in a long dark dress and a wide-brimmed hat looks in surprise at the camera while a little girl hides her face in the woman’s skirts. It is a fetching scene that has nothing in common with my grey memories of the place.

There is music in the distance now. Everyone is suddenly moving in the same direction, and Trientsje, spurred on by the general commotion, pulls me along. ‘If we’re quick, we can still get to the front.’

A bit further up the road I can see the band coming towards us, a group of people marching shoulder to shoulder with flags and banners held gaily in the air, instruments flashing in the sun. Their footsteps are sending clouds of dust up into the sunny sky and the monotonous thump of the drum lends the approaching dust-shrouded procession an air of doom. ‘Hullo.’ Jan is standing beside us, wearing a jacket that is too small for him so that his wrists stick out from the sleeves. He is pale and his eyes have a defensive look. Wrinkling his nose, he points meaningfully to Trientsje and gives me a knowing grin. ‘Walking out with your own sister, huh?’ He whispers something incomprehensible in my ear and gives a disdainful laugh. His hands have grown broader, there are black edges to his bitten nails and his eyes examine me closely.

‘Have you heard from home yet?’ I ask. I must remain friends with him, the two of us are going back together to Amsterdam. Jan spits into the palms of his hands and rubs them together vigorously.

‘No,’ he says, ‘but I don’t care, I’m staying on here anyway. I like it better here.’ There is a soft shadow on his sweaty upper lip and little white spots of sleepy dust in the corners of his eyes. He gestures with his head in Trientsje’s direction, gives me a knowing wink, and says, ‘She’s a bit of all right. Mind you squeeze her good and proper.’ He digs me in the ribs and runs across the road, right in front of the brass band. The noise thunders over us and overwhelms me. The musicians march past taking small shuffling steps, the sound they make rising up like a cloud of dust into the sky.

I can see Meint blowing his trumpet, red-faced, his eyes starting from their sockets. He keeps his gaze fixed rigidly on the sheet music clamped to the front of the trumpet, looking neither up nor down. My throat tightens at the sound of the music, I am being ground down between the mournfulness of the melody and the doleful counterpoint. A small open car follows behind the band with a soldier at the wheel and beside him another military man with a cap who brings his hand up to his head every so often in solemn salute. A tulip with a bright green leaf comes sailing out of the crowd, lands on the bonnet and lies there limply.

The car behind is full of soldiers hanging over the side and waving, some holding bunches of flowers in their hands. I look for a special face among them, but in a few seconds the procession has passed by. The people throng the road once more in the wake of the car and walk together towards the church.

I look around, but Trientsje has disappeared. Someone puts two arms around my neck from behind. ‘But if your father and mother come and fetch you, then I’ll go back with you,’ says Jan, continuing our interrupted conversation and pulling me back. ‘Remember, now, don’t you sneak off without me. Just tip me the wink, that’s what friends are for.’ He gives my shoulder a friendly push which sends me stumbling into a garden fence.

The brass band is lined up on the road in front of the church playing a solemn tune, the crowd joining in hesitatingly. Over their heads I can see the top of the army car. I walk towards the side where Hait and Mem are standing between villagers. Hait is singing. Mem looks impressive in her Sunday frock, abroad dark shadow engulfing Hait at her side. She listens with her chin raised, looking subdued and helpless, a little as if she badly wants to sneeze. Now and then she rubs her eyes awkwardly and nervously with a handkerchief. Hait could be her child, standing there beside her with a shy smile, his mouth moving as he sings.

When the song is over an engine starts up and the car with the soldiers drives off. The man in the cap has stayed behind; he walks up the church steps as if proudly showing off the gaudy stripes on his uniform jacket. In gleamingly polished shoes he goes up to the church doors, and stands there stiffly to attention.

As the car nudges a path through the crowd I try to get as close to it as I can. Isn’t that Walt, that soldier hanging on to a strut with upraised arms? Is he looking for someone or am I just imagining it? I use my arms to push through to the front, but the car is already too far away and gathering speed.

The church clock starts to chime and while everybody looks up, a gigantic flag is hung out from the tower. A hush descends over the houses: all that can be heard is the chiming of the bell and in the distance the drone of the car driving away through the village. I follow the crowd as far as the church doorway, then disappear as quickly as possible along the side of the little graveyard to the back of the Sunday school. A bit further on I find myself standing in the quiet street and without hesitating start running quickly across the bedecked road in the direction of the canal.

Walt is standing on the bridge as if he knew I was coming. He is hanging over the parapet and straightens up when he sees me, calm and relaxed, a lifebuoy to which I can swim.

‘Good morning,’ he says and kicks a pebble neatly into the water. He presses my head to his chest as if he were my father, then crouches down and lifts me up. ‘Hungry? Eat?’ We are walking to the field with the tents, his boots making a hollow sound over the water. Outside the biggest tent, several soldiers are sitting at a long table, listening to crackly music coming from a small radio, Walt sits down at one corner and pulls me down beside him.

‘Jerome,’ he says and turns me to face the men as if he were selling me. They mumble something and someone taps hard on the table with a spoon.

‘Eat,’ says Walt and pats his stomach.

I keep my eyes trained on the table-top and listen to the voices and the sound of the dinner things. Will the two soldiers from yesterday be there as well? Sun, music and the smell of hot food. I am handed a metal mess-tin and walk with Walt to the tent where somebody shovels a helping of crumbly, snow-white rice into it. ‘Enough?’

A young fellow with gleaming, freckled arms ladles a thick sauce with a sharp smell over the rice. I swallow and suddenly feel terribly hungry: American food, soldiers’ food, I really seem to belong here!

Staring into my tin, I start to spoon the food into my mouth, but I am so overwrought that I can’t taste anything, everything burns on my tongue and in my throat. Just so long as they don’t look at me. I can feel Walt’s knees against my leg under the table, pushing gently but insistently. The touch is electric. Carefully I edge my leg away from his.

Rice, chunks of meat, raisins, voices I do not understand and the sun shining in my eyes. My head swims. His leg is touching mine again, shaking gently as if it were laughing. Quickly I take a sip from a small mug Walt has pushed towards me and hold back a shiver. A soldier on the other side of me starts talking to me — slowly and emphatically. I try to listen to the patient voice steadily repeating the same words and short phrases as if I were at infant school, but all I am really taking in is the pushing knee demanding my attention.

I can still hear the bell in the distance. Now they are sitting in the church, I think, and feel a pang of regret. Am I going to be able to get away from here soon? With a shock I recognise the blond soldier who has come up and is standing next to Walt. They are reading something Walt has taken out of his pocket. Seeing the soldier’s hand on Walt’s shoulder makes me feel uneasy.

A bit later we are walking through the tents. Walt has placed a hand on my neck and is using the pressure of his fingers to steer me while he talks to the blond soldier. I am like a dog on a lead.

They stop by a small tent in which a man is lying down writing something. The blond soldier goes and sits beside him and unties his boots. ‘See you,’ he says. We walk to the last tent, at the very edge of the field. Suddenly I feel sorry that the other soldier is no longer with us. Walt seems lost in thought and says nothing. Swallows are darting over the grass and the sun feels warm on my back; from a long way behind us a monotonous voice is talking on the little radio.

Walt throws the tent flap open and crawls in. I crouch down: it is dim inside and smells of oil, like the sails of our boat.

‘My house,’ he says and rolls out a sleeping bag. I sit down on it when he points and see a rifle lying next to it. He takes a small book from a bag lying at the back of the tent and hands it to me. Photographs, some in colour, have been stuck in it behind transparent, shiny paper.

‘Me,’ he says, and points to a face staring tensely into the camera among a number of other boys’ faces. Can that skinny young boy really be the man sitting next to me? I bend over closer to get a better look.

The soldier puts an arm around my shoulder. I catch an immediate smell of metal. ‘School,’ he says and bends his head close to mine. ‘Me, school.’ I see the same boy at the edge of a swimming-pool and again with a dog on a lawn. Then suddenly I realise that the boy is obviously Walt. He is sitting on a floral settee beside a beautiful woman with a big, smiling mouth, their heads leaning affectionately together. I look carefully: who is she, this woman who is so obviously fond of him? His mother? She could just as easily be his sister or his fiancée. With her red lips and her blouse with its large open collar, this woman is smarter and more beautiful than our queen in the pictures I saw back in the village. Her hair falls neatly in glossy curls around her face.

I feel empty and disappointed: I look at all the people I do not know but who belong to him, who are his friends and more important to him than I am; the people in the photographs, and the two soldiers, too. I turn the pages automatically: Walt in a garden with the same dog — and even the dog bothers me — and with another boy, their arms slung amicably round each other’s shoulders. I turn the page quickly. Walt crawls across the tent, takes off his boots, sniffs at his socks and puts them outside on the grass.

When I close the book I realise that he has undressed. He is folding his clothes deliberately, wearing nothing but the shorts I saw him in on Sunday behind the sea wall. Opening a letter, he lies down next to me, scratches his knee and reads. I quickly open the album again and pretend to be looking at photographs.

A buzzing fly bumps along the tent wall, falling down from time to time on to the sleeping-bag. Time passes slowly, the letter rustles between his fingers and now and then he gives a long drawn-out yawn. A small card falls out of the album, a tiny photograph of Walt as he is now, with short hair and pinched, sunken cheeks, ‘Narbutus, Walter P.’ written under it in printed letters. So that’s what his name is.

Walt snatches the little card from my hand, pushes it back into the album and folds his letter up. As he touches my hand, I notice that my fingers have gone numb and are ice-cold, as if they are about to snap in two.

‘So?’ he says and adds a questioning, ‘Now what?’

He lies down and touches my knee. I can see the gentle curve of his back and the breath moving his stomach. I don’t have to be afraid, he is a nice man, anyone can see that. Gently, he pushes me over backwards.

Narbutus, Walter P., I think. I must remember that for later, then I can write to him.

He grips my hand and my heart throbs in my ears: everything has gone too still, why doesn’t he say something? When I swallow I have the feeling it can be heard all over the tent. I grope sideways with one arm and touch the cold barrel of the rifle. The fly buzzes up the slanting walls and I can see a patch of grassy field and the cloudless sky through the open tent-flap.

The soldier sits up and, reaching across me, tries to tuck the letter into a pocket in the side of the tent. As his body, leaning over, edges further forwards, I can see that his shorts have got stuck and that the naked spheres of his buttocks are emerging from the bunched-up material. I can touch them if I stretch out my hand.

He is lying there with a bare bottom, I think, his pants are coming off and he hasn’t even noticed.

When the letter has been put away in the side pocket, he lifts his body until he is arching over me like a bow and looks at me from under his arm. He gives his backside a few resounding slaps, takes my hand and repeats the action. The contact with the softly moving curves makes my hand begin to glow, a burning sensation as if his bare skin is contaminated with some mysterious substance.

He drops down and moves closer to me, propping his head up on his hand. Deliberately he unbuttons the top of my shirt, slipping his hand inside under my vest, until it meets the top of my pants. My heart beats wildly, I seem to be spinning over backwards, tumbling deeper and deeper. Don’t be frightened, nothing will happen: but my heart hammers furiously, ready to burst.

‘Okay, Jerome. No problem.’ He pulls his hand back.

The hairy place in his armpit is close to my face. He uses his upper arm to wipe away a drop that is running down his ribs. Metal, the smell almost stupefies me. He places his lips on mine, I remember that from before and obediently open my mouth in the shape of a soundless scream. He is sucked fast to me and conducts a scrupulously close examination with his tongue. I dare not resist, he is bigger than I am, and stronger. He has a gun.

He speaks words I cannot understand, repeating the same sound over and over, and draws wet lines across my face with his tongue. Outside everything is still, the fly buzzes monotonously in the tent and further away someone whistles a tune. Walt fumbles clumsily with my clothes, pulls my vest out from under my belt and pushes it up. A button snaps off, making a tiny sound as it rolls to the side of the tent.

Suddenly he stops, lifts up his head and listens: voices are coming closer. What if they are people from the village, what if Hait and Mem missed me at church and have come here to look...

Walt crawls to the front of the tent and I breathe freely again. It’s over now and I’ll be able to go. But I am wrong: carefully he draws the tent-flap tight, shutting out the stretch of green field and the summer sky. On his knees he turns towards me and pushes his shorts down his legs.

What naked bodies I had seen up to now had always appeared in a flash, during embarrassed quick fumblings in the school changing room or at the swimming-baths. All I ever got to see were glimpses of lean, bony bodies, angular knees or part of a skinny ribcage hastily covered with a piece of clothing. Only Jan had been different, at the Cliff, a pale and fleeting memory.

The soldier kneeling over me is a collection of threatening shapes — shoulders, thighs, neck, ribs and arms — under which I lie imprisoned.

At night, on our street, we had sometimes talked about grown-ups, about what they do in bed. We would speak about it in excited whispers and then choke with laughter. Now this man is shoving my pants down, tugging at my vest and running his hands over me, touching me with his big fingers. My heart is like an overwound spring about to give one fatal bound and break. I push him away from me and try to say something, stumbling over my words. The raised-up body of this stranger is a grotesque reflection of all our whispered juvenile confidences, a feverish dream in which things swell to a monstrous size.

He spits into his palm — in a flash he has become a mirror image of Jan — and runs his hand between my legs. Then, warily, as if I might break, he starts to lie down on top of me, a building collapsing above me, a loosened rock about to crush me in its fall. He leans on his arms, his eye right above me, and gives me a reassuring smile. As he reaches down with one hand, he says, half-whispering: ‘Sooo...’

I feel something — his hand? — making a smooth unchecked dive between my tightly squeezed legs. He slides down and in one short movement squeezes all my will power out of me. His arms seem to hold me in an embrace, but he is involved in a grim battle, snarling and straining convulsively as his breath comes fast and furious and his bristly hair brushes against my face.

When he raises himself up on his knees he looks past me as if I do not exist. Much as I once watched the hands of a doctor reaching for the gleaming instruments with which to snip the tonsils in my throat, so I watch his movements now. He spits large gobs into his hand, and then resumes that frantic activity of his. Why am I making no sound or protest, why am I not screaming out loud?

Suddenly he bites the top of my arm hard with knife-sharp teeth, and then I do give a twisted scream, tears burning in my eyes. The soldier has a red smear across his chin. Annoyed, he puts a hand over my mouth and makes a hissing sound.

His fingers smell of tobacco and iron. He is cutting off my breath and throttling me slowly, a great roaring pressure in my ears, a swelling-up of noise. Then he heaves himself up a little and something shoots across my body, running on salamander-like feet over my belly and my ribs in a thin line. As he collapses on top of me our bodies give a strange squelch, like a wet foot in the mud.

His voice is against my ear, whispering words: I can make out my name. A hand travels groping down my belly: you have to pull, and to push... Embarrassed, I draw my legs up, I want to lie still like this, I don’t ever want to move again, ever see anybody again. He kisses me and looks at my arm. I feel no pain; I am shaken and drained, and dirty. The fluid between our bodies is turning cold and sticky. ‘Jerome, okay?’ Sweet and gentle. He pulls my vest down, there are damp patches on it. He wipes a cautious hand across my wet skin, pulls me up and puts my arms around his hips. I tremble, first in my hands and elbows, then all over my body. He pulls my pants up, fastens the buttons and buckles my belt, touching me all the time with his lips.

I crawl to the front of the tent. ‘Wait,’ he says, ‘tomorrow, swim.’ He picks up a piece of paper, writes something on it and puts his hands up, ten outspread fingers. ‘Tomorrow, yes?’ He is sitting with his legs apart, and with his finger he picks up a thin thread running from his sex to the sleeping bag and wipes it across my mouth. Then he kisses me, I shudder, the foulness of it all, all the things I was taught never to do... He pushes me towards the opening of the tent: ‘Go.’

The light outside is blinding: ditches, farms, the road. I can hear him coughing inside the tent. It’s as if I had never been inside: all over and done with, the cough just that of a stranger. I walk past the tents. Soldiers lying in the grass give me an unconcerned ‘Hello’. Do they know what happened inside the tent, do they think nothing of it?

I stand still on the bridge from where I can see the flags in the village: a great day. I look for his tent, the last one, a small green triangle in the field, peaceful and remote. No movement, no feelings, nothing, only the piece of paper in my hand. When I walk on, the wet patch on my shirt feels unpleasant against my skin. I pull my stomach in as far as I can.

Hait is sitting by the window, a shadow looking out into the colourless night. Now and then he rubs his foot and says under his breath, ‘My oh my oh my...’ It has a contented and reassuring sound. The family is safely back in its home again, the celebrations over for the day. We have been eating as if we were dying of hunger: white and rye bread, bacon, milk. Now we are waiting for the bar of chocolate that lies invitingly in the middle of the table, I keep seeing eyes straying towards it, but nobody will touch it and the mysterious object remains intact. My arm is burning; a small dark red stain has appeared on the sleeve of my shirt. Every so often I try to touch the spot and soothe the burning without being noticed.

‘That was a lovely celebration,’ Mem says, ‘and a lovely sermon, I haven’t heard the minister preach like that for a long time. And so many people, they were spilling out of the church.’

Pieke leans against my chair and rocks gently to and fro. Darkness is spreading without a sound and enveloping us.

Diet puts the enamel teapot on the table and hands mugs around, the steam from the pot rising in grey wisps.

‘Are we going to have a piece of chocolate now?’ asks Pieke.

The soldier crouches over me like a beast, threatening and watchful. Frightened, I look at Hait.

‘You’ll have to ask Trientsje, the Americans gave it to her. She must decide.’

He moves his chair closer to mine at the table. Still I see the soldier, naked and large.

‘Ask, and it shall be given you, knock, and it shall be opened unto you,’ says Mem. ‘Asking, we’ve been doing that for a long time, haven’t we, Wabe? How often didn’t we ask God for an end to the war. And now it’s here!’ She drinks her tea with small, careful sips.

The finger moves from his prick to my mouth and rubs along my lips. I feel sick and swallow hard.

‘How did the minister put it again, that we have opened our hearts to our liberators, that they are guests in our souls?’

‘Be not forgetful to entertain strangers, for thereby some have entertained angels unaware,’ Meint says fervently.

I squeeze my legs together so as not to feel that movement between them any more, and tense my belly. I must think of something else. Hait looks at me. ‘And you, my boy, did you have a good day?’

I slide down from my chair, glad that I can move about freely, and go and stand beside him. He breaks Trientsje’s bar of chocolate into pieces and neatly wraps what is left into the silver paper. I make for the door.

‘I’m going to pee.’

The darkness outside descends upon me, an on-rolling ocean of muffled sounds; a voice calling in the distance, the short nocturnal cry of a lapwing.

The sheep are huddled against the wire fence in a grey clump, making human sounds, mumbling and coughing. In the distance are the small glowing spots of illuminated windows.

‘God,’ I say, and hope he can hear me, ‘let the soldier be my friend, let me always be with him. And don’t let anyone ever find out.’

I stand there, the cold spreading upwards from my feet. ‘I shall always go to church and always pray, I promise You. Please make him take me along with him...

I can hear a door opening in the house and a gust of speaking voices that wafts out and is quenched.

‘Jeroen?’ calls Trientsje.

I say nothing and crouch down in the dark. I put my hand inside my shirt and gently rub the sore place.

She takes a few steps outside and looks around the corner of the house, then steps into the cookhouse.

It hurts, I think, but it doesn’t matter if he stays my friend.

Suddenly I feel happy, he is probably lying in his tent right now thinking of me, or perhaps he’s at the table with the other soldiers, writing me a letter.

I feel the piece of paper in my pocket and clench it in my hand. Tomorrow he wants to go swimming with me, I shall be seeing him again, and I shall no longer be frightened. He is a liberator and he has chosen me, perhaps with God’s help. Thank You very much, God. With audible wingbeats a silent shadow glides low across the meadow. I run to the ditch and look towards Warns. There is the bridge, there are the tents. I want very much to shout out into the dark over the still countryside, or to kneel in the grass, to do something.

When Mem calls me in an annoyed voice, I act surprised and innocent.

Shivering in the sudden warmth I get undressed and leap into bed before Meint has a chance to see my stained shirt and bruised arm.

I stare at the wall. His naked buttocks are so shamelessly close to my face that I hug them tight, a redeeming round shape that keeps me afloat...

The blood rises up inside me, beating in my throat and through my lips. Vaguely I hear Meint climb into bed and shut the little doors as quietly as he can.

Say nothing now, and don’t disturb my dreams....

## 5

I HARDLY SLEEP that night. Every time I turn over I wake up with the pain in my arm, thinking of the soldier. A small shutter falls open in my head and I have a clear vision of all those things he did with me, as if it had all only just happened. Afterwards I can no longer close the shutter, I feel so guilty.

You must not kiss, kissing isn’t allowed, no one had ever kissed me, except at home, quickly and softly on the cheek. And there he had lain on top of me, naked, something that was completely forbidden. Suddenly I feel afraid: if I don’t go to meet him tomorrow, will he come here and take me prisoner?

My arm burns and my back is wet with sweat. I crawl over Meint to get out of bed and go to the privy. I pause at the back door; there are figures hiding everywhere in the dark, holding their breath, but I can sense their watchful presence as they lurk in the dark, waiting to pounce on me. I flee back to bed where Meint mumbles protestingly and turns over with a groan.

If you kiss a person, it means that you like him: Walt had held me tight in his arms, as if he wanted to squeeze the life out of me. Why did he do that, he didn’t even know me... I shut my eyes and try to sleep. Don’t think of ‘that’... Is it bad if someone goes stiff down there? Diet says it’s a sin to talk about such things, but when it’s to do with an American it surely can’t be a sin; they only do what’s right and proper.

Just make the burning in my shoulder stop. He comes sneaking down on top of me and grins as he hisses at me to keep quiet, because no one must hear us. His hind legs are hairy and his tongue hangs pink and long from his snout. He is a dead cat...

<br>

I wake up with my head swimming. The mere thought of having to get up fils me with despair. Meint is already out of bed, yawning and stretching exaggeratedly, but curled up motionless I sullenly pretend to be fast asleep. I shall say I am sick so that they’ll let me lie in, life outside this bed seems bleak. There is nothing I want any longer.

In two minds I pull the blankets over my head: the hard patches on the bottom of my vest and the pains in my arm are problems I don’t want to face. But Mem flings the cupboardbed doors open and whips the blankets off me. ‘Up you get, boy, this isn’t a holiday camp.’

<br>

Hait and Popke are still sitting at the table. It’s like a Sunday morning, everyone up late after the celebrations and still at home. Trientsje helps Mem clear the dishes and looks at my pale face.

‘Well, poppet,’ she says, ‘the celebrations are over now, so it should be back to work again. But the farmer’s going to have to do without me this morning. If all of you have been let off school, I don’t see why I can’t skip a morning as well.’

The water is ice-cold on my face and when Trientsje is not looking I push a wet hand inside my shirt and place it on my arm; it feels as if needles are being stuck into the wound. Mem is in a bad mood and sends all the children outside. She can’t seem to cope with the irregularity of this free morning. I have a slice of bread pushed into my hand, ‘Here, go and eat that outside.’

Hait strolls uneasily with Pieke along the fence, a free morning and no church! Meticulously he plucks tufts of sheep’s wool from the barbed wire and kneads them into a ball. I chew on my bread, my throat tight. There is the clattering noise of something falling inside the house, followed by raised voices. Jantsje rushes out sobbing and disappears into the barn. The celebrations are playing on all our nerves now.

I walk to the back of the house where the wind is less strong and fling my buttered bread across the ditch. The folded piece of paper lies in my trouser pocket, fingered so much that it feels as soft as a piece of cloth. I want to read it, I want to see those letters again, that secret message, and feel it between my fingers like a priceless possession.

Meint is crouching by the ditch parting the duckweed with a stick. The sun sends a shaft of light right to the bottom of the still, black water where a water-beetle is moving about amongst the weeds, its little legs furiously thrashing. ‘It eats tadpoles,’ Meint whispers, ‘we’ve got to kill it.’

I poke the water. The duckweed folds up over a small crimson creature that wriggles down towards the bottom in agitated circles.

‘We’ve got to find something to keep frogs in,’ says Meint, but I go off to the wooden privy without helping him.

‘Wednesday, 10 o’clock,’ and the thin lines with the letters L and W and the small cross showing where we are to meet. It looks like a capital T, the downstroke is the road from Warns to Laaxum and the cross-stroke must therefore be the dyke. Ten o’clock, how much time have I got left?

Meint bangs on the door. ‘Haven’t you finished your shit yet? I’m off to look for a tin at the harbour. Come on!’ Through a crack I can see the deserted, sunny road to Warns. I push my pants down, sit on the round hole in the plank and hope Meint will go off to the harbour by himself. I pick at the crusts on my stomach. My life is like a funnel that gets narrower and narrower, constricting me until I can’t escape. I tear up the piece of paper and let the snippets flutter down through the hole into the stinking pit below. Meint puts his face to the crack and makes spluttering noises with his tongue. ‘Shithouse,’ he laughs.

Crossly I go back into the house. ‘When is dinner?’

Mem laughs. ‘You’ve only just had your breakfast, we don’t keep going non-stop, you know.’

I look at the clock: a quarter to nine. Another hour.

Down at the harbour I keep my distance from Meint, who is hunting about among stacks of crates on the other side. I must try to slip away unnoticed, which means hiding myself when he isn’t looking. My legs, dangling over the quay wall, are reflected in the water. The heat is oppressive and there is no one else in the harbour. Now and then the door of the shack bangs shut, breaking the silence like gunfire. Meint gives a shout and triumphantly holds up a tin can which he has fished out of the harbour with a stick. Thick blobs of sand drip out of it.

‘Got it! Come on, now we can go and catch some frogs.’

If we go back home I shall never be able to get away. I take the tin and, lying on my stomach, scoop up some water with it. ‘It leaks, it’s got a hole in the bottom. It’s no good.’ I try to fling it into the water, but Meint snatches it out of my hand.

‘Don’t be stupid. I can easily mend it with a bit of tar.’ He makes for the shack and disappears behind the banging door. Instead of following him I race past the little building up a grassy slope, drop into a hollow and press myself close to the ground. Meint calls out, first round the back of the shack and then towards the side where I am lying hidden. ‘I can see you. Come on out, I’ve mended the tin.’

Why don’t I go along with him, catch frogs and play by the ditch? I have to stop myself from raising my head. When I hear the sound of his clogs. I peer through the grass and see him disappear behind the dyke. Doubled over, I take to my heels and run across the bare, open fields towards the Mokkenbank. I don’t know if I’m too early or too late, but I keep running, away from Meint, away from the frogs, away from deciding whether to go back.

The sandbars of the Mokkenbank lie grey in the advancing and receding waves. Overhead the gulls climb like whirling scraps of paper, hover in the wind and dive down. I clamber on to a fence and look around: not a soul in sight. Straining my eyes I scan the line of the distant sandbars; maybe he is there already, lying in the sun or taking a swim.

On the other side of the fence I stop to scrape the mud from my clogs. Isn’t he coming, or has he already been and gone? The sun is broken by the sea into dazzling splinters of light. Go and sit down at the foot of the dyke and shut my eyes tight against the glare. A cricket makes bright little chirping noises beside my ear, the sound becoming thinner and thinner, a wire vibrating in the sun.

I wake up when a pebble plops into the grass next to me. Walt is lying close by, looking at me. I feel dizzy. The earth is filled to overflowing with sounds, all nature seems to have sprung to life. Walt says nothing, he just whistles — I remember the tune — and turns over onto his back. I had thought the two of us would rush up to each other, that it would be like yesterday and he would be pleased to see me.

A little later we are walking through the marsh grass towards the sea. He carries me over a swampy patch, taking long strides and pressing on my burning arm. I am suspended in the air, without speech or will. Where is he taking me? Suddenly he stops, turns round and pushes me down roughly in the reeds. Two cyclists are riding along the dyke, I can hear the squeak of pedals and snatches of conversation in the distance. ‘Sssh. Don’t move,’ he says and pushes me further down. ‘Wait.’ He watches the approaching cyclists, his hand patting my knee reassuringly. ‘Kiss me.’

We stay there crouching for a moment longer, then he pulls me along to the beginning of the sandbar and goes and sits down in the cover of a circle of reeds. He holds me at arm’s length as if looking me over, brushes the hair from my forehead and tweaks my nose. He pulls off my clogs and draws me between his knees, pursing his lips to kiss me, a dry mouth against mine. He pulls his clothes up a little and puts my hand on his bare waist, a warm curve under my fingers.

‘You happy? You like?’ He has closed his eyes as he talks to me. I keep my hand nervously where he has put it, but his ‘hand lies softly kneading between my legs. My body gives answer, I can feel it move under his touch and stretch out in his hand, and I turn away to one side.

‘Jerome, come on.’ He speaks gently, as if whispering to me in his sleep, and pushes my mouth open. I let him do it, but remain tense under the squeezing and kneading of his hand. ‘Sleep,’ he says then, takes off his jacket and kisses the hillock in my trousers. Humming softly he lies down next to me; I listen: everything he does is beautiful and fascinates me. When I place my arm on his he looks at me in surprise.

The sand turns cold and wet under me, as if the sea were seeping into my clothes. He has stopped moving, his head has drooped to one side and his breath is heavy and deep. Above us the gulls complain and call.

I look into his defenceless face, the mouth that has fallen open slightly and the small white stripe between his eyelids. Every so often he makes a sound like a groan. He’ll take me away with him, I tell myself, if I don’t hear from home I’ll stay with him, he’ll wait for me in his car and then we’ll drive away to his country.

His cracked lips, the hollow sloping line of his cheeks, the eyebrows that almost join and his strong round neck, I pass them all in review, taking them in carefully, the colour, the outline, every irregularity, every feature: I must never forget any of them!

I slip my hand cautiously back under his shirt. He opens his eyes and looks at me in surprise, as if wondering how he has landed there, on this sandbar, with this boy and in this situation. He makes a chewing movement and swallows audibly.

I would like to say something, to talk to him; the long silences are oppressive and each time add to the distance between us. With a moan, he moves closer to me and I bury my face in his sandy hair. He folds his cold hands between our bodies.

Why does he keep falling asleep? I had thought he would do the same as yesterday; I was frightened of that, but now that he doesn’t I am disappointed. I think of Amsterdam: will I ever hear from them again, from my father and mother? What if they are dead, what will happen then?

I feel cold and tired; I ought to get up and go home or else I’ll be late again, but Walt is sleeping peacefully, like a child. Now and then there is a rustle in the reeds as if someone were moving behind us and I lift my head quickly to look. The waves make startled sounds against the shore, over and over again the glittering water sucked into the sand.

Time passes, why doesn’t he move? A small beetle runs across his hair, trying laboriously to find a way through the glistening tangle. Then he is awake and scratches his neck.

‘Baby.’ He looks sleepily at me.

I am no baby, I am his friend. He looks at his watch, gets up with a start and pulls me to my feet.

‘Go,’ he says and gives me a gentle push. ‘Quick.’

It feels like being sent out of class at school. I slap the sand from my clothes and slip into my clogs.

He doesn’t remember, I think; he’s forgotten what we did yesterday. And he probably won’t be taking me away with him, everything is different from what I imagined. ‘What about tomorrow?’ I want to ask, but how?

He sits down by the edge of the sea and lights a cigarette. No kiss, no touch?

When I am standing on the dyke he is still sitting there just the same. I want to call out but a hoarse noise is all my voice will produce.

<br>

The rest of the day seems endless. In the afternoon we go down to the boat with Hait and help him bail water out of the hull. I do the monotonous job of emptying a tin mechanically over the side of the boat, filling the tin with a regular movement and listening to the dull splashes beside the boat, time after time, first Meint, then me, in endless repetition. Meint keeps talking to me while Hait gives us directions. I pretend to listen. They mustn’t notice anything. I mustn’t ‘give them the slightest inkling of suspicion, but must act as if ‘nothing is happening. I speak, I eat, I move about, I bail water out of the boat, I answer Hait and make jokes with Meint, everything as usual...

But later, doggedly, I run a little way back up the dyke and scan the horizon. Clouds of white gulls rise brightly against the darkening grey sky: I can hear the far-off screeching very clearly.

I stare intently: that’s where he was, that’s where we lay...

<br>

‘Give us a hand, come on,’ says Diet and pushes the breadboard into my hand, ‘don’t just sit there daydreaming.’ The evening meal is over and I help her clear away. ‘You must have met a nice girl at the celebrations yesterday. I can tell by just looking at you.’ It sounds like an accusation.

I pretend to be indignant: me? In love?

‘Don’t try to deny it, it’s nothing to be ashamed of!’ She throws her arms around me aggressively, sticks her head out of the door and shouts with a laugh, ‘Hey, boys, Jeroen is courting, he’s going to take a Frisian wife!’

<br>

I find a big marble in the grass and walk back towards the harbour. The sun is low and deep red between long strips of cloud and the grey walls of the shack have taken on a pink glow, as if lit up by fire.

I go and sit beside the little grave among the stones. I push the marble into the sand, a beautiful one with green and orange spirals running through it. Over towards Amsterdam the horizon is a bright line. Will a letter ever come? How will I ever find my way back to them?

## 6

THE SAME HOUSE. It stands hidden between the trees at the end of the overgrown path. I recognise it at once: this is where we were.

The engine is turned off. The soldier gets out cautiously and walks towards the garden behind the small building, then disappears around the corner. A moment later he hurries back, his feet crunching on the gravel path. ‘Quick,’ he takes my hand and pulls me impatiently to the door. It feels pleasantly cool in the shade of the house after the hot car. I take a deep breath. The cry of a bird echoes clearly and challengingly among the trees.

Before putting the key in the lock the soldier listens out and looks back at the road a few times. We stand like thieves outside the deserted house. The turning of the key seems to break the spell, shattering the stillness.

People are bound to hear us, I think, someone will come.

He pushes me into the house and immediately locks the door behind us. Inside it smells of damp wood; subdued light filters through the windows. We stand motionless and listen. I feel his hand touch my cheek. When I look at him, he nods reassuringly, but I can see how tense he is.

<br>

Meint had not gone back to school in the afternoon but had gone to help Hait with the boat. Mem had looked surprised when she saw that I wanted to go with him. ‘You can learn a lot from Hait,’ she said. ‘Not the things they teach you at school, but they’ll come in very useful later on.’

On the road to Warns I had let Jantsje go on ahead, thinking that with a bit of luck I would be too late for school. I took a quick look down the village street: was there anybody left outside or had they all gone into school? If no one was about then I could take my time about deciding what to do next. Perhaps there was some trace of the soldier somewhere, a car, something.

I noticed how oddly I was behaving, stopping, looking around, and then taking another few steps: why was I being such a fool, I really ought to be at school as usual. Then I heard a voice calling my name hoarsely, followed by a short whistle. Immediately I was torn between wanting to run away and looking back, and as I walked on I was surprised at myself: this was the sound I had been hoping for, that I wanted to hear, and here I was making off as if I was scared.

‘Jerome!’ Walt is waving to me. ‘Come.’

He is standing next to the Sunday school and starts coming towards me. We walk down the street out of the village. But I really ought to be at school...

‘Sit down.’ Hurriedly he pushes me onto the verge. ‘Wait.’

I make myself small, slide down towards the ditch where the grass is taller and watch him walking back along the road until he disappears around the bend in the village. I could get up now and run to school. I could say that I was late because I had a sore foot, and yet I do not move at all, the suspense ringing in my ears. Supposing I go back and run into him, supposing he sees that I’ve been disobedient; he could easily turn up suddenly in the classroom, point to me and order me
outside, in front of the whole school.

The time that passes seems like an eternity, I could have run back to school a good five times over by now. My indecision grows: perhaps he won’t ever be coming back.

Then I hear a car in the village, a reassuring purring that is coming closer. A moment later, the car rounds the bend and makes straight for me. The door swings open, but instead of getting out Walt calls something that sounds curt and impatient. As soon as I am inside he accelerates and we are swishing along the road. He takes hold of my arm without stopping driving. I can feel a small tin in the warmth of his trouser pocket, keys, a few coins.

I can’t see the road at all. We are driving fast and it frightens me. I keep my hand on the hard place which every so often moves upwards.

I could easily have gone back to school a good five times over...

<br>

Walt walks further into the house and opens a door, I can hear him drawing curtains somewhere. There is a desk with a row of drawers down one side in the corner of the room, and under one window a sagging brown settee, a small round table and two wooden chairs with grotesquely bulging seats. A calendar is hanging behind the desk and several sheets of paper have been stuck to the wall with drawing pins. The ashtray on the small table is full of cigarette ends. Does that mean there are people about? I listen.

An imprisoned brown butterfly flutters against one of the windows, its wings rustling across the glass. The soldier comes back into the room and pulls the shutters at the front of the house across the windows; there is a rattling noise and it is dark. Then he shoots the bolt of the door with a loud click and it suddenly dawns on me: I am a captive, I shall never get away from here.

A narrow staircase at the back leads up to a landing with three doors. We go into a small room which has a mattress on the floor and some blankets flung down on to it rather untidily. Walt sits on a chair, pulls me towards him and kisses me. I allow it to happen, unyielding and resentful: I have been locked up, there is no one here but the two of us.

I can watch the slight movement of the leafy branches through a small window. I shall never see anybody again, this is the end. Yet somewhere among the shaded leaves the same bird is still singing, clearly and challengingly.

The soldier has gone out of the room. I am alone. What is he up to, is he going to leave me all on my own? I prick up my ears for any sound. It is like being kept in after school, being aware of the silence and emptiness in your body, the injustice of being the only one left behind in a deserted room. A tap is running somewhere, making a gushing sound through the house. ‘Hey, where are you?’ His voice sounds normal, he hasn’t sneaked out of the house after all, hasn’t left me behind, hasn’t gone for his rifle to threaten me with but is standing in the room next door, his back towards me. He has put his clothes in a pile in a corner of the little passageway and he is washing himself at a basin, the water running down his body on to the floor.

From his hips to halfway down his thighs his body is as white as chalk, the dividing line running straight and clear-cut across his skin. His movements are quick and nimble, his arms slipping over his body, sliding from his back across his knees to the half-raised foot. The foaming white soap on his belly smells sweet. He rinses himself clean with quick handfuls of water, then he takes off my clothes without saying a word and hangs them on the door-knob.

I catch my breath as he runs a cold, soapy hand over my shoulder-blades and ribs. His knowing fingers handle my terrified sex deliberately, then slide across my belly and my buttocks.

When he touches my injured arm I can see that he is startled.

‘Me?’ He points to himself.

I shrug my shoulders and shake my head, I am afraid to accuse him.

He examines the wound and touches it with anxious fingers. ‘Sorry,’ he says. ‘I am so sorry.’ He feels around the sore spot carefully as he brings his head close to my ear. ‘Jerome?’ He shakes me as if he wants to wake me up. ‘I love you.’

I know what that means, the soldiers call it out sometimes to the girls in the village, whistling and smiling, and the girls walk grimly ahead without looking at them.

He sits down on a chair and dries himself with his shirt. It looks strange, a naked man sitting on a chair. I stare at the lino on the floor and move my foot: in a moment he will get dressed, then we’ll go back to Warns. I take a look through the door to where my clothes are hanging.

‘Come here.’ He wipes the wet shirt along my back, gripping me tightly between his legs so that Ic an feel his little wet hairs tickling my belly. ‘I love you.’ Again he shakes me gently from side to side. What should I say back, what does he want from me? I look at him: he is an unknown person, a stranger of whom I am afraid, to whom I can say nothing, of whom I can ask nothing. Thin and miserable I continue to stand between his rock-like limbs until he pulls my head backwards and forces me to look up at him... ‘No problem,’ he says, ‘no problem.’

He has stood up and is holding that soft thing, from which I have scrupulously averted my gaze, right in front of my face. I feel him push it carefully against my tightly compressed lips and turn my head away. When I start to make for the door, he grabs hold of me and pulls me back.

‘Is okay, Jerome. Okay.’

We fall onto the mattress. His body is hostile and hurts me. It is nothing but parts that stick out and that burrow and thrust and force themselves upon me. We are having a gigantic wrestling match, a painful performance accompanied with jerkings, twitchings and torrents of low, panted whispers.

He rolls me over him, kneels above me, turns me on to my stomach and licks my body like an animal. Then all of a sudden he interrupts the wrestling, lifting up a corner of the mattress and feeling about with his hand. A small metal lid falls to the floor; I sit up and look at it. He rubs a smooth, cool finger between my buttocks as he says unintelligible words in a gentle, soothing voice. He strokes and cuddles me, he caresses me, but all the while one hand continues to hold me tight in an iron grip. With my forehead pushed into the mattress I let him do what he wants, afraid to make the least sound.

‘Baby.’ It sounds like a husky, warm laugh in my ears. ‘Givem e a kiss.’ He twists my head to one side and in his hurry drives his teeth hard against mine. I cry out with pain.

‘Is okay, is good. No problem.’ His tongue moves greedily between my lips as his weight gradually squeezes the breath out of me. I try to swallow his spittle but his tongue, a ramrod inserted in my mouth, prevents me. I clench my fingers around the mattress. In the other room the tap is dripping, an imperturbable sound of falling drops, as if nothing were happening in here.

I arch my back and tense my legs, his thing prodding my body impatiently, insistently, an unimaginably coarse and blunt instrument trying to make an opening into my body.

...We are sitting in a small circle at the corner of the street. It is evening and already dark. ‘When we go to the country tomorrow,’ one of the boys says inscrutably, ‘we must catch a frog. If you stick a straw up its arse you can blow it up.’ I have never seen anyone do that, the whole thing has remained a gruesome, unsolved mystery, but now I see the frog, transfixed and grotesque, blown up like a balloon, a cruel and horrible picture...

As soon as the pressure of his body lessens a little, I wrench myself frantically from under him and crawl to the furthest corner.

‘Sorry, baby,’ he says and gives me a lop-sided smile. A flaming sensation is shooting spasmodically through me, paralysing me. Walt moves up close and runs his finger over the spot where he hurt me, tilting his head to one side and thoughtfully scratching the corner of his mouth with his other hand. Why is he smiling at me so compassionately, does he think I’m stupid or that I’m babyish? I hold my hands to my face, ashamed. The pain is like a knife slicing me in two.

‘Easy,’ I hear him say. ‘Easy, baby.’ When he pulls my hands away I see that he looks serious. He blinks his eyes and clears his throat. ‘Come over,’ he says and pouts his lips at me.

I hesitate. He brushes his hand over my eyes and keeps looking at me, his own eyes grey and clear. Suddenly I am crying.

‘Come.’

I put my lips to his mouth.

‘Sooo,’ he mumbles, drawing it out, ‘good boy.’

Then he lays his head between my legs as if he were going to sleep, his hair soft as rabbit fur against my skin. He touches my feet, my legs, then slowly and insistently he strokes my collapsed sex. I no longer move; just so long as everything stays calm and gentle and safe.

It happens without me, the unhurried straightening of my legs and the slow, embarrassing erection. I can do nothing to stop it, Id o not look at it, just register numbly the jerking of my body under his hand.

I twist my hips round to try and screen the movements and find myself ludicrously exposed to his searching gaze. He closes his lips around the erect little shape and it vanishes into a sucking wetness.

He is going to bite, just the way he bit my arm: as soon as the thought occurs to me, it shrivels up, is gone, it’s all over.

‘Not good,’ he laughs. ‘Jerome baby.’

He draws thoughtfully on his cigarette and blows smoke into my face. Then he stubs the cigarette out on the floor, calmly and decisively. He takes hold of my chin like a dentist and presses on my jaws, puts the hard thing between my lips and pushes it in firmly.

‘Kiss me.’

It sounds loud and flat in the empty room. I clasp his legs tightly to withstand the lazy movement in and out. Then I resist no more. He has me under his control as he bends thrusting over me, while I perish in the spurting waves that cut off my breath and beat against my throat and the roof of my mouth. An opposing wave is rising in my stomach, a surf that breaks upwards at furious speed, and it is all I can do to force it back down.

The muscles of my throat contract, I gag and struggle for air. He pulls his body away and the thing slips out, suddenly slack and harmless. A sickly sweet taste seems to be sticking to my gullet with rubbery tentacles. I feel clammy and cold and shiver so violently that my whole body shakes.

<br>

Walt stretches out beside me and puts his arms and legs around me: a smell of iron, of warmth and sleep. I hear his soothing voice in my ear and with every breath his belly presses against mine. He is suddenly gentle again. I feel him putting his arms around my neck. Slowly, following his breathing, I calm down. Without being asked, I press my mouth to his neck.

<br>

I lie shut in between the wall and the protecting rampart that is his body. He has turned away from me and stopped moving. Is he asleep?

His back is a landscape, his shoulder-blades the hills, his skin a sloping field under which pale blue rivers flow. At the curve of his neck little glossy mouse-hairs grow in strangely symmetrical formations, a miniature display of toy armies on the march. The landscape and the armies move slowly with his breathing.

He is fond of me, that’s why he said, ‘I love you’.

His arm gropes backwards and pulls me closer. When I push up against his hips he makes a satisfied sound.

<br>

He picks up his watch from the floor, and takes a long look at the dial. Is it time, do we have to go? I wonder where he will be taking me; not back to Warns, anyway, that’s out of the question now, I’m sure of that. He turns over on to his back and smokes a cigarette, flicking ash into a neat pile on the floor.

My head is lying on his chest, his arm moving across me, from his mouth to the floor. The thing is lying limp and harmless on his belly, looking at me with its split eye: it is a silkworm, a docile roll of putty. Now and then Walt puts his hand over it. I study how he handles the thing and moves it about. A grown-up’s body is strange, different. It enthrals me and repels me.

<br>

He holds out both hands to me, pulls me up from the mattress and gives a meaningful laugh. But I don’t laugh back. I don’t understand his laughter: why, what is there to laugh about?

I stand close to him and feel his skin, warm and shifting like sand in the sun. When I move my hand over the little curly hairs, I wonder if he understands that I am giving him a guarded caress. Persuasively he thrusts his body against mine, so that we move together to the window. He pulls it open. The air that falls over us is crisp and smells of dry leaves. The cooing of a pigeon breaks off abruptly and I hear hurried rustling and clumsy fluttering in the branches.

Walt bends over me and looks out. It is motionless and still and yet the trees are full of chirping sounds. He rubs his body against my back, first imperceptibly and then more obviously, but I pretend to be looking out undisturbed as if I feel nothing: if I don’t notice anything then nothing will happen. The car is parked by the side of the house, large and lumbering, like a watchdog waiting patiently for his master to finish his business and to come back to him. The movements behind my back continue. It is as if the thing were an intruder, another person standing between Walt and me, an obstinate bore conducting his affairs with no reference to us. I rest my chin on my arm and look at the trees: how tiresome grown-ups can be.

‘Let’s go.’

He stands big and strong by the basin and shamelessly fingers the thing with soapy hands. He has no secrets from me and behaves as if everything were normal. I get dressed and wait patiently for him to lace up his boots. What now?

I can’t go back home: if somebody has undressed you and done things with you, then you belong to him, that’s how it is with grown-ups; you’ve been singled out, it means he wants you.

On the ground floor, just when I think we are finally leaving, Walt suddenly becomes very busy, fetching tins from a crate and carrying them to the car. I sit in the front of the car and listen to him walking about in the house, shutting the window upstairs and locking the door. Then he is sitting beside me, leaning back. Now I shall know what is going to happen, where we are going.

He smokes.

His cheek gleams in the light, I can see his nostrils quiver as if he were smelling trouble. Every so often he blows smoke in my direction by way of making conversation. The cigarette is taking an exasperatingly long time to grow smaller.

Now I want him to put his arm around me, so that I can smell his nearness. The sudden longing carves painfully through my insides. I wait for him to pull me towards him so that I can lean into the folds of his clothes.

He flips the butt outside and starts the engine.

<br>

Evening creeps soundlessly over the land. When I look out of the tent I can see the twilight slowly shrouding the distant trees and farmsteads.

I am sitting in the middle of a groundsheet and hardly dare move or touch anything, not the sleeping-bag rolled up beside the laced-up green bag at the head of the tent, nor the magazines with the colour photographs on top of the small pile of folded clothes, nor the two mess-tins, the pocket torch or the small bits and pieces drawn up in strict formation along the sides of the tent. My eyes have examined them all dozens of times now. I have taken it all in and fixed it inmy memory: this is where he lives, this is his home and these are his possessions. Everything is confined inside this small square around me, the shapes that I see turning greyer and greyer until they blend with the fading cloth of the tent.

I can hear voices in the distance, violently flaring up and then dying away again, and music from a radio. A little while ago I had smelled food and heard the familiar sounds that go with a meal: the clatter of pans, the irregular clicking of cutlery and people talking with full mouths. I had not moved and had listened holding my breath until I grew giddy trying to distinguish that one voice. Could I possibly hear it from so far away, would it reach me over such a distance?

But the conversations remained a jumbled skein of sound to which I could put neither figure or features, no matter how hard I listened. The smell of the food does not disturb me, because all the time he has been away — an hour, one and a half hours or more? — I have felt no hunger, nor have I minded the chill that has invaded the tent with the night air.

Will this be my life from now on, in tents, cars and deserted houses, waiting for him to return? Will I, as now, always have to gaze out aimlessly and bored, dreaming my life away, apathetic and without a will of my own? And will I be travelling with him from one country to the next, making war and celebrating liberations and sleeping inside this little square? Then it will be my tent, my kit-bag and one of the mess tins will perhaps be mine alone.

Do I really want that, to leave Laaxum for good, to forget Amsterdam, live inside this canvas house? But why not? Even if it is small in here, it is well laid out and private, a protected existence like a rabbit’s in a hutch.

I turn that thought over and over, chew it and chew it again.

Outside, the farmsteads have been as good as swallowed up by the dark and an alarming red glow has traced a fiery tear on the horizon. Suddenly I feel cold and when I try to move I realise that I have grown stiff.

I shiver, as if I have just woken from a bad dream. Of course it is good to be here, I like this tent and Walt is my friend. If I go away with him he will look after me well. Everything will be all right, then.

<br>

From the house we had continued along a different road; so now it was going to happen, my journey into the unknown had begun. But the start hadn’t been very pleasant; Walt had said almost nothing, all he had done was yawn every so often and then smile apologetically. I wondered if he felt the same as me. My body seemed to be a jumble of separate parts that had been pulled to pieces and then thrown haphazardly and painfully together again. Was that why he was being so unfriendly?

But then, unexpectedly, he had seized me roughly and pushed my head into his lap. Now and then his hand would slip down from the wheel and move over my face which made me feel less uneasy. Where were we going, how far had we come?’

‘Come.’ I had heard the car slow down and from his movements I had understood that I was to sit up. When Ih ad looked out I was confused, but then I recognised the tents and the camp and the bridge beyond — it was just that I was approaching them from a different angle. We had come round in a circle to the same place.

The first thing I had felt was disappointment: so we were back in Warns after all. But perhaps it was only for a short while, perhaps Walt had things to settle before we left.

We had walked to the big tent where a few bored soldiers were sitting at the table. One of them had poured us coffee and while I finished off the tepid liquid that tasted strong and bitter, gulp by gulp, I had listened to the sound of their voices and tried as hard as possible to be one of them, sitting on my chair like them, leaning on the table and drinking from my mug just as they did. The more everything was done like them, the better.

A little while later two of them had dragged a large crate from under a tarpaulin and carried it to the car. Walt had started to walk with me towards his tent, then stopped and pointed to it.

‘Wait there.’

As I walked on I saw him go towards the car, a bag over his shoulder. He stopped for a moment and waved his arm. ‘Go in’ It had sounded brusque, like an order, and obediently I had opened up the tent and slipped into the quiet, warm little space that was both tidy and empty.

Later he had come in, changed his shirt for a clean one, squatted down for a bit and then gone out again.

‘You wait. Me...’ He had mimed bringing food up to his mouth and pretended to chew.

‘Jerome wait. Okay?’

He had placed a finger to his lips and given me a conspiratorial look.

‘Good boy.’ It had sounded like praise and approval and had banished my feelings of disappointment. Even when he had been gone for a long time I hadn’t dared move, had touched nothing and waited.

<br>

When he throws the tent flap back it is almost completely dark outside. For a moment it is as if he is surprised to find someone there. Had he forgotten me or had he expected me to have gone? Then he puts down an apple for me and tears the wrapping from a bar of chocolate, rolls the sleeping bag out and sits me down on it.

The smell, the odour of metal filling the tent!

He crawls in behind me and speaks in a lowered voice while he looks for something in the dark. Stopping what he was doing he puts his mouth to my neck. But I don’t move, just sit there motionless, waiting.

He lies down beside me, breaks off a piece of the chocolate and carries it to my mouth. ‘Eat. Come on, eat!’ He is whispering and yet his voice sounds loud. I grow giddy with the sweet taste that floods through my body, with the smell of his clothes and with his caressing hand on my knee. I feel as if I am softening and melting like the chocolate between my fingers: this is the way I want to live, of course, so long as he is there to fill the tent with warmth and smells and food.

He looks in the side pocket of the tent, rustling envelopes and paper, switches his torch on and shines it on something he is holding in front of me.

It is a photograph of him standing with his arms folded across a blue check shirt, leaning against a wall. I recognise his watch. He pushes the photograph into my shirt pocket and pats it.

‘For you. Jerome, Walt: friends.’

He pulls me towards him and I disappear into his arms. Feeling a heaviness in my eyelids, I slowly start to doze off. Outside I can hear a soldier talking softly and somebody seeming to hum an answer, everything sounding vague and far away. Are we going to sleep now?

He loosens his belt, picks up my hand and takes it inside his clothes. Why doesn’t he leave me alone, can’t he see how tired I am? Will it always be like this?

Soft and curled up, the thing lies sleeping contentedly under my palm. I touch it gingerly, afraid to wake it up. The sleeve of Walt’s jacket presses against my eyes and sleep overwhelms me.

‘Don’t stop. Go on.’

The murmuring voice sighs in my ear. I am stroking it, aren’t I? Wearily I notice that the silkworm, as if scenting danger, is beginning to straighten, suddenly springing upright, like that dog I never dare touch in the village.

‘No, hold it. Move.’

I move it.

‘Don’t stop.’

I don’t want to any more, I am tired, we must go to sleep. The thing stands on stiff, threatening paws. Its upper lip is drawn back, the hairs on its back bristle and it bares shiny yellow fangs...

‘Faster, yes. Do it.’

...and utters angry and repulsive sounds.

I bury my face deep in Walt’s sleeve until he lets go of my hand and I can hear him moving about in the dark. Then everything is still.

The quietness takes me by surprise. It is as if I have been left alone in a deserted, empty room. But Walt is close by: the pressure on my shoulder is the arm he has thrown across me. Are we going to sleep with our clothes on, and without any blankets?

When I hear whispers and suppressed laughter outside I have to use great effort to force my eyes open. Walt sits up and pulls his sleeping-bag over me. The childrens’ voices are close by, I can hear their smothered sniggers. Then a corner of the tent is lifted up and I see the smudge of faces bent down looking under the canvas.

Who are they, do they know me, do they know that I am here? But it doesn’t matter, I’m leaving here anyway, they won’t ever see me again...

When Walt leaps up and shouts something at them, they run away, a flock of excitedly cackling chickens. He crawls outside and sticks a peg back into the ground, talking meanwhile to some soldiers in another tent. Then he sits down beside me and turns on his torch: one side of the tent is hanging in loose folds. We wait and listen. I can hear his breath and the beating of his heart. The crackle of a small radio comes from a neighbouring tent.

‘Okay,’ he says, ‘baby.’

He leads me in the dark along the ditch. I stumble over holes and rough ground as if sleep-walking. He holds my hand tight, which surprises me, here where everyone can see us even if it is dark.

He stops close to the road and winds my hair round his finger until it hurts. What are we going to do now, where is the car?

‘Okay,’ his catchword, ‘sleep well.’

He gives me a firm slap on the bottom and pushes me up the road.

Whistling softly, he walks back, his feet moving audibly through the grass. I watch his shadow move past the faint circles of light shining in several tents, then it disappears.

I walk to the bridge: and I had thought that I would be staying with him, living with him in his tent, sharing his mess-tins...

The village is quiet. I can see people behind lit windows.

The ditch beside the road has a black and oily sheen. If you didn’t know any better you might have mistaken it for a path you could walk on, so solid and firm does it appear. Above the dimly glowing horizon is a venomous, thin little moon, a trimmed fingernail.

What shall I tell them at home? This is the first time I have missed the evening meal.

But in my heart I know that I am never going to go back. I shall carry on walking and no one will ever see me again. I shall carry on walking until I am back in Amsterdam.

## 7

THE MASTER CLEARS his throat noisily and gives a pointed cough. I start and look at him: the cough is definitely meant for me. His eyes are fixed on me as if he can sense that my thoughts are not on the composition.

‘That’s quick,’ his voice is mocking, ‘for once you actually seem to be the first in class.’ He walks up to my desk and swivels my exercise book around: again that disapproving look. As if in disgust, he guardedly turns a few pages over and gives me a questioning look. He is wearing an orange rosette in his jacket, a favour that does not suit him.

He could be nice enough when he needed me to do the drawings, I think, but now...

He turns the exercise book back to me abruptly and stalks off to the front of the classroom. It is stifling inside the room, the heat of the sun seems to release the smell of children’s sweat and of stables from our clothes.

I bend over the paper and try to construct a sentence. I can still feel the master keeping an eagle eye on me and I break out in a sweat which dampens my hair and then starts to trickle down my back. Grimly scratching my head, I write two words. ’...I walked...’ My hand falters, again the pencil is suspended lifelessly above the white paper. All around me pencils are scraping steadily away and now and then a page is turned over with a rustling sound. The boy beside me raises his head: ‘Finished, master.’

I peer over at his exercise book and see two pages covered with writing.

‘When we heard the war was over, I walked...’ It looks as if I shall never get beyond these few words and I read the unfinished sentence over and over until I feel I’m going mad. I run my finger over the W I have scratched with a nail into the corner of my desk: there is nothing I need write beyond that W; it is my whole liberation story.

I can hear the loudly tapping feet of a bird hopping angrily to and fro in the gutter. After a short silence, seemingly to catch its breath, it starts to whistle: gently swelling notes that turn into excited twittering, now high, now low, as if it is choking on its own passionate sounds.

I lean my head on my hand and shield my eyes. The pencils continue to write. Whatever can they all have to say? I look for a handkerchief and blow my nose. It presumably wouldn’t do to burst into tears about a composition that refuses to come.

When the master collects the exercise books he leaves mine untouched.

‘I am curious to find out what you have to say about the past few days, to read what is bound to be a wonderful record of this unforgettable time.’ He coughs solemnly. ‘From now on everything is back to normal, the celebrations are over, but when, in the future, you re-read your compositions, these days will again shine bright before you. Our liberators have other duties, they will be leaving us now, but we shall never forget what they have done for us...’ Silence and a penetrating look. ‘They have rid us of a curse, the Lord has sent us help just in time. If ever you are at your wits’ end and no deliverance is at hand, do not forget, God does not abandon you. How true the words of the hymn are.’

I brace myself at my desk, spent, squeezed empty.

‘Let us therefore pray...’

A little later I hear the voices fade away across the school yard. I am sitting alone in the classroom, forcing myself to put down one word after another, making short sentences that are stupid and meaningless: it doesn’t matter to me as long as there’s something.

The master comes in and draws a curtain to keep the sun off his desk. He leafs through the exercise books and yawns.

‘Stop now,’ I hear him say, ‘just bring me what you have done.’
 
Punctiliously, his eyes follow the lines I have written. ‘Later you will realise what momentous events occurred in these days, and then you will be ashamed that you could find so little to say about them.’

I look at the dry hand shutting my exercise book. ‘Pity,’ he says, ‘truly, I feel sorry for you.’ He walks before me through the door and holds it open for me affably. ‘Is everything all right at home? Do give Akke my kind regards.’ Taken aback by his genial manner, I walk along the corridor by his side. ‘No doubt you’ll be going to join the rest at the bridge, the whole class went there I think. But all of them will have gone by now, of course.’ He locks the outside door carefully. I cross the little yard. It smells of summer in the village, you can almost hear the trees bursting into bud and growing.

‘But all of them will have gone by now, of course...’ What did he mean by that, why does that sentence keep sticking in my mind? Reluctantly I walk towards the bridge, Meintis sure to have gone straight back home, why don’t I do the same then? Hadn’t I made up my mind never to go there again?

The people in the village are sure to have seen through me long ago: ‘There he goes, off again to the soldiers, what do you think he can be up to there?’

I feel more and more anxious, and slow down. Shall I turn back? A farm cart overtakes me, I run after it and hang on to the tail-board, hoping it will get me there more quickly.

The cart stops by the canal. I jump backwards from it, out of breath, take a few steps to the edge of the water and stand stock-still. The light is overpowering. Across the canal lies a cleared and trampled-down field. Wheelmarks run through the grass and it is easy to tel from the flattened areas where the tents used to be.

What have they done, what has happened? I run across the bridge and up the road: their cars must be parked somewhere. The fields are bare and empty. I stand still, then run back, hollow footsteps over the bridge and strange sounds in my head. People in the street turn around to watch me race past. My thoughts spin furiously upon a single point until I turn giddy and fall. Blood runs from my grazed knee. I race up the road to Bakhuizen, stop suddenly, and race back, a well-trained dog.

The bridge and the spot where the tent had stood: in a frenzy I chase around in circles, an animal looking for a prey that has vanished from sight. There has to be a sign, a note, a letter with some explanation, an address... I have been left no name, no country, no destination, neither his smell nor his taste... I feel panic, smell fear: where are you, where am I to go?

The blue sky, birds tumbling and calling. Untroubled.

God, I think, You were going to raise me up, You were going to help me. Oh, God, together we were going to perform miracles...

Where his tent stood there is now a square in the grass, a flattened shape of bent stalks and trampled-down flowers, a clearly outlined, life-sized sign. I run over to it and kick my foot into the grass, claw at it, digging, finding nothing but a rusty, bent fork.

Go home, I think, he’ll be waiting along the road, of course.

Winding broadly, the road runs through the summery landscape to Laaxum.

## 8

I MOVE THE bar of soap between my hands and then lather my forehead and neck. I look at my face in the slanting little mirror, a normal face with small, tired eyes, sleepy and drained white. I can no longer see traces in it of the anguish, the terror and the despairing rage that seemed to turn my eyes to stone, emptied of tears, hard and dry as if stuck together with clay so that they would not open in the morning.

I can hear voices and footsteps in the cookhouse, the creaking of the pump, and I feel an arm nudging me: ‘Get a move on, it’s our turn.’ I am here and I am not here.

The hard-ridged pattern of the coir matting bores into my feet and the ice-cold water etches tingling spots on my face. I lean closer to the mirror: are there really no tell-tale signs left of my laborious breathing, of the hollows around my eyes, of my panicky distress? I curl my toes and move my feet in little circles over the matting.

When I put my shirt on I feel the flat little place on my chest where his photograph is. I don’t take it out because I know for certain that he will never come back if I look at it now. I must be strong and wait.

At table they all talk and laugh, everything is as usual. I force myself to eat the bread and butter which piles up in my mouth, turning into a solid gag. Take a sip of tea, swallow, work it down, another sip; all is well, no one seems to notice anything. *Is okay, Jerome, is good...*

Of course he hasn’t left, he is in the village, waiting in the car: suddenly everything inside me lights up and I feel a sense of freedom and relief. He is sitting behind the wheel, waiting for me to come. I must hurry to school before he goes... Come on you lot, get that food down, don’t dawdle, can’t you see I’m waiting, that I’ve been ready for hours? Come along, hurry up, please, before I miss him, do hurry up, I’m in a terrible hurry...

Meint and Jantsje are still a bit sleepy and take their time walking to school through the countryside, breathing in the cool morning air and stillness. They chat and laugh and I feel forced to join in. We stop for a long time beside the twisted body of a dead gull at the side of the road, its rigid claws sticking up into the air. Walt, I think, don’t go away, I’m just coming, I’ll be with you in a moment.

Why don’t I go on ahead, why don’t I run, why do I hang about with them meekly instead? Meint shoves the gull over the edge of the ditch with his clog. ‘It’ll take two weeks to turn into a skeleton,’ he says, ‘we’ll keep on looking every day and see how it happens.’

We walk on, a bit more quickly now, but in my thoughts I am racing ahead, careering up the road, flying to the crossroads, to the church, to the bridge. He is sure to be there, somewhere not far away, my patiently waiting liberator, and everyone will see me step into his car. I shan’t be ashamed, not even when his puts his arm around me. We shall drive off and leave the gaping villagers behind and I shall hold tight to his jacket and never let go again.

At lunch, Mem puts a dish on the table with a gigantic, furiously steaming eel laid out on it. It is pale and shiny and the thick skin has burst open revealing its greasy, white flesh. The smell of fish hangs heavily in the small room, seizing hold of me and clinging to my nostrils, mouth, skin and clothes. I shiver.

Hait slips a long knife along the blue skin, splitting the hideous bicycle tyre into two steaming wet halves. I reluctantly hold out my plate. *Hold it, yes, go on...*

‘They eat corpses,’ a fisherman in the harbour had once told us with a laugh as he emptied a bucket of squirming eels into a crate. ‘They crawl into anything lying dead at the bottom of the sea and suck it dry.’

It lies steaming on my plate, the potatoes swimming in a white, watery fluid with yellow islands of fat floating on top. Mem is proud of her big fish and looks on anxiously to make sure that Hait has given everyone a good helping; I am going to have to eat it all up or she will be angry.

Walt is suspended upside down in the water, his round, muscular arms relaxed as they float above his head, moving gently in the sea current. He has a wild, distant look in his eyes and a mouth like a fish, wide open, as if he wants to scream. But all sounds have been silenced.

I see a long, coiling fish circling lazily in his head, through his open mouth and in his eyes, feeding and searching with a lisping, slippery tongue and sliding through the torn, white vest. Where his hair used to be, green, slimy seaweed waves about, and his chest moves, rising and falling, in and out...

I stare into my plate at the indefinable morsels and narrow my eyes to slits. Don’t start crying now, go on eating, if I don’t do any chewing, and swallow very quickly, then I won’t taste a thing.

‘It’ll take two weeks to turn into a skeleton, we’ll keep on looking and see how it happens...’

Back to school again: Walt will be there, he’s waiting, of course he’s there, he waves and smiles without a care in the world. Nothing wrong!

The afternoon heat scalds my throat and eyes making me feel sick. I must go to bed; my blood is beating in my throat and I can’t move. But I have to get to school, to the village, where he will be sitting relaxed and patient in the car, where he will lift me up, touch me, fondle me. WE THANK YOU, V = VICTORY... I have to get there. Come along now, you lot, don’t dawdle, keep walking, honestly, that gull hasn’t changed yet, we can look at it tomorrow. Keep walking, or I might miss my lost soldier...

The village is empty and hot, the road stretching lazily between small gardens with shrubs in bud and young plants flowering profusely. A goat bleats like a plaintive child and a cat crosses the road slowly, sits down and licks its fur, one paw extended high in the air.

The church, the crossroads. But there is no car outside the school.

‘We thank Thee, Lord, for granting us good health this day in one another’s company. Forgive us our sins, of which we have a multitude, and help us to confess our misdeeds.’ The master walks to the door and holds it open for us.

And suddenly I am sure they must know all about it at home, that they are angry with me and that my last foothold is about to splinter under me.

‘Go away, get out of here, we detest you, you and your townish ways.’ They have always known about it and have simply been biding their time. Now they will pack my suitcase and put me down by the dyke. And they are right, I am disgusting, I am a sinner, I am sure to go to Hell. I shall be punished, tormented...

As I sit by the window and look at the birds still flying about in the cool, noiseless evening air, Mem brings me a mug of milk. She pats my cheek and says, ‘Don’t fret, my boy, everything will turn out just fine. You’ll be getting a letter from them any day now, I think the post in Amsterdam is working again.’

I wake up because my body is shivering, my limbs shaking uncontrollably. I press myself flat into the mattress and clench my teeth. Beside me Meint is sleeping the calm, docile sleep of the young. I stare into the dark, but it stays black and void, his face, his voice, his smell, not reaching me, no matter how desperately I seek them.

Next morning I fold my shirt carefully with the breast pocket turned inside and quickly store it away in my suitcase. I do not so much as glance at the small photograph.

We go back to school and once more I run ahead of myself, up the road and through the village to the crossroads. But each day my haste seems to lessen and I slow down: I seem to be marking time and quite often freeze into immobility right in the middle of running.

I realise that it is all in vain, my hurrying, my hoping, my waiting. He has gone.

THE SHEEP BEHIND the fence look at me with chilly, inscrutable eyes as they scratch their fleeces against the fence; one of them appears to be giving me a sardonic smile, chewing continually with its mouth askew. I have to turn my eyes away: does everyone know my secret?

I pretend to be reading, but my eyes see neither the words nor the lines, just a dazzling bright spot that shimmers and glitters in front of me. Everything appears faded and washed out: the sleeves of my pullover, my socks, the reeds beside the ditch.

From the other side of the house comes the dull, thumping rhythm of a ball bouncing against the wall; every time I hear the sound it is as if somebody were banging away persistently in the middle of my head. I look at the hundreds of letters making up the sentences and flick impatiently through the pages of the book. I must read it properly and not keep thinking of other things.

He has been gone for nearly two weeks now and with every passing day he is hundreds of miles further away, his distance from me growing unimaginably great. Is he thinking of me, is he planning to come back? At first I was convinced he was, but now I am no longer certain. I pluck a dandelion from its stalk and the milky sap that wells up out of the little ring leaves black marks on my fingertips. Then I pull off the yellow petals and reduce them to a few sticky, powdery wisps between my fingers.

The thumping behind the house has stopped to make way for an ominous silence: why aren’t they playing any more, are they about to come over here? The small bouncy ball, a construction of orange rubber bands wound tightly round each other, flies past me, rolls through the grass and comes to a halt close to the ditch. I quickly bend over my book.

‘Are you still sitting there?’ While Jantsje runs after the little ball Meint leans over me and says, ‘Still on page twenty-one, I see. Haven’t you got anything better to do with your time? Come on!’

I turn over a page. I can hear them snigger and whisper conspiratorially behind my back. Why do I always feel so tired these days, with that empty, sick feeling in my body that doesn’t want to go away?

‘Come over to Hettema’s with us, just wait till you see what’s going on there, it’s fantastic... ’

The sheep are walking alongside the ditch now, their feet sinking in the mud, so many round balls of wool stuck onto four brittle little sticks. Soon they will be shorn and thin as dogs, then it will be my turn to smile sardonically and chase those nosy riffraff all over the countryside. Stupid creatures...

I allow myself to be pulled up from the grass, but then Mem’s voice rings out from the house. ‘Jantsje, you aren’t going anywhere. Pieke’s always being left here all by herself, none of you ever give her a thought. Either she goes too or else you are all going to stay right here.’

‘Make her hurry, that’s all, we’re not going to wait for her, everything takes ages when she’s around.’

As if to prove how quick she is the little girl hops licketysplit through the grass, tossing and flailing her arms about, her callipers clattering over the tiles that serve as a path through the meadow. She stops out of breath by the fence and stands there waiting until we heave her impatiently and roughly over it. On the other side, in the lee of the dyke, lies Hettema’s farm, surrounded by tiny fishermen’s cottages like a big mother hen ruling over her roost.

‘Come on, you lot, hurry up or it’ll all be over!’ We run through the sheets of mud in Hettema’s yard towards the stables. A large animal stands four-square on a narrow path behind the barn, two men keeping it under control. The beast grunts protestingly and shakes its enormous head, making a jingling sound.

‘Albada’s bull from Warns,’ Meint whispers, ‘he could go straight through a wall if he wanted to. Just look at those legs!’

The soft nose, wet and dripping slime, has a thick iron ring stuck through it on a solid chain that one of the men is pulling while the other beats the animal’s flanks with a piece of wood. ‘Get moving, blast it, you lazy devil...’

Hettema is standing by the cowshed. ‘Not behind the beast, Meint, get the children away from there, he can turn nasty.’

Goaded, the bull turns its head from side to side in soundless protest, then lets out a bellow as if maddened by pain. I can see a streak of blood in the slime from the nose and cringe suddenly with every blow I hear. I’m not going to stay here, I tell myself, I’m going back home.

Then the bull is thwacked on its hindleg and moves forward reluctantly, meandering along the narrow path. Meint grips my arm. ‘It’s all right, it’ll all be over in a minute. And they don’t feel that hitting at all.’

They‘re going to slaughter the bull, I think, fascinated by that large chunk of life that for mysterious reasons will suddenly cease to be, like a storm that dies down in the twinkling of an eye. In Warns I once saw them slit a cow’s throat: a sure, razor-sharp movement of a piece of steel across the soft skin, the eyes turning surprised and glassy, before a moment of dazed silence, and then, suddenly, a gush of blood and the huge body caving in as blood and shit spattered up the walls. I was frightened that time, going rigid with revulsion and yet I didn’t avert my eyes for a single moment.

As the bull stamps rebelliously past the barn Pieke and I watch from a safe distance, our hands excitedly clasped together, while Jantsje leaps to and fro, nervous as a hare.

‘The cow’s over there,’ says Meint solemnly. ‘Now you’ve got to pay attention, that’s what the bull has come for.’

The bull is brought round behind the meekly waiting cow, stays standing for a moment stock-still, then heaves his unwieldy, leaden body up onto the thin, tottering hindlegs, snorting violently as if under enormous exertion, and places his front legs comically on the cow’s back.

‘He’s dancing,’ laughs Pieke, ‘can you see, he’s going to do a dance!’

Meint pushes us anxiously along the wall to the front. ‘Otherwise you won’t see a thing.’

The animals take a few clumsy, stumbling steps as in some primitive fox-trot, until one of the cow’s legs slides off the path into the mud and the bull loses his balance and falls back onto his own legs. For a moment there is nothing but angry bellowing — we have all run away giggling nervously — then the cow starts to trot about impatiently, her udders swaying under her stomach. Where the path meets the fence she tries to turn round, but the men have already grabbed her by the head.

‘Go and fetch us some hay, boys!’ Meint runs back to the barn and throws an armful of fragrant grass onto the ground in front of the cow. She shakes her head stupidly at first, then the tufts of hay begin to disappear steadily into her grinding mouth, nothing else seemingly able to distract her attention. The bull is brought up again, but this time Hettema has to use all his strength to slow him down. He rises up like a furiously rearing horse, the prize-fighter’s muscles so powerful and contorted that we beat another hasty retreat.

A long, shiny spear, scarlet and naked, protrudes from the belly of the rearing animal, a defencelessly displayed sex organ probing and trying to find its way. Dribbling, the bull clumsily seeks support with his forelegs on the cow’s back, while she continues to chew absent-mindedly as if failing to notice that the point of the spear is being pressed quickly and battle-ready into her body. Mesmerised, I watch as the bull, with a vacant, confused expression, takes impatient little steps along the cow’s back, moving jerkily like a lamb tugging at an ewe’s udder.

On the other side of the animals, which loom up between us like a mountain range, stand Jantsje and Meint. I meet their eyes, feel deeply ashamed and turn red. Are they laughing at me? I suddenly break into a sweat and my skin starts to prickle. Why are they looking at me like that?

The bull is standing on all four legs again, with what is now no more than a long thread dangling from his belly. ‘Well, that didn’t give him much trouble,’ says Hettema, flicking a cigarette butt over his shoulder, ‘but then he’s an old hand at it.’ He gives a short laugh.

The bull snorts and blows along the cow’s flanks, licking the soiled skin with a caressing movement, showing such devotion and gentleness that I feel myself going weak at the knees and am afraid of falling. Did the bull really go deep inside those elongated, dark folds, into that sticky place he is now licking so quietly and patiently? Was that fucking I had just been watching, the ‘doing nookie’ that the boys in Amsterdam were always whispering and smirking about so mysteriously at school?

> The emp’ror of China,
> He often fucked Dinah,
> It sounded like thunder...

Was that what I did with Walt, fucking? Surely you can only do that with girls, fucking has nothing to do with boys, has it?

‘Go in the barn, I’ll get you all something to drink.’ Hettema picked up a few mugs and poured milk into them from a tin. I look at the manure lying all over the floor in lumps and puddles and think of the sticky thread dangling under the bull. ‘Fresh from the cow,’ says Hettema, ‘full-cream milk, the best you can have. It’s still warm, have a taste.’

The milk rolls about heavily in my mug, little black bits floating on its surface.

> ...It sounded like thunder, As Dinah went under.
> And didn’t they snigger,
> As her belly grew bigger...
 
As soon as I have the chance and no one is looking, I pour the mug out into the straw.

<br>

Jantsje and Meint stay behind to play in Hettema’s yard but I go back to the road and start walking towards the harbour. Pieke, weaving along on her lame leg, trudges behind. Why doesn’t she leave me alone, why does she keep following me, can’t she tell from my mood that I don’t want her company?

I wait for her to catch up with me. Panting because the dyke is too steep for her, she smiles, baring a set of milk teeth full of gaps in a grateful grimace.

‘Did the bull frighten you?’ she asks.

‘Don’t be so stupid.’

What do they all want from me, why do they look like that and ask questions all the time? When we reach the pier she takes hold of my hand, startling me with her touch.

‘Shall we wait until Hait gets back from the sea?’ She careers towards the little beach and hops about collecting pebbles. Sitting at the water’s edge, my arms tight around my knees, I think of Walt, of the leg he threw over me and the impatient thrusts after that. Now I long for those hurried actions that frightened me at the time, I pine for the touch of his belly, of that private, secret place that had need of me. My longing is so strong that it makes me feel weak and ill.

I watch Pieke squat right next to the place where the kitten lies buried. Does it matter if she finds the grave? The paper flower is still there, faded and crumpled now, no longer looking like a rose. Everything else has vanished.

‘I think I can see a boat.’ I am lying, but Pieke hops towards me and peers along my outstretched arm. I stare across the water and fling a pebble into the waves. Plop, a mean little sound, a splash, the beginnings of a ripple and then it is all washed away.  I try to picture my mother but I can’t, I am no longer able even to imagine that she still exists. Probably everything has gone, all of them, our house, all vanished in the war, swallowed up in an abyss of horror.

Plop...

I try to pretend that I don’t really care about the letter that doesn’t come, that I almost despise the whole idea. What difference can it make now? And Walt, where is he, is he still having to fight the war? The sea wall he lay behind is across the harbour. That’s where he was waiting for me and where I walked towards him over the rocks. How far away it all seems.

Plop, another pebble...

I don’t think about Jan any more. It doesn’t bother me at all that I hardly ever see him these days, it all seems quite unimportant, a dim memory. The minister’s wife is with God, she can see everything that happens, and perhaps my mother is with her too and they are looking down together to find out what I am doing. I place three pebbles next to the little rose, one for Walt, one for my mother and one for the minister’s wife, in that order.

They are sitting together on a large, grey bench, fused into an ageless whole. Their eyes are not cast down but look out across the mass of grey-white cloud that stretches before them. And yet they see me, they follow me and speak about me, tonelessly, without words.

Are they pleased with the pebbles, have I made them feel more kindly, can I mollify them, win them over?

Bye, angels, I think, oh, angels, bye!

The boat comes into harbour like an all-conquering, dark-brown bird. Pieke shouts and we hop, skip and jump to the landing stage as fast as we can.

While we are walking back home Hait puts his hand on my shoulder familiarly, as if I were a man, a mate of his. ‘Don’t look so unhappy, young ‘un, that won’t get us anywhere. You’ll see, there’ll be news very soon, things are happening very fast now.’

I bite my lips. Who will hold me tight, who will take me in his arms, let me feel another person’s warmth? I am dark and dirty inside and out, and that’s how it will always be if his mouth is not there, the tongue that licks me clean, that touches me considerately and selflessly, beyond my shame.

‘Pieke, girl, come on, make Jeroen laugh for a change!’

At table I toy with my food and use my fork to make two piles on my plate, intending to leave the bigger one uneaten.

‘That’s all the thanks we get,’ says Mem. ‘Don’t ask me what’s been wrong with him these last few days. But you don’t get down from this table until that plate is empty.’

‘Lord, we thank Thee for this food and drink. Amen.’ Everyone gets up, while I am still wrestling with myself. The bull’s gigantic body rears up threateningly and yet helplessly, the spear stuck out in triumph like a blood-red flagpole...

I have laid my head in my arms and sniff at the oilcloth on the table. Overhead I can hear a fly caught in the flypaper, a piercing, penetrating buzz. All of a sudden Diet pulls my chair back so that I nearly fall to the floor; the world does a half turn in front of my eyes and my heart misses a beat. I let out a shriek and run sobbing out of the house, across the meadow in my stockinged feet. As I race through the grass I can still hear my scream, a ludicrous yet terrifying cry, echoing in the emptiness that I feel all around.

When I go back home a little later the sheep are standing huddled together behind the fence, looking at me with cold, searching eyes. One of them is smiling at me, pointedly and sardonically.

## 10

RARELY DOES THE postman come out to Laaxum; generally he waits until somebody can take the letters, if there are any, back with them to our small hamlet.

Halfway home on our way back from school we run into him, bending far over the handlebars of his bicycle as he pedals against the stiff wind, and a ray of hope shoots through me, even though I have gradually managed to eliminate all feelings of expectation.

While Jantsje and Meint quench their thirst at the pump, I hurry into the little passage as inconspicuously as I can. The house is unusually quiet, the living-room door is closed and Mem is nowhere to be seen, which is odd. When I push the door open with a gesture of apparent unconcern, she is sitting idly at the table, her hands on her knees and she gives me a strangely gentle look, dreamy and far away. Her jaw makes soft movements to and fro. Is she trying to suppress a smile? Before I can open my mouth she stands up and, as if caught out, starts to move the little framed portraits on the mantelpiece, rearranging them.

I let my eyes run over the room and almost instinctively spot where the change is: on the little chest there now stands a square white envelope, modestly tucked away behind a little vase. There is a green postage stamp in the top right-hand corner, but the place where the handwriting of the address should be is hidden from my view by the little vase.

I look away quickly, turn around and put my exercise book down. As I open it and pretend to be giving all my attention to what I am reading, I watch Mem out of the corner of my eye: she is standing leaning across the table, her head turned towards me. I wait, but she says nothing.

‘We’ve been given some really hard sums to do.’ I talk to break the silent tension between us. My mouth is dry and I bite my lips. I have an almost irresistible urge to push her out of the way and to read the writing on the envelope.

When Jantsje and Meint come in Mem sends them out brusquely. ‘Go and play with Pieke by the house,’ she says, ‘or give Diet a hand with the cooking. I’ll call you in good time.’

But I take care to slip out quickly with the others to avoid being left alone with her again. I don’t want to know — let her keep the secret to herself.

I wonder if she has read the letter, if the envelope has been opened. I try feverishly to recall the image of the small white square: what did it look like, had it been opened along the top?

What if it is a letter from home with bad news, a note from the neighbours to say that there is nobody left at home, that they all dead... But it could just as easily be a letter from Walt, even though he doesn’t know my surname or our address, because all he would have to do is put ‘Jerome, Laaxum, Friesland’ on it and it would still get here, everyone would know that it was for me.

I go rigid. What if she has read it, what if she understands English? Perhaps that is why she’s been giving me such strange looks.

‘Is there enough time to go down to the harbour? I’ll make sure I’m back for dinner.’

Diet gives me a surprised look, she has pulled up her sleeves and wipes her wet hands over her hair. ‘You can see the food is ready, can’t you?’ She puts her head outside the cookhouse and points. ‘The menfolk are on their way now.’

I had wanted to escape, to put off the evil moment. They’re going to have to open the letter without me, I don’t want to be there when it happens. But how can I get out of dinner? Popke and Hait are already by the door, stepping out of their clogs and disappearing into the little passage, leaving behind them a salty tang of fish, tar and wind.

As we make for the table, Hait and Mem stop by the door for a moment and talk together in undertones. I squint at their faces out of the corner of my eye while my heart beats fast; do they look serious, is it bad news? I dig my fingers into my thighs and move my palms across my trousers. God, be nice to me and help me.

After grace there is complete silence, as if everyone knows that something is about to happen. Hait stands up and walks over to the little chest, picks up the small white patch and carries it towards me. Why? I want to disappear under the table, I am horribly frightened and ashamed. How can I avert this evil moment?

‘A letter has come for Jeroen, from his father and mother in Amsterdam. I think it’s going to be a nice letter.’ When I don’t take it from his hands he puts the envelope down on the table in front of me.

So it’s not from him. Why do I think that, why is that the first thing to come into my head? He hasn’t sent me a message...

All of them are looking at me now, the whole table full of beaming faces, and still there is silence. Should I say ‘thank you’ now, open the letter and leave the room, or should I wait until after the meal?

‘Don’t you want to read it?’ Mem asks. ‘Would you rather Hait read it out to you?’

The white patch flickers before my eyes on the tablecloth, there is an enormous distance between us. Why aren’t I pleased, I think, how is that possible? But what I want is news from him, that’s what I’ve been waiting for.

Suddenly there is a large empty space under my eyes in which shiny flowers begin to take shape, little bunches of daisies, some roses, symmetrically placed garlands and blue forget-me-nots, all sprinkled with the grease spots the oilcloth has gathered over the years.

Hait’s voice reaches me from a distance, hoarse and solemn. I can tell by his tone that he is looking at me and addressing the words to me. I edge backwards until I can feel the back of the chair pressing into me. I hear snatches of sentences, a single word here and there, or a name, but the beating of my blood distracts me and drowns out even Hait’s steadily reading voice.

After dinner Mem keeps me in, leads me to the chair by the window and puts the envelope in my hand. ‘You’d best read it over quietly, by yourself,’ she says and sits down facing me. ‘Oh, my boy, what wonderful news this must be for you!’

The small sheets of paper come rustling out of the envelope. I unfold them and recognise my father’s handwriting.

Amsterdam, it says on top, 9th May. My dear son...

My eyes stop moving. My dear son, is that me?

Is that what Daddy calls me, does he mean me? Is that the eternity that lies between us, the longing, the poignant homesickness? *Kiss me Jerome. Is good, I love you.* My head rests against his neck and his hands clasp my shoulders tight as if he is afraid I might escape. Say it, baby: I love you. Yes, that’s good, very good.

<br>

My dear son,

At last a letter from us. We hope you are well and that you haven’t forgotten us completely! We’ve come though the war all right and now we are free.

It’s taken a long time for us to be able to send you this letter, but slowly and steadily everything is getting back more or less to normal. Bobbie is well, he has grown into a big boy, so you will hardly be able to recognise him. When you left he was such a skinny little baby, but now...!

It is still difficult in Amsterdam to get food, or to get clothes, or shoes. Still, everything is getting a little better, almost every day.

Everybody is very relieved and celebrations are being held everywhere, in the street and at your school. The school is being used by Canadian soldiers now, so you won’t be able to go back there for the time being. You won’t believe your eyes when you do get back! There have been some sadnesses, too, I am sorry to have to tell you. Aunt Stien’s and Uncle Ad’s Henk died suddenly from the cold during the winter and Mijnheer Goudriaan from across the road as well. It’s been really awful for Anneke, not having a father any more. Write her a card one of these days if you can, she’ll be terribly pleased if you do. Mummy and I want to have you back home as quickly as possible of course, but I think it’s best for you to be patient a little while longer until everything is a bit more settled. We don’t even know if you’re all going to be fetched back again, but if not, Mummy or I will come for you, and I think that Jan’s mother will probably come along too in that case. How is he? Please remember us to him.

We have written a separate letter to the lady who is putting you up. You must be very grateful to her for having looked after you for such a long time.

Dear Jeroen, I’ll do my best now to get this letter to you as soon as possible. Just a little while longer, and then all of us will be together again. Be a good boy and give everyone in the family there our kindest regards. They’ll all have to come to Amsterdam soon!

With love from Daddy.

<br>

And then, in a schoolgirl’s hand:

Hello Jeroen, Daddy has written almost everything already. We’re having a good tuck-in now with all the things you can get in the shops again, real milk sometimes, and white bread, and powdered egg, it tastes wonderful. Your little brother is turning into a real fattie, he is almost too heavy to lift up. When you get back home you’ll be able to take him walking by the canal, we are trying to get a push chair for him.

Is everything all right in Friesland? Mummy.

<br>

Is that all? I turn the sheets of paper over. Nothing. Mem has got up and looks at me expectantly. Her eyes are soft and she comes and stands close to me.

The lady who is putting me up.

‘Isn’t that lovely?’ she asks. ‘Now everything is sure to be all right.’

‘Were they liberated in Amsterdam by different soldiers to ours?’ is the first thing I want to know. ‘Ours were Americans, weren’t they?’ I look at her, but she shrugs her shoulders.

‘I don’t exactly know, my boy, you’d best ask Hait about that.’

‘The letter was sent on the ninth of May,’ I say, ‘what day is it today?’

She goes up to the small calendar and slowly counts the days.

‘The twenty-seventh,’ she says, ‘it took along time getting here.’

I put the letter back in the place where it stood, behind the little vase on the chest.

In the little passage I take my coat off the hook and press my face into the cloth. I move my nose slowly over the sleeves, the collar, the back.

Sometimes it is as if I can vaguely identify his smell, that mixture of metal and hospital, and when I do I am indefinably happy and reassured. But now I can smell nothing, no matter how fiercely and desperately I try.

Without warning, Mem opens the door and gives me a somewhat disconcerted look. ‘Don’t you want to put the letter away in your suitcase? Then it won’t get lost.’

‘No,’ I say. I hang my coat back on the hook.

## 11

THE LIVING-ROOM floor creaks, a chair is pushed back, a plate clatters across the table. Meint is not yet asleep, he has a cold and his breath sounds laboured and congested. Outside it is still warm and light, but I can tell from the muted impenetrability of the sounds that the heat is dying down and seeping into the earth.

The fields are lifeless, no sound can be heard, and the house is immersed in an ocean of stillness. Inside, too, no one has spoken for a while, the silence broken only by an occasional sigh or a tired yawn.

Night in the country, the day’s work done.

Mem sits by the window, as she does every evening, looking out over the countryside as she knits. Almost no one is about at this time of day, just a few cows or a fisherman still straining his eyes out to sea.

‘Two people are coming over the Cliff,’ I hear her say. Hait answers the broken silence with an indistinct mumble. I turn over and pick at the scab on my arm. One edge has come away and I try carefully to continue the painful process of levering it off. Just so long as it doesn’t start to bleed again.

Silence, nothing moves, except for the scab slowly coming off.

‘It’s two women.’

I can hear Hait turn his chair around. His footsteps move across the floor and the door gives a sharp creak.

Mem yawns and shifts in her chair. There is the noise of the pump in the shed: Hait is filling his mug. I can hear the gurgling flow of the water.

‘They’ve got bicycles.’ Mem gets up, her voice growing agitated. Hait has come back inside and I hear him put the mug on the table and then drink in slow gulps so that I can follow the course of the water as it passes through his body with funny little sounds.

‘They aren’t from round here, they’re wearing townish dresses,’ the report continues, getting faster.

I push one of the cupboard-bed doors a bit further open and see Mem leaning against the window, one hand over her eyes to see better. ‘What could they possibly want here, so late?’ Hait has moved next to her. The evening sun falls over their faces and lingers on a piece of furniture. Dust whirls in the late light, astonishing quantities of minuscule particles on silent, everlasting journeys.

Why do I just lie there, why don’t I move?

‘Oh, my goodness, they’re turning this way!’

I gather that the two women have left the dyke and are coming in the direction of our house. Two women from the town. My heart begins to thud, an unreal feeling pervades my body.

‘They’re pointing at our house,’ says Mem, now clearly excited, ‘could it possibly be for Jeroen?’

Is it up straight, petrified.

‘Take it easy, man, nothing’s going to happen,’ says Meint and blows his nose noisily.

‘They’re climbing over the fence, they’re either going to Trientsje, Ypes next door, or coming here. Goodness knows which. My, oh my!’

She drops back into her chair, looking like a goddess sensing disaster. Then she gets up, looming large in the low room, and goes to the back of the house. The outside door rattles violently.

‘Could well be your mother come to fetch you,’ says Hait and opens the cupboard-bed doors. I am sitting bolt upright, completely at a loss. There are more voices from the other cupboard-beds and Meint beside me gives a protesting cough.

My mother.
 
If it is her, then the war is definitely over...

<br>

But is it her? Has she really come? Has their longing for me finally become strong enough? My body feels feeble and limp, my belly seems to have dropped to the floor. When I try to take a step, my knees are out of control and I have to cling to the wall in case I collapse.

I can’t believe it and I mustn’t believe it either: I can’t afford to any more, it’s sure to be just another empty hope drifting by like a useless tuft of sheep’s wool.

I stand in the doorway and lean my head outside just far enough to take in the stretch from the side of the house to the fence. Hait and Mem are standing halfway down the path, and when I see that Hait has quickly slipped on his Sunday jacket my knees start giving way all over again: what’s going to happen next? Mem is busy with a lock of hair that refuses to stay in place, her hand patting her head and running down her hip in turns. Seeing her standing there, solemnly, yet on her guard, watchful as if she were about to have her photograph taken, makes my breath escape with a jolt, like a gasp.

I am suddenly very aware that the inevitability of the moment has been impressed on everything around: on the gusts of wind bending the tall grass, on the evening sky as it dims to pale grey, and on the expectant silence of the landscape in which the two dark figures have now become motionless. I had tried to steel myself against my own fantasies and dreams, but all of a sudden I have become defenceless and vulnerable, an abject and easy target.

As if sensing that I am hiding in the doorway, Mem turns and looks straight at my face peering around the corner. She takes a few steps towards the house and calls out in an attempt at a whisper, ‘Surely you’re not standing there in your underclothes? Hurry up and get dressed, they’re nearly here.’

I dash back into the room, breathlessly pull my trousers on, wrestle with the buttons and leave my shirt undone.

‘Don’t bother with your socks,’ says Diet, when she sees me bending over. They are all sitting up in their cupboard-beds and are following my frantic scramble with curiosity but also with some awe: I feel that I have suddenly taken the centre of the stage, that tonight I have taken over the main role in the household’s doings, that they are all aware that the dénouement of an unfinished tale is about to be played out.

The first thought to spring to my mind when I see her climb over the fence, loose-limbed and youthful, is that she doesn’t look in any way hungry or poor and that makes me feel almost cheated and disappointed. Quickly and apprehensively the two women walk towards Hait and Mem. I can see that they are talking to each other nervously and that they are feeling ill at ease on the grass without a proper road under their feet.

I recognise my mother at once, her movements, her hair-do, her familiar yellow dress. Both of them are wearing colourful summer frocks, billowing out behind them.

Mummy, I think, as the sound of her girlish voice suddenly reaches me. For a moment it seems as if I have lost all control over my muscles and am about to wet my pants. Desperately I squeeze my legs tight together and arch my back.

‘Oh, my goodness,’ I hear Mem say as the slight figure in the yellow dress walks up to her, ‘isn’t this wonderful. So you are Jeroen’s mother.’

Far away on the sweltering horizon I can hear an indistinct threatening rumble, as if the war is still going on, and behind the deserted dyke the sky is aluminous white. The evening is stifling and the air full of dancing mosquitoes. I watch them shaking hands, laughing, their delighted surprise: we have made it, we have found you!

Suddenly, as if on command, all four turn round towards the house. I can hear their footsteps in the grass. If I go back to bed, I think, I might be able to put off having to meet them until tomorrow.

‘Come on, Jeroen, it’s your mother. Here she is now.’ Mem’s voice sounds like a trumpet in the night air. ‘What are you hanging about back there for?’ and she adds in an apologetic tone, ‘He had already gone to bed, Mevrouw.’

I don’t move, pressing myself against the wall. All at once I have become suspicious and mortally afraid of the moment for which I have been waiting for nearly a year. I look down at the floor and keep quite still.

‘He’s never like that normally, is he now, Wabe? He’s a bit shy because everything has happened so suddenly.’

Gently insistent, Mem pushes me towards the back door. I can feel the pressure of her large warm hands as they lie calmly but firmly against my shoulders. Outside stands my mother, a stranger still, somebody I know no more than vaguely and have no wish to know better than that.

‘Hello,’ I say and walk towards her. ‘Hello, Mum.’

She throws her arms around me, kisses me, and then holds me at arm’s length and says in a choking voice, ‘How tall he’s grown, and how he’s filled out! My word, son, don’t you look well, haven’t you put on weight!’

I recognise the catch in her voice, the same pinched way of speaking she used when there was an argument at home and a row suddenly flared up with Daddy.

Don’t cry, I think, please don’t cry, not now and not here.

But she doesn’t cry, her eyes shine and her hair dances in light curls around her forehead. Her joy is something I recognise and it sets off a thrill inside me, as if to drive home to me how strong the ties between us are.

She pulls me back against her once more and this time I am aware of the gentle slope of her belly and the curve of her thigh. The familiar smell of her clothes tickles my nose and brings me closer to home, to our street. I can see the bedroom again, the kitchen, the veranda, the stairs.

Mechanically I put one arm around her, but it feels awkward, artificial. When I turn my head a little I see Hait and Mem standing stiffly side by side in the doorway with lost little smiles, as if they don’t know quite what to do with themselves or about the situation. When I meet Mem’s gaze she nods at me with gentle, helpless eyes and raised chin, as if trying to stifle a sneeze.

And then recognition floods up from deep inside me, a wave that runs through my knees, my belly, my gullet and stays stuck in my throat. My hand makes a few panicky movements and finds a small hole in the soft material of my mother’s dress. I bore my finger into it, a burrowing, frantic finger. I can feel the material give and gently tear.

‘What are you doing?’ My mother’s voice has a sharp, nervous edge to it. ‘Are you trying to ruin my dress? My only good dress!’

She gives a childlike laugh and abruptly pulls my hand from her body. When she sees how crushed I look she bends down and kisses me warmly. How odd to be kissed without the rasping prickles of a beard; it takes me by surprise and I’m not sure if I like it.

Now I ought to put my arms around her neck, hold her tight and never let go again, tell her everything that happened while she listens patiently and finds the answer to all my problems. But again I don’t move and just stand there sullenly between the adults.

‘What do you say, young ‘un, isn’t that something now? They must have gone at a fair lick to get here so quickly, musn’t they?’ Hait bends down and shakes my shoulders gently as if waking me from a deep sleep. ‘Don’t just stand there. Say something to your mother. Why don’t you show her round the house?’

I nod and say nothing. The words won’t come and my throat refuses to move. I lift my head to see if my mother is looking at me and discover that Jan’s mother is standing next to her with an arm around her. What is she doing with her hand over her eyes? Is she crying?

Then we all move into the house, somehow.

‘Why don’t you say hello to Mevrouw Hogervorst?’ My mother’s voice sounds worried, she whispers the words almost inaudibly into my ear. My feet are wet and cold from the grass and I put one on top of the other to warm them. Jantsje and Meint are standing huddled together in the semi-darkness of the room. I can hear their suppressed laughter and see that Meint is trying to draw my attention by waving his hands about urgently.

‘Scharl is a bit further back, you passed it on the way here.’ Hait is standing by the window and points into the dusk for the benefit of Jan’s mother. ‘In the daytime you can see it clearly, it isn’t far.’

Of course, they’ve come for Jan as well. Will they go back again after that, will everything be as before, this family, this little house, my loneliness? For a split second I fervently hope so.

‘I’ll walk with the lady down to the harbour to find Popke, then he can show her the way to Scharl. And I’ll bring your bicycle over the fence at the same time.’

<br>

My mother sits at the table, her eyes radiating light. As she looks around the room, her gaze always ends up on me so that I begin to feel ill at ease. It is strange to see her here, small and slender, a little bird unflustered by her sombre surroundings. She doesn’t belong here, I think, she makes the room look strange. The yellow dress, her townish ways of talking, the little shoes that she had kept on inside the house.

‘How small it is in here for so many people,’ she says, ‘it must be terribly hard for you with so large a family. Jeroen did tell us in one of his letters, but I never imagined it would be this small...’

Stop, I think, for goodness’ sake stop.

She strokes my cheek and I draw back shyly. ‘It was awfully good of you to be willing to take in one more.’

‘My goodness me,’ Mem folds her sturdy round arms, ‘it’s no more than our duty. God arranged it that way.’ She pours tea into the best cups with careful, almost respectful movements. ‘It’s nothing to feel grand about and for us one more doesn’t make that much difference.’

I hear a mixture of modesty and pride in her voice and her eyes are filled with the gentleness that always disconcerts me.

‘Jeroen has been a good boy. He’s never given us the least trouble. He was like one of our own.’

My mother pulls me towards her and takes me onto her lap. ‘Darling,’ she laughs brightly, ‘you come and sit with me. You’re a little bit more used to me again now, aren’t you?’

I strain away from her slightly.

The children are sitting around the table, unusually still and obedient, staring at the lady from the town without uttering a sound. I feel the gulf, plainly and painfully, and am ashamed.

‘Has he always been as quiet as that here?’ my mother asks softly and anxiously, as if I shouldn’t be listening. ‘Was he homesick a lot? You know, he always was a bit different, always a dreamer. He could sit in a corner playing for hours, not a bit like a child. Hey, Jeroen?’

Suddenly I fling my arms violently round her and cling to her tightly.

Walt, why aren’t you here? Why did you go away like that?

From her movements I can tell that she has begun to sob uncontrollably, although sometimes it almost sounds as if she were laughing wildly. I look at her in astonishment.

‘Yes, we’ll be going back home pretty soon now,’ she says. ‘Back to your Daddy and your little brother. Don’t cry.’

I am not crying.

Won’t Mem find it odd to see me sitting on somebody else’s lap, since I still belong a little bit to her too?

‘Are you glad that we’ll be going back home soon?’ asks my mother.

‘But not straight away,’ I say quickly and almost imploringly. It suddenly hits me that if we leave here, Walt will never be able to find me again; I will have vanished without trace.

<br>

In the cupboard-bed Meint whispers to me. ‘How pretty your mother looks, she could be your sister.’

I hear subdued voices from behind the cupboard-bed doors, and time and again they speak my name.

‘Perhaps I’ll be staying on here all the same,’ I say hopefully. ‘I’m sure she’s not taking me back with her.’ I no longer know what I really want. Leaving seems unthinkable and yet I don’t want to stay here either.

‘Ah, well,’ says Meint with a laugh and gives me a dig under the blankets. ‘In a month’s time you’ll have forgotten all about us.’

I hear footsteps and then the little doors open a chink. ‘Keep quiet and go to sleep,’ says Hait, ‘dream about being back home.’

When I am about to say my prayers I realise with surprise that I won’t need to pray for my parents any longer. The only one left will be Walt.

God, please keep him safe. Don’t let him be wanted for the war and please let him come back. I’ll always go to church, even in Amsterdam. If he just comes back.

He unbuttons my clothes and puts his hand inside my shirt, sliding it down between my shoulder blades. I shiver and stiffen.

<br>

Not until next morning does it really sink in that my mother is here, and then the day seems unimaginably bright and carefree. I scramble up the attic stairs to wake her up, but she is already sitting on the edge of the bed pulling her socks on.

‘How peaceful it is here,’ she whispers, ‘what a wonderful time you must have had, All I can hear are the birds.’ She looks at me long and hard. ‘Thank your lucky stars you were able to come here, you look so well. Back in Amsterdam we had a horrible time. Aunt Stien’s little Henk is dead, and Mijnheer Goudriaan as well. Thank goodness, darling, all that horrible business is over now. Won’t Daddy be surprised to find you looking so well!’

We walk around the house and I show her everything, the sheep, the little plant I sowed myself under the window, my collection of pebbles. As for the grave, I think, I’d better not show her that, because a little bit of her is buried there. My prayers have changed now and the grave feels different as well: it is as if all the cornerstones of the life I have built up here have caved in.

‘Look, that’s where Jan lives.’ I point across the sunny landscape to the group of trees in the distance. I recall with surprise how long ago it was that I used to stare across to those trees, sometimes day after day, longing for Jan and weaving fantasies around him.

Her bicycle is leaning against the wall at the side of the house, old and rusty but with real tyres. ‘Daddy got those from a colleague at work,’ she says, ‘especially for our trip. But they are old ones, let’s just hope we don’t get any punctures on the way back.’

I walk round it and touch it. So I shall be leaving Friesland on that. I look inside the pannier bags hanging from the carrier, and see that they are empty. ‘That’s where we can put your bits and pieces. It’s easier than a suitcase. We’ve got quite a long way to cycle.’

After breakfast we go and see Jan. During the meal Pieke had stood right next to my mother the whole time, fondling her and clamouring for attention. She had admired everything, the dress, the shoes, the watch, and by the time we went outside the two of them were as thick as thieves.

‘If we go on the bicycle, she can come along too, on the back,’ says my mother, but I insist that we walk and that we go alone. Suddenly I feel that I don’t want to share my mother with anyone else. We walk along the dyke, and near the spot where the car had parked she says, ‘Is the sea behind this? Let’s have a look!’

She runs up the dyke and spreads her arms out wide. ‘Wonderful,’ she shouts, ‘oh, isn’t this wonderful!’ The wind blows her yellow skirt up as she comes flying through the grass. ‘Come on,’ she says, ‘I’ll race you.’

I do not want to go up the Red Cliff. ‘It’ll take too long,’ I tell her, ‘we’ve got a lot to do still. First we’ve got to go to Jan.’

Jan’s mother is sitting in the front room with the farmer’s wife looking at photographs spread out on the table.

‘Jan?’ The farmer’s wife opens the door to his little room and calls up.

‘He’s probably in the barn,’ says his mother, ‘he’s full of it, he talks of nothing else.’

She shows a photograph to my mother.

‘Just take a look at that,’ she says. ‘You can’t tell from looking at it, but he’s dreaming of cows and sheep. He is set on being a farmer.’ She roars with laughter at the thought. ‘He’s a real boy, that’s for sure!’ she says bluntly, looking straight at me. What does she mean, a real boy?

The farmyard. I seem to be seeing it all in slow motion now; the light over the tiled roof, the stinking yellow-brown manure heaps, the weeds shooting up in corners, the muffled sounds from the stables, it all comes home to me suddenly and I go cold for a moment: am I here for the last time?

‘How nice it all is,’ says my mother. ‘Wouldn’t you have preferred to live on a farm, too, with all these animals?’

Stung, I answer crossly, ‘Laaxum is much nicer, and you don’t have to work there all the time. And I’d much sooner go to sea, anyway,’ I add sourly.

We walk through the lofty stables towards the sound of intermittent banging and find Jan on his knees in a dusty corner, doggedly hammering away at some boards.

‘Hello,’ he pants. ‘A pen for the calves.’

He stands up and with an air of satisfaction plants his boot on his work. With a manly gesture he blows his nose, calmly wipes the back of his hand down his overalls and holds it out to my mother. ‘It’s a bit different from Amsterdam here, hey?’

I am amazed to hear him talk to my mother like an adult, calmly and easily. She takes him seriously, too, and they have a short conversation that makes me jealous. If Walt were here now they would see that I am grown up as well, that I have a big friend who likes my company.

‘Hey, you,’ Jan gives me a companionable nudge, his shining eyes close to mine, ‘we’ll be going back home in a few days. On the boat, my mother says, that’ll be fun.’ He drums his fists in a friendly way on my ribs and then, in an outburst of exuberance, throws an arm around my neck.

‘The two of you are sure to be inseparable,’ says my mother, ‘you’ve had such a lot of good times here together.’

‘Oh yes, lots,’ Jan shouts with laughter. ‘Hey, Jeroen?’

Next we make for Warns, walking straight across the fields. The sun is hot and the maze of ditches and fences — ‘No, Mum, those aren’t bulls, they’re cows!’ — makes us tired and sweaty.

‘Let’s just sit down for a moment,’ she says. I walk along the edge of the ditch trying to find an egg for her, a lapwing’s or a duck’s.

‘Don’t bother,’ she calls, ‘just come and sit down with me. I don’t want an egg.’ She almost disappears among the tall, shiny buttercups and tufts of grass and points in surprise at all the little beetles wheeling about in the sunlit patches of water between the duckweed. ‘We shan’t go back ever,’ she sighs. ‘Daddy will just have to move here.’

She picks a bunch off flowers, yellow and white, mixed with plumes of reeds and pink clover, and walks across the field in her yellow dress carrying the bouquet, and then through the village: suddenly I feel proud of her, now I too have somebody; look, this is my mother!

The church, the crossroads, the school, then I don’t want to go any further.

‘What’s wrong with you, suddenly,’ she says. ‘Don’t be a bore, all I want to see is that bridge Meint talked about such a lot.’

‘But that bridge isn’t there any longer,’ I say, ‘it was all smashed up and there’s a new one now. And it’s a long way, quite far from the village. We’ve got to get home.’

Silently we walk back along the twisting road to Laaxum and all at once I know for sure exactly where the man with the bicycle let me off that time when he went back to fetch the suitcase I had left behind. But I don’t tell her that.

Mem has put the faded flowers in a jug on the table and keeps brushing fallen petals and pollen from the table-top. My suitcase, too, sits on a corner of the table and my mother is busy repacking everything into one of the panniers. I want my clogs to go in as well, I insist on that, but the straps won’t close.

‘Then I’ll wear them tomorrow.’

‘But you can’t do that, darling,’ she says, ‘you can’t possibly be thinking of going to Amsterdam in those things.’

Offended, I walk out of the room and Mem walks after me.

‘Come on,’ she says, ‘don’t take on so, your mother’s only just come. You’re making too much fuss.’

I stand alone by the fence and look across to the little tower in Warns, I feel an inexpressible ache in my body, a longing that crushes me and that is screaming at the top of its voice inside me. I want to stay behind and wait, stay here until inundated by the smell of the earth, until turned into the same substance as the ground beneath my feet, riddled through by the roots of grass, by the grubbing about and the digging of insects. I want to stay at one with these surroundings, with the ground where I lay with him, stroked his skin, breathed his smell, felt the warmth of another human being flooding over me, throbbing, shaking, taking possession of me.

## 12

TO LEMMER. We cycle over the dyke and when I look back I can see the handful of houses that is Laaxum lying down below. On the other side, the elongated shape of the Mokkenbank stretches out some way ahead of us, a waving green sheet of reeds enclosed by a restless, greyish-brown sea. I can see and hear the gulls tumbling and swooping about, as multitudinous and fidgety as clouds of mosquitoes.

We have to leave home early to catch the boat. When we say our goodbyes, the whole family gathers at the back of the house, standing stiff and solemn in a semicircle, and I can see how anxiously Mem follows every movement I make.

‘Come,’ says my mother, ‘we must be off. Say goodbye to everyone.’

My heart knocks and thuds. I daren’t look them in the face, feeling like a traitor, a coward. I shake hands with Popke, then Trientsje, then go along the row shaking hands with everyone. I feel torn apart and when it is Mem’s turn I fling my arms around her neck and burst in to a fit of tears that reaches wrenchingly and quiveringly deep inside.

Hait takes us to the road, opens the gates for the bicycles and helps me onto the luggage carrier. Jan and his mother are standing waiting for us on the road. There is a lot of excited talking, waving and shaking of hands, and people come out from the other cottages to look and to wave goodbye.

‘Upsy-daisy,’ says Hait, ‘off you go.’ He gives my head a quick hug. ‘Write Mem a nice letter soon, now.’

I grit my teeth and bite my lips so as not to cry aloud, but my face is wet and what with the tears and my nervous state I no longer see anything.

How is it that everything has happened so quickly? For months I have been waiting and waiting and now suddenly everything has come down on me like an avalanche and dragged me away before I have even had time to think about it.

‘Is everything all right, Jeroen, are you sitting comfortably?’ I can hear the concern in my mother’s voice, but I cannot answer.

‘We’ll just have a nice day and if we miss the boat, well, that’s just too bad. We’re not going to rush,’ she says to Hait.

From the sound of her voice I can tell that she is afraid to mention my leaving and that she is trying to take my mind off it.

I have put my hands carefully on her hips so as not to fall off, but I don’t like the feel of another body. The material of her dress is thin and soft under my hands, yielding like water, and through it I can feel the rotating movements of her legs. And is that her skirt or the wind blowing against my bare knees?

Jan and his mother are cycling behind us. In the distance, disappearing into the sparkling green landscape, lies our little house. They are still standing outside and waving, like tiny fairy tale figures in front of a doll’s house, far away and of no significance.

I wave back.

‘You wave, too, Mum.’

The heavily-laden bike gives a dangerous lurch and she quickly looks straight ahead again. The gulls fly upwards in screeching clouds. I look out for the spot where I sat with Walt, a sandy little patch of ground at the edge of the reeds. Has it been overgrown or have we just passed it?

We stop and my mother gets off; the flapping of the pannier’s little strap between the spokes is irritating her. I peer vainly into the jigsaw puzzle of sand, reeds and water while I hold the bike for her. It’s hot already and we still have a long way to go: I can see dark sweat patches under the short sleeves of her dress and her face looks tense.

Jan and his mother bicycle past us.

‘Beat you to it!’

Nobody is standing outside the little house now, or is that someone walking across the meadow to the road? The sun falls in fitful patches over the landscape and the fields dance in an abundance of yellow flowers.

‘Don’t just stand about like that, try and hold the bike up properly.’ Her voice is suddenly hurt and desperate. ‘It looks as if we’ve got a puncture, the tyre is much too soft.’

If anything is wrong we’ll have to ask Jan to fit it. I’m still as bad with my hands as I was before I went to Friesland.

We are back on the bicycle again and I try to make myself as light as possible.

We pass a young farm-hand.

‘Hullo, Jeroen.’

I greet him proudly, feeling quite a man: this is my world, these are my friends. They know me here, she can tell that, can’t she? The boy stops in the road, but we carry on past him.

‘We’re going to Amsterdam,’ I call back to him importantly.

My heart lifts, I look at the passing farms, the gardens, the animals scattered over the fields. The changing scene diverts me and imparts a feeling of adventure. I lean forward past my mother to see if we are catching up with Jan.

‘Keep pedalling, Mum, they’re miles ahead. Don’t let them leave us behind.’

Life has turned into a light-hearted game. We pass the Gaasterland woods and I look out for the house; isn’t that the path between the trees? What if they are still there, what if I should suddenly spot his car?

‘Do you think the Americans will still be in Amsterdam when we get back home?’

She is out of breath and her voice sounds laboured, as if every word were too much for her. ‘I told you not to worry, they’re going to be around for quite a while yet. And they’re not Americans, they’re Canadians.’

If we stopped, I could look around, even explore a little along the path. The road seems familiar, those overhanging trees and the low bushes on the verge.

‘How about a short rest, Mum? I’m as stiff as a board.’ It’s no more than the truth, because I have to sit on the luggage carrier with my legs spread wide across the two full panniers and the insides of my thighs feel as if sandpaper had been run over them. But my mother pedals on obstinately until we catch sight of the other two by a side-road.

We sit down on the verge and Jan’s mother unwraps a packet of sandwiches. ‘It all seems to be forgotten again,’ I hear my mother say in a half-whisper, ‘how quickly things change: one moment they’re crying and the next they’re having the time of their lives.’

I chew my sandwich and look down the side-road for a roof between the trees with a small dormer-window. When I think of that room I can still smell the wood and the blankets on the mattress.

Jan, who is already starting on his third sandwich, sits leaning against a tree on top of the verge. He flicks off the ants running over his legs with his fingers, aiming them towards me. ‘Nice, a trip like this, isn’t it? You get to see quite a bit.’

He doesn’t realise that I have been here before and much further too, with an American soldier, in an army car. From where I am sitting I can look up his trouser leg, up his smooth, slim frog’s thigh. But I am unmoved and can hardly believe that once upon a time — long, long ago — I used to yearn so desperately for that body, that I would dream about it, torturing myself with the mere thought of it.

I walk as nonchalantly as possible to the other side of the road and peer between the trees to see whether there is a clearing or an old railway track inside that wall of green. The hairy legs tighten around my hips and the jaw rasps painfully across my neck and cheek while I listen to the breathless voice: *‘Hold it, yes, go on, move. Yes.’*

The bicycles have been lugged up from the verge with much groaning and sighing and they are all looking at me impatiently. Jan’s mother rings her bell. ‘If you don’t hurry up you’ll have to walk to Lemmer!’

Awkwardly, I manoeuvre myself, hopping and wide-legged, onto the luggage carrier and aim a furious kick at the bulging panniers.

‘Try and put up with it a bit longer. Just another hour, I think, and then we’ll be in Lemmer.’

She is pedalling again, the chain creaking and the wheels making heavy weather of the forest path. I am tired, I want to rest my head against her back. Any moment now I’ll fall off the bicycle. There is rye-bread stuck in my mouth which has a sour taste. I run my tongue over my teeth and chew on a left-over morsel.

If only I can remember Walt’s taste, his breath and his spittle in my mouth. But how do you do that, how do you remember a taste, a smell?

## 13

TWO MEN IN blue overalls are standing by the boat, one carrying a rifle and looking ready to go into action any moment.

A small group of people has gathered on the quay, but to join them we must go past the two men.

‘Come on,’ says my mother, ‘nothing to be afraid of.’

I look back for Jan who is still standing with his mother by the small office where we paid for our boat tickets.

‘They’re coming. If we hurry we’ll be able to find a good spot on the boat.’

I walk with her up to the men, who ask to see her identity card, open up the panniers on the bicycle and search them thoroughly and suspiciously.

‘What on earth are you after, I thought the war was over?’ She says it curtly and yet her voice sounds cheerful. When the panniers are closed again, they start joking with my mother and I hear one of them call her ‘sweetheart’. I feel excited and almost flattered because they are being so nice to her, but at the same time they make me nervous and their easy laughter infuriates me.

‘Is the boy with you?’ they ask. ‘Is he going on the boat as well?’

‘He’s my son. He’s been in Friesland for a year and now we’re going back to Amsterdam. I’m fetching him home.’

The conversation comes to a sudden halt. Someone is bringing our bicycle up the narrow gangplank and we follow behind carefully. I can see the black water below and hear it making gurgling noises between the ship and the quay.

The ship looks enormous to me, bright and white and full of little stairs, doors and corridors. The wooden floors are wet, the puddles giving off a soft salty smell, reminding me of holidays and of Walt’s tent, of lashings of sweet-scented security.

‘Come along,’ says my mother, ‘let’s go and sit up on the deck, it’s lovely weather.’

We clamber up an iron stairway and I run to the rail and look down. The little town is quiet, there are just a few small houses around the harbour. I can see a baker’s shop, some fishing boats, a large pile of crates and the two men leaning against a fence and concentrating on smoking a cigarette.

‘How quiet it is,’ says my mother,’ I think there’s hardly anybody else on board. Can you see Jan and his mother?’

As I bend over further I hear Jan’s voice and see him running up the stairway.

‘Wow, what a great place.’

We race along the railing and go down another stairway, making for the front of the ship to watch the crates being loaded on board.

‘See that fellow? What a he-man,’ Jan says admiringly. But, secretly, I compare the man’s arms with the ones that I know, and look away unimpressed.

Our mothers are sitting silently on a bench in a sunny corner of the deck, suddenly looking tired and anxious. My mother has laid her head on her arms, looking across the water. Jan’s mother lies slumped over her bag with her eyes shut.

‘Are you tired, Mum?’

‘Just leave us be for a moment, darling. We’ve been bicycling for three hours, with you on the back, and we’re not used to it.’

I sit on the floor next to the bench and run my hand over the silky, uneven surface of the planks. The sun is hot and dazzles me. I look at the pannier and wonder what other food might be in it. Then the boat begins to judder, the planks shake softly under my hands and the trembling spreads right  through my body. After as light lunch I see that the quay has moved, that the houses are slipping away from us. I jump up and run over to Jan who is calling to me and hanging over the railings gesticulating wildly. Then there is a deafening noise, a shattering, almost unbearable hurricane in my ears that nearly scares me to death: the ship’s funnel emits a dark cloud of smoke like black vomit being blasted up into the sky.

My mother has clapped her hands to her ears, her face looking pale and miserable, and suddenly I realise that this is farewell to Friesland. I run back to the railing and stare at the harbour which already seems an amazing distance away from the boat.

My mother puts her arm around my shoulder. ‘Here we go,’ she exclaims and I have to listen very hard to hear what she is saying. ‘Take one more look. But next year we’ll go and visit them, that’s allowed again now.’ She sighs and then gives the receding little town an impulsive wave. ‘Bye Friesland!’ She pulls me towards her and suddenly everything feels pleasant and carefree, as if nothing had happened. ‘That adventure is behind you now.’ Her hand touches my hair lightly, and I feel an irrepressible longing for the hand that slid down my back fingering me roughly and yet carefully and attentively. Suddenly I feel sick.

‘Are we going to have anything to eat? My stomach hurts.’

‘In a moment. We’ve only just started. Go and find Jan but don’t stay away too long.’

Listlessly I walk through the ship. The flag flaps above the bubbling water, making the same sound as a big fish flung by Hait onto the deck where it would beat on the planks, thrashing about, contorted with desperation.

The ship’s propeller churns up clouds of sand and air in the water and rolls them up to the surface, creating an eddying grey maelstrom, binding us to the mainland like a living, twisting cord.

The small town and the coastline have become a blur, an unknown domain I have never visited and at which I now stare with a stranger’s eyes. The sea is covered with scales of glittering and sparkling light that hurt my eyes. We have been cast into the void, adrift in a shoreless sea, and I would not be in the least surprised if Amsterdam never appeared, if our passage turns out to be a journey without end. Amsterdam, what is that to me now, what am I going there for?

Jan has joined me; he spits overboard and tries to follow the course of the blobs until they touch the water.

In the distance ships lean in the wind and I strain to make out the letters on their brown sails. Imagine if Hait suddenly drew up alongside to give me a surprise and I saw those familiar faces close-to again: Hait, Popke, Meint. Aren’t they coming nearer, isn’t one of them getting a little bit bigger?

The water glitters and the wind blows tears from my eyes. The little boats have hardly moved. When Jan calls to me, I quickly turn my head away in case he thinks I am crying.

‘There are soldiers on board,’ he says a little while later. ‘They’re keeping to themselves, they’re probably not allowed to mix with us.’

I almost force him to take me then and there to where he has made this discovery, and a moment later, peering furtively through some little round windows, I see the green uniforms and hear the familiar sound of voices speaking an incomprehensible language.

We clatter down a stairway. How can we find where they are, where is the way in? People are sitting about who look at us silently, bags and parcels beside them. They seem tired and worn out.

‘Psst, come and look at this.’

We go into a w.c. cubicle with a sharp, penetrating stench. Jan points up to a corner of the wall where a small sign has been scratched, a sort of circle with a line through it and a dot in the middle.

‘Cunt,’ says Jan triumphantly. He laughs and slams the little door shut. I hear him racing up the iron stairs.

Our mothers have gone inside, because it’s too hot on deck, they say. We have a drink of milk from the bottle Mem gave us and eat a clammy sandwich. ‘The bread in Amsterdam must be less coarse than this by now,’ says Jan’s mother and I wonder how this can be with the war hardly over.

I feel myself getting furious: they’d better not start running Friesland down all of a sudden.

Jan leans against his mother and goes to sleep; my mother has shut her eyes as well. There is a leaden silence and scarcely a breath of wind comes through the little open windows. For a while I listen to the muffled thudding of the engine, then I get up and walk quietly around the deck until I find the windows where we heard the voices. There is the sound of soft music and of people talking so low that I can hardly make anything out, even when I stand right next to the porthole. As soon as anyone comes by, I go quickly and lean over the railing, pretending to study the sprawling waves, but a moment later I am back trying to look into the little half-open window to catch a glimpse of what is going on inside, waiting for the sound of a familiar voice.

Suddenly someone is looking me straight in the face, then with a furious tug the small curtain is drawn.

<br>

Enormous numbers of close-packed houses, roofs, cranes, the commanding dome of the Central Station and everywhere people and bicycles, a mysterious, bustling, gloomy world: that’s how Amsterdam looks to us as we watch it slip by from the deck. Even Jan is silent and subdued. He leans on the railing, looking tired, and has nothing to say.

Army cars are parked alongside the station, not just a few but scores of them. I pretend to be looking elsewhere, at the milling crowd on the waterside, at the crowded quay, and I try hard to make conversation with my mother. But my mind is elsewhere.

‘Come on, pick up your things,’ she says, ‘it’s time we went down.’

We stand beside our bicycles anxiously and nervously, ready to disembark as quickly as possible.

‘Stay by me,’ says my mother, ‘hang on tight to the luggage carrier, or else I’ll lose you in the crowd.’

A lot of people are standing on the landing stage, some craning upwards as if looking for somebody, but most seem to be hanging about aimlessly. We go down a gangplank much broader than the one in Friesland, hemmed in by the other people from the boat. Dusk is falling and I feel cold.

A lot of people are standing on the landing stage, some craning upwards as if looking for somebody, but most seem to be hanging about aimlessly. We go down a gangplank much broader than the one in Friesland, hemmed in by the other people from the boat. Dusk is falling and I feel cold.

‘Isn’t Daddy coming?’

‘Of course not, he doesn’t know when to expect us, this way we’ll be giving him a surprise. He’ll be at home as usual, looking after Bobbie.’

Bobbie! When I get home there will be a baby, a little brother waiting for me in this dark, mysterious city.

We tie on the luggage a bit tighter as Jan jumps up and down impatiently, dying to be off. As we start bicycling I can see that the soldiers, wearing rucksacks, are just coming off the ship. I wrench around so suddenly to look at them that my mother nearly falls to the pavement. Then, like an oppressive shadow, the back of the station engulfs us.

<br>

The evening sun shines over the Rokin where hundreds of people are milling about. Everywhere there are flags, placards, decorated lampposts and triumphal orange arches, an overwhelming riot of colour and sound.

And everywhere I can see uniforms and army cars, driving around or parked in the street; a truck full of singing soldiers brushes right past us. I shall find him again, of course he is here. I wonder if he is living in a house or if they have put up tents somewhere, near us perhaps, on the field by the Ring Dyke.

His bare legs will fold around me again, his fingers close teasingly about my thin neck. Keep bicycling, Mum, come on, quickly, I want to get home, I want to make plans!

We swing off towards Spuistraat and at the turning I look at Jan, waving both arms enthusiastically at him. ‘Don’t be silly, hang on tight or we’ll have an accident.’ I cling to the yellow dress again and feel the rotating hip joints.

The Dam is like an ant-heap, impossible to get through, and the street alongside the Royal Palace has been blocked off. We have to dismount and make our way through the crowd on foot, my mother ringing our bicycle bell for people to make way. I peer over the barrier into Paleisstraat; it looks eerily empty and deserted, as if some disaster has struck it. That’s where the lorry had stood, parked right next to the Royal Palace. The sparrows twittering in the sun seem to be the only thing I can still remember clearly.

The Rozengracht, and we get back onto our bicycle.

‘Mummy, a tram! Are the trams running again?’

We bump over the uneven road and I become steadily more excited as I recognise more places and as everything starts to look familiar and to tie in with my old life. On Admiraal de Ruyterweg workmen are busy laying sleepers under the rails, the large piles of wood surrounded by groups of curious neighbourhood children.

‘See how hard they’re working? They go on right through the night. Everything has to be repaired.’

I say nothing. We’ll be home in just a moment. How dark and bare it looks here, all the trees gone. Across the big stretch of sand I can see our housing block, sunny and out in the open, and children digging holes and chasing one another through the loose sand, shouting and laughing.

My heart beats violently and my joy evaporates. The boys, school, break: ‘Wait till we catch you alone, you’ll get what’s coming to you after school!’ Fleeing back home, back to the safety of our doorway.

Our street. Open windows and balcony doors, and yet more tricolours, stretched-out lines with little fluttering flags, strange white stripes chalked down the street like a sportsfield.

And, in the middle of the street, the patch of grass. The remaining rose bushes are in bloom.

I feel dizzy and dejected. What I would like best would be to disappear and arrive back home unseen. As it is, a lot of people seem to be standing at their windows especially to have a look, and when we dismount outside our door children come running up to the bicycle.

I shake Jan’s mother’s hand to say goodbye. Jan has already run across the road and is shouting up to the balcony where his father and little brother are standing. I walk quickly across the pavement past the children and bolt inside the building. I am so confused that I haven’t taken in who anybody is.

My mother puts the bicycle in the lock-up and I stand in the doorway and peer inside, listening to her rummaging about in the dark and untying the panniers.

‘Will you carry this?’ As we go up the stairs I can hear voices, and without having to look I know that somebody is hanging over the banister and looking down the stair-well.

The neighbours and my father are standing on the secondfloor landing. He is beaming all over — What a funny face, I think — flings his arms around me and pulls me close. Dazed and numb I allow it to happen, and then we go on blankly to shake hand after hand.

‘Leave him be,’ says my mother in a whisper, ‘he’s tired, we’ve been travelling all day.’ And then, louder, ‘Hey, Jeroen, we’re going to go up now and then you can get in your own bed, won’t that be nice?’

‘Doesn’t he look terrific?’ she asks my father when we are upstairs.

They are standing side by side in the passage and look at me with the eyes of children who have wound up a toy animal that then begins to move as if by magic. ‘When you hear him talk you’ll hardly understand him. He’s turned into a real country boy.’

Inside, I see immediately that the furniture has been moved: this is not the room I kept thinking about in Friesland, this one is different and strange. Against the wall, where the sideboard ought to be, there is a baby’s highchair. A small, solid person with fair, curly hair is sitting in it giving me a frank inspection with expressionless eyes as if to say, ‘What on earth are you doing here?’

‘Give him a kiss, go on.’ My mother pushes me gently towards him, but he begins to scream and leans far out towards my mother. Is she my mother any more? She lifts him out of the chair and tries to turn him towards me.

‘Yes, sweetheart, Mummy’s back, don’t cry. Just look who’s come. That’s your big brother, he’s back with us for good now. Say “Hello, Jeroen”.’

She tries to drown the deafening screams. ‘He can say a few words already: dada. Go on, say dada. He can say mama as well and we’ve tried to get him to say Jeroen, sometimes he says toon.’

But the fair-haired little boy doesn’t say toon, it takes quite some time before the house is quiet again and I am depressed and aggrieved to see how often they keep on going to the cot to try and calm him down.

‘The shower isn’t working yet,’ says my mother. ‘Shall I give you a wash in the kitchen?’

How can I tell her that I’d rather do that on my own. ‘In Friesland,’ I say, ‘we always had to wash ourselves.’

She puts out a tub in the kitchen and mixes a kettle of boiling water with cold water from the tap.

‘You can go now,’ I say, and shut the kitchen door.

I remember in amazement how, before I left, she would soap me down every Saturday night while I stood shivering beside the little tub.

‘Here are some clean underclothes, I shan’t look, no need to get alarmed,’ she says teasingly and her arm places some clothes on the edge of the kitchen dresser. I allow her back in when I have put on my underpants.

‘What’s that?’ she says and takes hold of my arm. The wound festered and there is still a scab.

‘Nothing. Hurt myself playing.’

After that I am allowed to sit out on the balcony for a little while. It is dark but warm and we drink ersatz tea. I can hear the tinkle of cups from the neighbours’, and voices through the open windows, peaceful, unimportant sounds that make me feel sleepy. Down in the street Jan is already playing with the boys as if he had never been away.

I move back a bit and sit where no one can see me from downstairs. The street resounds with shouting and singing.

Inside my mother is unpacking the panniers.

‘Bacon,’ I hear her say, ‘real bacon. And rye-bread. And that’s sheep’s cheese, just smell it. They made that themselves.’

I go back inside quietly.

‘Poor dear, he’s worn out,’ she says compassionately. ‘Come, I’ll make up your bed, it’s been quite a day for you, hasn’t it?’

My father lets the bed down. I give him a kiss. Go now, I think, leave me by myself. But he lingers on.

My little room looks bare, lifeless. Everything has been tidied away carefully and is in its proper place.

My mother pulls the blankets up to my chin and sits down on the bed. ‘It’s nice at home with your own mummy and daddy, isn’t it,’ she says enthusiastically. I seize hold of her arm. Who can help me, what will happen to me? ‘Tomorrow we’ll go up to town together, there are celebrations all over the place,’ she says, ‘in our street, as well. How wonderful that you’re back in time to join in. There isn’t such a lot happening in Friesland, is there?’

She opens the window a crack and draws the curtains.

‘Off you go to sleep now. You’re sure to have some wonderful dreams.’

I listen until the door has been pulled to. Then I bury my head deep under the blankets.

# Freedom and Joy

##  1

THERE ARE SOUNDS all around me: the clumping of footsteps on the floor above us, the rattle of dustbin lids on the verandas, voices in the street, a song echoing over the gardens across the road. Is it late? I close my eyes again and try to shut out the day. My bed feels good now that I am awake, wonderfully soft and yielding, and I let myself sink into it as into an embrace. During the night I woke up with the anxious feeling that I was sliding into a substance from which, try though I might, I could no longer extricate myself. I had finally struggled upright and had sat on the metal edge of the folding bed, panting and worn out. The little room looked ghostly and unreal: walls that seemed too far away, dawn glimmering in the wrong corner, unfamiliar sounds. Confused, I had slipped back under the blankets.

<br>

At breakfast, in our room that seems spacious and sunny and where there are only three instead of nine sitting at table, I feel like a guest who has merely spent the night and is about to set off again.

I can’t get used to the new way the furniture is arranged.

‘Did you sleep well?’ my father asks. I can hear the probing tone in his voice.

‘Yes, thanks. But the bed is so soft, I’m going to have to get used to it again. The cupboard-bed in Friesland was made of wood.’

‘You shouted out in the night, do you know that? You don’t have to be afraid any longer, everything is over, you’re back at home now. Life is back to normal again.’ My mother lays a hand softly and protectively on mine. My forehead begins to tingle, a feeling of anxiety comes over me, but I daren’t draw my hand back. Nobody may touch me, it’s not allowed. She puts a big square biscuit on my plate.

‘Would you like some of this? Don’t pull such a face, it’s a ship’s biscuit, delicious! From the Canadians.’

I smell it and push the thing away. ‘Isn’t there any ordinary bread?’

She goes to the kitchen and cuts a slice of the Frisian rye-bread. ‘I thought you might want to eat something else for a change.’ She looks disappointed.

In the little side-room my brother is making spluttering noises in his cot. My mother gets up and takes him in her arms. She kisses him, holds him up in the air at arm’s length and then pats his wet nappy fondly.

At the table she feeds him bits of ship’s biscuit dipped in tea and his little chute-like mouth becomes covered in brown blobs. He waves his arms about wildly and uncontrollably, knocking the spoon out of her hand so that the tablecloth, too, becomes covered with mess.

‘Da-da-da-da,’ he sings, happy, ignoring me completely, as if I am not there, as if everything is the same as always.

‘Jeroen,’ my mother prompts him. ‘Toon-toon.’

‘Da-da-da.’

‘You’ll have to repeat the sixth year,’ my father says. ‘We’ll just have to write off this last year, they can’t have taught you all that much in Friesland.’

Who says so? How do you know? Aren’t the Frisians good enough for you all of a sudden?

‘We’ve found you a 6A class where they prepare pupils for high school. It’s quite a  long way from here, in town, but they’re sure to bring your maths up to scratch.’

I go back into my room and sit down on the unmade bed. My books are stacked in a a neat little row, my toys on a shelf in the cupboard, the drawing-book, the pencils, my sponge-bag, everything is there, all my things. When I was in Laaxum I missed them, but now I don’t even touch them. I am listless and anxious and don’t feel like moving.

My little brother is crying, whining and whimpering. I can hear my mother talking in a subdued voice out in the passage, as if I were still asleep.

‘I’ll take him outside for a bit of a walk, the weather’s lovely. Then we can leave Jeroen here a while longer, he still has to get his bearings back.’

For a long time I sit there motionless. What shall I do? Quietly I open the door. My father is standing in the kitchen.

‘Well, did you give your room the once-over? Is everything just as it should be?’ There is a teasing undertone to the way he says it. He goes out onto the veranda and hangs a row of nappies on the line. When I go and stand beside him and look over the railing, he kisses my hair. ‘Great to have you back,’ he says, ‘we missed you.’

I look at the green gardens, the canal and the cultivated fields beyond. There is a barn and vegetables planted in long straight rows. My mother is sitting on a bench by the water, a pram next to her, and she waves when my father calls her name. She looks pretty and happy. ‘Come,’ she beckons to me.

‘Go and get some fresh air, sitting about indoors won’t do you any good. Soon you’ll be stuck inside school all day again.’

We walk through the house and my father shows me everything. ‘Look, we’re back on the gas and the electricity. The radio is going again too, just listen. If you turn this little knob you get music.’

‘Perhaps I’ll just go into town this afternoon,’ I say, ‘to have a look what’s going on.’

‘You go and have a look round, there’s a lot to see, celebrations and exhibitions everywhere. I’ll come later in the week and we’ll look at things together.’

He watches me as I go down the stairs and turn round once more, as if I am asking for help. Downstairs I stop inside the main door for a while, summoning up the courage to open it and venture out into the street. Then I run around the corner and sit down on the bench beside my mother. She is rocking my brother’s pram to and fro, first forwards, then backwards, making me sleepy just watching.

‘Go and find the other children, they’ll be glad to have you back. You must have a lot to tell them about where you’ve been.’ But I stay sitting next to her, clutching her arm as she rocks the pram. When a soft drone of engines can be heard she says that it’s sure to be the Canadians. ‘They’re quartered in your school, but I don’t know for how much longer. Go and have a look. Sometimes you can see a whole procession of cars passing by.’

At the end of the street, by the White School, I watch two cars being driven out of the playground and disappearing around the corner. Men in the familiar green uniform are walking about in front of the wide-open doors that lead into the gymnasium.

So, they’re here, right next to our house...

Suddenly, I run back home and sneak up the stairs, feeling almost caught out, ashamed and agitated.

‘No,’ I tell my father when he looks surprised to see me back so soon. ‘I’m not going to go out today, I’d rather stay at home, I don’t want to go out on the street.’ I walk onto the balcony and look at the school, at the open gym and the soldiers’ comings and goings. I feel sluggish and washed out. Should I go and look to see if he is there? And if he is, what then?

I go into my little room to sit down, and notice that some of the pages from my books have been ripped out while others have been covered with big scratches. The work of my little brother, that’s for sure. Restlessly I leaf through the books, read a line here and there, look at the pictures then push the books to one side again.

My father puts a small plate with neatly cut sandwiches and a peeled and quartered apple down next to me.

The terror inside me wells up, takes possession of me slowly and paralyses me. I don’t ever want to move again, ever go down in the street again. I don’t ever want to see anybody again.

I put the plate down on the windowsill: I don’t like the food here either. Nothing in Amsterdam tastes of anything.

<br>

In the afternoon my mother takes down the nappies while I sit on the kitchen doorstep and watch her quick hands collect the washing. She hangs other pieces of clothing on the line: my shorts, underwear, my towel, the socks knitted by Mem.

I stiffen when I suddenly realise what has happened: the wet shirt with the small pocket is hanging from two pegs among the other washing. The photograph.

When my mother has gone into the kitchen, I slip my fingers quickly into the opening in the shirt. How could I have forgotten! I touch a small, crumpled wad but keep on fumbling wildly and incredulously. Then I pick it up between my fingers and unfold it, a matted mangled bit of paper.

‘What have you done?’ My voice sounds distorted and shrill. ‘There was a photograph in my shirt. Couldn’t you have waited?’ I almost scream the words at her.

My eyes fill with tears. Back in my room I fling myself onto the bed and squeeze the scrap of paper desperately between my fingers.

‘I don’t know what’s got into him.’ From the sound of her voice I can tell my mother is crying as well.

My father soothes her, their voices disappearing towards the living-room. When the door shuts there is a deathly silence.

<br>

‘If you don’t go down to play today I’m just going to push you out of the door,’ she says next day as she sets a cup of warm milk before me on the table. ‘Don’t you turn into a stay-at-home, you hear? Children are meant to play outside.’

She talks gently and I can see that she is smiling. And yet I hear the anxious undertone in her voice and her whole attitude suggesting that she is feeling her way with me.

After looking out of the window to make sure none of the boys are outside I decide to go downstairs. I run towards the canal and then, as if doing something that is not allowed and that mustn’t be seen by anyone, I turn down another street towards the White School. It feels as if everyone is watching me, that they are wondering what I am up to, why I am there and what I am going to do.

I walk past the school fence alongside the deserted playground. There is a broken-up-for-the-holidays air about the place: thick clumps of grass and weeds are sprouting here and there between the paving stones, and the narrow flower-beds under the windows look neglected and overgrown. Most of the classrooms appear empty, the plants and drawings on the windowsills gone, all the colour and gaiety vanished. From an open window comes the loud voice of someone talking over the radio in a foreign language. I stop and fumble cautiously with one of my socks, trying to look inside unnoticed. One classroom has been turned into a store, piled high with crates and bags, and my own classroom has been converted into a kitchen or dining hall, the windowsill lined with cooking pots and metal pans. In the gym there are long rows of narrow beds on crossed legs; rucksacks, books and helmets stand underneath them in orderly array. The recognition of all these things throws me into total confusion. I want to walk away but I remain where I am with my fingers clutching the wire netting of the fence, listening to the sounds echoing across the square, the metal clink of boots, a car starting up, the laughter sounding through an empty classroom.

It seems that the whole street is staring at my back, as if my clothes bear a message writ large: this boy is in love with a soldier. ‘The slimy creep, that dirty sneak,’ I can hear them call after me in my imagination A soldier crossing the playground looks at me and takes a few steps in my direction. He rummages about in his pocket, whistles, and then flings something at me as to a dog: a colourful, hard little object which lands on the ground by my feet. I stiffen and walk away.

Back in our street a group of boys is sitting on the pavement. Appie is there, and Wim and Tonnie. I go and stand beside them, leaning against the wall and listening to what they are saying. Wim turns round to look at me. ‘Hey, are you back too, then? Did those peasants stuff you full of food?’ It doesn’t sound unfriendly, at worst mocking, but I feel hostility and a great gulf. The boys laugh. Should I sit down with them, behave as if everything is back to normal, as if I have never been away and nothing has happened to me?

A squad of soldiers comes marching down the street and the boys run over to them, leaping around them, shouting and drawing attention to themselves, then fawning on them sweet as sugar.

I am shocked by the liberties they take, grabbing hold of the soldiers’ arms and running along with them like that.

*‘Hello, boy, How are you? Cigarette, cigarette?’* they pester, and the soldiers smile back at them, seeing nothing bad or rude in that sort of behaviour.

I am ashamed and jealous at the same time. I should love to walk along like that and be so free and easy with those soldiers, laughing up at them and holding on to their clothes. Even with Walt I shouldn’t have dared to act like that, it would never even have occurred to me.

Tonnie walks slowly back to me and nonchalantly brings a small box out of his pocket. There is a picture on it of a bearded man wearing a sailor’s cap under which you can see sandy hair.

‘Players,’ he says, opening the packet. I see two smooth round cigarettes lying at an angle inside the silver wrapping. ‘Want to swop? What have you got?’

When he realises that I have nothing he looks surprised and then disdainful. He calls the others over. 

I must try to get away, I am wasting time when I could be searching, watching the classrooms, keeping an eye on the cars. When the boys get into a huddle in a doorway, their heads conspiratorially close together, I disappear around the corner and run to the other side of the school.

There they are, a whole lot of soldiers sitting on the running-board of a car, on the paving stones, some in a school window with their legs dangling out they are like a group of animals basking in the sun on a rock. They talk, polish their gear, laugh, laze about; one of them even has a needle and thread and is mending something.

Brown arms, pliant neck under short-cropped hair, nonchalantly outspread legs, a young boy squatting on his heels who confuses me so much that I dare not look in his direction: the wad of a photograph, in my cupboard, the face that has been lost. I feel the skin of my cheeks tighten and my eyes start to burn.

A small open car comes driving across the playground with three girls sitting in it, their arms wound provocatively around each other’s waists, bursting again and again into noisy giggles, doubling-up with laughter then collapsing against one another. The soldier at the wheel has huge shiny arms and deep black eyes that, half-amused, half-bored, take stock of the other soldiers who come running up to the car, shouting incomprehensibly. The girls shriek, allowing themselves to be lifted out of the car. A moment later, they go inside the school, tittering and holding tightly onto each other, followed slowly by the soldier with the muscular arms. His hands are in his pockets and he has a powerful, bow-legged walk.

‘And have a guess what they’ll be doing inside?’ says one of the boys who has come to stand by the fence a short distance away from me.

‘They’ll be having it off,’ bawls another and rams his body against the railings, setting off a devastating din.

He is just a monkey in a cage, I think, the whole lot of them no better than animals the way they carry on making an exhibition of themselves. Why can’t the soldiers see that? But the soldiers are gesturing to the boys, pulling faces and giving them knowing winks.

I am cross, and jealous of everyone, of the boys, of the giggling girls and of the soldiers. Why are they looking so pleased with themselves, what are they doing inside, why is there such a strange air of excitement about the place? I don’t even think of that other thing, because that was only between me and him. The girls are probably being given rice with raisins right now, or chewing-gum.

But why should I care? I must go into town and look around some more, try to find those places I saw from the boat, where the cars were parked.

I walk as far as Admiraal de Ruyterweg and suddenly, when I see all the buildings and the streets full of people walking about, queuing outside shops or busy repairing the pavements and the tramlines, I daren’t go on. Must I really go through these crowds all by myself? What if I lose my way, or if something happens to me?

But I still have to find him, as soon as possible. Every day I fail to look for him is a day lost. He may still be here today, but tomorrow he could have gone.

I walk another block, my anxiety growing all the while, then I turn on my heels and run back home without stopping. 

I thump on the front door, fly up the stairs and stand panting in the passage.

‘Was it fun playing with your old friends again?’ asks my mother. I pretend not to hear and disappear into my little room. When, cautiously, she opens the door a little while later, she gives me a worried look.

‘Nothing happened, did it? You didn’t get into a fight? If there is anything the matter, you will tell me, won’t you? Promise?’

##  2

THAT EVENING THERE are celebrations in our street.

Loudspeakers have been tied to two lampposts, and every so often a man’s voice comes out of them, cutting right through the cheerfully blaring marching music to address the people of the neighbourhood. ‘You are all invited to join in this evening’s festivities and enter the competitions for the attractive prizes you will see specially displayed. The celebrations will be graced with the presence of a neighbourhood personality well known to one and all, whose name I am not yet allowed to divulge but who will be treating us to her glorious talent.’ For a moment the marching music drowns out the announcement, then, ‘By way of a bonus I am pleased to be able to tell you that Mijnheer Veringa has kindly agreed to open the festivities with a special address.’

By eight o’clock our street is jam-packed with children and their parents. There can be nobody left inside. From our balcony I can see men in blue uniforms — ‘Look, the Resistance,’ says my mother admiringly — small groups of girls walking arm in arm, lots of families from other streets, and, among the crowd, knots of soldiers drawing nonchalantly on their cigarettes and hanging about looking expectant.

‘Come on,’ says my mother, ‘it’s time we went downstairs. You must want to join the fun,’

I make a face.

‘There’s so much going on, surely you don’t want to miss anything? You don’t have to join in if you don’t want to, you can just stand there and watch.’

When we come out of the front door the crowd is thronging towards the other end of the street, the children running excitedly through the crush, shouting and pushing people out of the way, laughing exuberantly. I fall silent, my spirits dampened by their boisterousness, and as we join the motley stream of people I clutch my mother’s arm, longing to be back home. The street is like an empty swimming-pool with pennants hanging from long strings fluttering overhead in the wind. I walk along the bottom next to the sall sides and look for a way out.

At the other end of our street, close to the school, an apartment window on the first floor has been flung wide open, the windowsill draped with a flag and a few sad-looking daffodils with broken heads fastened to the woodwork.

Everyone is gathered under the window and looking up eagerly. The loudspeaker music stops with a loud click in the middle of a tune and the hubbub of voices in the street grows softer and dies away.

A man appears in the window, wearing a bow tie and with an orange ribbon pinned to his coat. His plump face is set and solemn.

‘Who is that man?’ I whisper to my mother.

How can anyone be so fat after the war? He must be someone very important.

‘Ssh, I don’t know either.’ She tugs at my arm and I shut up. The gentleman surveys the crowd with a haughty and satisfied expression. He is standing in front of a microphone which he taps a few times, making the loudspeakers resound with a series of drones and crackles.

‘Fellow countrymen,’ he cries. ‘Neighbours. Friends.’

Suddenly there is a piercing whistle, a high screeching sound that makes some people clap their hands to their ears. And there is suppressed laughter as well, which shocks me: how can they stand there and snigger so disrespectfully, what is there to laugh about anyway?

A few arms reach from out of nowhere to snatch the microphone away and there is more grating and booming. Now my mother is laughing too...

‘Fellow countrymen. Neighbours. Friends. The enemy has been beaten, our nation has been liberated from the German yoke and our liberators’ — here he makes a sweeping gesture over the street — ‘are amongst us. Before very long our beloved queen and all her family will be joining us...’

He is holding a sheet of paper which, as I can see clearly even from below, is shaking in his hands as if caught in a stiff gust of wind. The flood of words roll over me, sounds whose meaning escapes me. When he folds up his paper there is a deathly hush, and then I hear the strains of a familiar tune.

‘Whose veins are filled with Ne’erlands blood...’ The man stands stiff as a ramrod in the window-frame, his stomach sticking out. Around me I hear more and more voices joining in the singing, hesitantly at first, then louder and louder.

‘Why isn’t he singing himself, Mum?’

The mood is subdued and solemn now and some people are dabbing their eyes. The song is over and the silence is unbroken. My mother looks at me from out of the corners of her eyes, nods at me encouragingly, then takes my hand and squeezes it hard.

A startled dog runs barking through the crowd, scattering a cluster of shrieking girls; a moment later I hear a loud high-pitched whine as if someone has given the dog a kick.

‘Our neighbour Marietje Scholten will now oblige us with Schubert’s *Ave Maria*. Her father will accompany her on the piano.’

The gentleman bows, making way for the milkman’s daughter who takes his place at the microphone. Her cheeks are scarlet as she looks straight across the street with staring doll’s eyes, as if she does not see any of us and is standing up there against her will, about to burst into tears.

‘Are you ready?’ she asks, looking behind her into the room. When her question comes loud and clear over the loudspeakers, she gives an apologetic laugh and places her hands over the microphone. The crowd claps sympathetically as a few hesitant, searching chords become audible.

‘I want to leave now,’ I whisper, ‘can we go?’

The thronging crowd is stifling me, and I have the feeling that the girl will never get her song started; she has brought the microphone close to her petrified face, clutched in both hands, and still nothing is happening.

Then, all at once, she sings. I stand rooted to the spot: the sound of her voice and the music she sings echo deep within me, as if a stone is being turned over inside me. An ecstatic feeling of happiness and abandon soars up in me, growing all the time. My chest feels tight and I pull my hand away from my mother’s. I am being torn apart.

The people stand side by side listening in total silence, neighbours, soldiers, children, nobody moving. We have forgotten about the shrillness of the microphone, we are held captive by that voice, soaring over our heads into the night sky.

The applause ebbs away. Marietje bows awkwardly and knocks the microphone over.

‘What’s the matter?’ my mother asks, but I turn my face away from her furiously. ‘Darling,’ she says, ‘don’t be like that.’

I watch the rest of the celebrations from our balcony, the people standing in place by the white lines down the whole length of the street, craning their necks and jostling one another. Where the lines end, and someone has chalked the word FINISH in large letters, other people are crowded around the table with the prizes: a bag of flour, a few bars of chocolate, a packet of tea and some small bags of milk and egg powder.

Our next-door neighbour, apparently in charge of the competition, shouts, ‘Attention!’ and blows a whistle.

To the accompaniment of encouraging yells the competitors stumble, hopping and falling about between the white lines, their legs stuck inside jute sacks.

My mother, too, has joined in this grotesque parade. I don’t know whether to be proud or ashamed of her, and am in two minds about cheering her on. The bystanders call out names and urge the competitors on, and my father, standing beside me, whistles shrilly through his fingers.

‘Keep going, Dien, don’t wobble!’ but by then she has stopped and fallen into the arms of some bystanders, helpless with laughter.

‘You ought to have joined in as well,’ says my father, ‘why didn’t you, lazy-bones? Such fun and you could have won a bar of chocolate.’

I don’t answer and watch my mother wriggle out of the jute sack still screaming with laughter. When a voice announces that the winners’ names are about to be called I try to slip inside unnoticed.

‘Hang on a minute, Jeroen, you want to hear who’s won, don’t you?’

What difference can it make to me, you can all do what you want, behave like little children if it pleases you, but leave me in peace.

‘Do you hear that? Jan has come third!’

But I am looking at a couple of soldiers who are disappearing over the grass bank along the canal, followed by two girls. They saunter to the edge of the water and sit down, so that I can see little more than their heads. One soldier puts his arm around a girl and they slide backwards into the grass, disappearing from sight.

The water in the canal is black and still, a few pieces of weed floating in it and a duck balancing on a rusty bicycle wheel that sticks above the surface. A glimmer of light from the gardener’s house on the other bank makes an oily reflection across the water.

I walk into my dim little room where the hubbub from the street sounds unreal. The house is empty, cavernous.

I bite my pillow and try not to think. I fold my hands between my tightly-squeezed legs and curl up small.

Please God, make him come back, make him find me. If You let him come back I’ll do whatever You want.

I am bicycling to Warns. I have no clothes on. Walt is sitting on the luggage carrier with his arms around me, stroking my belly.

I can see people everywhere, behind windows, in gardens, on the pavements and ducking around corners. I pedal like one possessed but the bicycle hardly moves. I sweat under the firing squad of prying looks. I gasp and struggle on. Mem stands in front of the house, crying and beckoning me with her dependable, stout arms. I can’t reach her. I pedal and pedal.

*‘Move,’* Walt whispers, *‘move, Go on, faster.’*

His fingers caress my stomach and I feel ashamed, everyone watching us. I want him to stop, I want to cry and shout at him but I have lost my voice.

He puts me up against a wall and points his gun at me, one of his eyes is screwed half-shut and looks cold and inscrutable.

Don’t let me fall, I think, if I fall I’ll drop in the mud.

But my feet are already slipping...

<br>

I am standing in the passage and there are my parents, in their pyjamas, giving me surprised and anxious looks.

‘What are you doing out here, why aren’t you asleep? Do you have to go to the lavatory? Jeroen, can you hear us?’

My mother leads me back to my bed, I let her tuck me in, her hands soft and familiar.

‘Did you have a bad dream? You must tell us if there’s something frightening you.’

My father stands in the doorway with a sombre face. I can see him vaguely against the light in the passage.

‘You could have won, you should have carried on. Why did you stop?’ I say to her face bending over me.

‘Whatever’s the matter with him, he sounds delirious,’ I hear my father whisper. Why did he carry on like that about Jan on the balcony tonight, I wonder, was that just to tease me? Doesn’t he think very much of me just like Jan’s mother? ‘He’s a real boy...’

‘I’ll leave the light on, maybe that’ll make him feel better.’

Their footsteps go creaking back to their own bedroom.

<br>


##  3

I SET OUT on a series of reconnoitering expeditions through Amsterdam, tours of exploration that will take me to every corner. On a small map I look up the most important streets to see how I can best fan out criss-cross the town, then make plans on pieces of paper showing exactly how the streets on each of my expeditions join up and what they are called. To make doubly sure I also use abbreviations: H.W. for Hoofdweg, H.S. for Haarlemmerstraat. The pieces of paper are carefully stored away inside the dust-jacket of a book, but I am satisfied that even if somebody found the the notes, they wouldn’t be able to make head or tail of them. It is a well-hidden secret.

For my first expedition I get up in good time. I yawn a great deal and act as cheerfully as I can to disguise the paralysing uncertainty that is governing my every move. 

‘We’re going straight to the field, Mum, we’re going to build a hut,’ but she is very busy and scarcely listens.

‘Take care and don’t be back too late.’

The street smells fresh as if the air has been scrubbed with soap. I feel dizzy with excitement and as soon as I have rounded the corner I start to run towards the bridge. Now it’s beginning, and everything is sure to be all right, all my waiting and searching is about to come to an end; the solution lies hidden over there, somewhere in the clear light filling the streets.

The bright air I inhale makes me feel that I am about to burst. I want to sing, shout, cheer myself hoarse.

I have marked my piece of paper, among a tangle of crossing and twisting lines, with H.W., O.T., W.S.: Hoofdweg, Overtoom, Weteringschans.

The Hoofdweg is close by, just over the bridge. It is the broad street we have to cross when we go to the swimming-baths. I know the gloomy houses and the narrow, flowerless gardens from the many times I’ve walked by in other summers, towel and swimming trunks rolled under my arm. But beyond that, and past Mercatorplein, Amsterdam is unknown territory to me, ominous virgin land.

The unfamiliar streets make me hesitate, my excitement seeps away and suddenly I feel unsure and tired. The town bewilders me: shops with queues outside, people on bicycles carrying bags, beflagged streets in the early morning sun, squares where wooden platforms have been put up for neighbourhood celebrations, whole districts with music pouring out of loudspeakers all day. An unsolvable jigsaw puzzle. Now and then I stop in sheer desperation, study my hopelessly inadequate piece of paper, and wonder if it would not be much better to give up the attempt altogether.

But whenever I see an army vehicle, or catch a glimpse of a uniform, I revive and walk a little faster, sometimes trotting after a moving car in the hope that it will come to a stop and he will jump out.

Time after time I lose my way and have to walk back quite far, and sometimes, if I can summon up enough courage, I ask for directions.

‘Please, Mevrouw, could you tell me how to get to the Overtoom?’

‘Dear me, child, you’re going the wrong way. Over there, right at the end, turn left, that’ll take you straight there.’

The Overtoom, when I finally reach it, seems to be a street without beginning or end. I walk, stop, cross the road, search: not a trace of W.S. Does my plan bear any resemblance to the real thing?

I take off my shoes and look at the dark impression of my sweaty foot on the pavement. Do I have to go on, search any more? What time is it, how long have I been walking the streets?

> Off we sail to Overtoom,
> We drink milk and cream at home,
> Milk and cream with apple pie,
> Little children must not lie.

Over and over again, automatically, the jingle runs through my mind, driving me mad.

As I walk back home, slowly, keeping to the shady side of the street as much as I can, I think of the other expeditions hidden away in the dust-jacket of my book. The routes I picked out and wrote down with so much eagerness and trust seem pointless and unworkable now. I scold myself: I must not give up, only a coward would do that. Walt is waiting for me, he has no one, and he’ll be so happy to see me again.

At home I sit down in a chair by the window, too tired to talk, and when I do give an answer to my mother my voice sounds thing and weak, as if it were finding it difficult to escape from my chest. She sits down next to me on the arm of the chair, lifts my chin up and asks where we have been playing such tiring games, she hasn’t seen me down in the street all morning, though the other boys were there.

‘Were you really out in the field?’

‘Ask them if you don’t believe me!’ I run onto the balcony, tear my first route map up into pieces and watch the shreds fluttering down into the garden like snowflakes.

<br>

When my father gets back home he says, ‘So, my boy, you and I had best go into town straightway, you still haven’t seen the illuminations.’

With me on the back, he cycles as far as the Concertgebouw, where he leans the bike against a wall and walks with me past a large green space with badly worn grass. Here, too, there are soldiers, tents, trucks. Why don’t I look this time, why do I go and walk on the other side of my father and cling — ‘Don’t hang on so tight!’ — to his arm?

‘Now, you see something,’ he says, ‘something you’ve never even dreamed of, just you wait and see.’

Walt moving his quivering leg to and fro, his warm, yielding skin, the smell of the thick hair in his armpits...

I trudge along beside my father, my soles burning, too tired to look at anything.

We walk through the gateway of a large building, a sluice that echoes tot hes ound of voices, and through which the people have to squeeze before fanning out again on the other side. There are hundreds of them now, all moving in the same direction towards a buzzing hive of activity, a surging mass of bodies.

There is a sweet smell of food coming from a small tent in the middle of the street in front of which people are crowding so thickly that I can’t see what is being sold.

I stop in my tracks, suddenly dying for food, dying just to stay where I am and to yield myself up to that wonderful sweet smell. But my father has already walked on and I have to wriggle through the crowds to catch up with him.

Beside a bridge he pushes me forward between the packed bodies so that I can see the canal, a long stretch of softly shimmering water bordered by overhanging trees. At one end brilliantly twinkling arches of light have been suspended that blaze in the darkness and are reflected in the still water. Speechless and enchanted I stare at the crystal-clear world full of dotted lines, a vision of luminous radiation that traces a winking and sparkling route leading from bridge to bridge, from arch to arch, from me to my lost soldier.

I grip my father’s hand. ‘Come on,’ I say, ‘let’s have a look. Come on!’

Festoons of light bulbs are hanging wherever we go, like stars stretched across the water, and the people walk past them in silent, admiring rows. The banks of the canal feel as cosy as candle-lit sitting-rooms.

‘Well?’ my father breaks the spell. ‘It’s quite something, isn’t it? In Friesland, you’d never have dreamed that anything like that existed, would you now?’

We take a short cut through dark narrow streets. I can hear dull cracks, sounds that come as a surprise in the dark, as if a sniper were firing at us.

My father starts to run.

‘Hurry, or we’ll be too late.’

An explosion of light spurts up against the black horizon and whirls apart, pink and pale green fountains of confetti that shower down over a brilliant sign standing etched in the sky.

And other shower of stars rains down to the sound of muffled explosions and cheers from the crowd, the sky trembling with the shattering of triumphal arches.

I look at the luminous sign in the sky as if it is a mirage.

‘Daddy, that letter, what’s it for? Why is it there?’ Why did I have to ask, why didn’t I just add my own letters, fulfil my own wishful thinking?

‘That W? You know what that’s for. The W, the W’s for Queen Wilhelmina...’

I can hear a scornful note in his voice as if he is mocking me.

‘Willy here, Willy there,’ he says, ‘but the whole crew took off the England and left us properly in the lurch.’

I’m not listening. I don’t want to hear what he has to say.

W isn’t Wilhelmina: it stands for Walt! It’s a sign specially for me...

The army has arranged a dance for the neighbourhood. The playground is brightly lit up with floodlights and is chock-full of people, pressed close together up against the fence and listening to a small band seated on the steps of the gym.

I hear the fast rhythm, the seductive slithering tones, the trumpet raising its strident voice, sounds you cannot escape.

‘Off you go,’ says my father. ‘Liberation happens just once in a lifetime. Mummy and I will be coming to have a look too.’

There is frenzied dancing on an open piece of ground ringed by curious spectators. Most of the men are soldiers.

They hold the women and girls tight, then suddenly thrust them away fast, turn round, catch them on one arm and bend over them with practised ease.

The girls move about on agile, eager legs, turning flashily in time with the music and moulding themselves compliantly to the masterful bodies of their partners.

The few older residents who are dancing do so sedately, moving around carefully with dainty, measured steps, smiling about them politely.

‘Swing’s great, hey?’ Jan is leaning over my shoulder, his mouth close to my ear. ‘See how tight a grip they’ve got on those bits of skirt?’

His body moves in time with the music, and he hums along with delight.

We look at the couples clamped together, the quick bare legs under the short skirts, the firm grip of the soldiers’ arms, the heavy boots moving effortlessly like black, hot-blooded beasts. The heat given off by the dancers transmits itself to us, rousing us.

‘Christ, take a look at that, will you? That sort of thing oughtn’t to be allowed. Look, quick!’ I feel a push and am propelled forward. ‘See her? You keep your eye on her, it won’t be long before she’s walking around with a big belly.’

I admire them, I think they’re exciting; I want to go on looking at them for a long time, the way the two of them dance, the way he puts his arm around her and looks at her.

‘Come on,’ says Jan, ‘let’s go and have a smoke, I know somebody with a packet of Players.’

The soldier spins around like greased lightning, kicking his heels so high that they tap his bottom. He presses the girl hard up against him, his hands on her buttocks as she hangs meekly on to him. They whirl about between the other dancers, disappearing from view and turning up suddenly somewhere else. Whenever he finds enough space the soldier throws the girl up in the air, swings her between his legs as she comes down, sleekly rotates his quick hips and then takes a few long steps, one of his legs thrust out between hers.

In a corner of the playground some boys are sitting on their haunches against the fence, and when Jan joins them I squat down as well. They are smoking with self-important expressions and an appearance of doubtful enjoyment. When the military band strikes up a new tune they snigger and sing along softly so that the bystanders cannot hear them:

>*‘No can do, nooo can do,*
>*My momma and my poppa sez*
>*Nookie do...’*

They fall about laughing and one of the boys holds the burning cigarette high up between his legs. I look sideways at Jan. Don’t you start laughing now, I plead with him silently, don’t leave me in the lurch, please don’t laugh.

He gives me a broad grin and holds out the cigarette to me with a friendly gesture. I grin back weakly.

Slowly I climb up our dark stairs. Players, nookie do, the shocking dancing, what a strange world these soldiers live in. None of it has any connection with Walt, even though he held me between his bare legs and pushed his thing into my mouth.

Above me a door is being opened and my mother, who has heard me coming, calls out, ‘You don’t have to walk up in the dark any longer, the electricity’s on again!’

With a click the little light on the stairs comes on.

>‘Happy birthday to you,
>Happy birthday to you...’

My mother is singing, and at the word ‘you’ she points Bobbie’s little arm in my direction.

A small present is lying beside my plate, wrapped in thick brown paper. When I open it, I find four exercise books, a pencil sharpener and a small rubber in two colours.

‘The black bit is for rubbing out ink,’ my father explains, ‘the red side is for pencil.’

‘We bought you the exercise books for school, make sure you keep them away from Bobbie. Do you like them?’

I nod. I feel no excitement at all about the present, only vague disappointment and indifference.

The rubber has already disappeared into my brother’s mouth. He is chewing on it and dribbling long threads onto the tray of his high-chair.

‘Tut, tut, tut,’ says my mother, ‘you little piggy, that isn’t a toy for little boys like you. Any moment now, he’ll swallow it.’ She puts the small wet object back on the exercise books. ‘That’s your big brother’s, he’s growing into a smart young man.’

‘Twelve years old today, your first birthday for five years without a war going on! That’s just about the best present of all, isn’t it? Everything’s going to change for the better now.’

My father gives me a serious look. ‘You’re going to have a lovely time from now on, son.’

The sun is slanting into the room and falls across one end of the check tablecloth. I catch the light on the blade of my knife and bounce it back on to the wall. A speck of light jumps across the wallpaper with every movement I make.

‘Twelve is quite an age, you’ll have to do your very best at school now that you’re a big boy.’

I bed the little rubber between my fingers until the ends touch. Why do I have to get bigger? I’d rather stay as I am, if I change Walt won’t recognise me.

‘Ask some friends up to play this afternoon, why don’t you,’ my mother says, ‘I’ll get them something nice to eat.’

The rubber snaps at the join of the two colours. Before they can see anything I hide the two halves in my pocket.

<br>

I continue my expeditions. One day I walk through Haarlemmerweg to Central Station, the next day over the Weteringchans to Plantage Middenlaan, and the morning after that across the sands to the Ring Dyke. But I break off that last expedition quickly because everywhere I go in that long overgrown stretch of land with its weeds and tall grasses I come across people lying in the hollows along the dyke, alone or in pairs. There is an inevitable exchange of silent looks, or signs of a rapid change of position, so that, despite the rumours that there is an army camp somewhere along the dyke, I turn around and return to the busy streets.

The morning I pick up the piece of paper with the letters V.B.S. — C.B. on it, I hear my father, who is listening to the radio, suddenly curse softly ‘God help us all...’ I try to catch the announcer’s words. He is saying something about ‘Japan’, ‘American air force’, ‘an unknown number of victims’, but the meaning of it all escapes me. My father is listening attentively. I ask him no questions.

The expeditions into town have begun to exhaust me and I continue them without any real faith. My conviction that I shall find Walt again has worn thin; sometimes I have to jolt my memory to remind myself why I am in the part of town where I happen to be and for what purpose. I walk, I trudge along, I sit on benches, stare at passers-by, look into shops, long to be back home, anything so as not to think about him.

On the Ceintuurbaan, I recognise the cinema I went to with my mother a few years back, when the war was still going on.

>Dearest children in this hall,
>Let us sing then, one and all:
>Tom Puss and Ollie B. Bommel!

I had sung along obediently but my illusions had been shattered. Ollie B. Bommel, I told my mother, was much too thin, his brown suit hanging like a loose skin about his body. And Tom Puss (‘Tom Puss, Tom Puss, what a darling you are’ — but by then I had stopped singing along) had turned out to be a little creature with a woman’s voice prancing coquettishly about the stage.

I stop on a bridge to watch the fully-laden boats moving towards me down the broad channel and then disappearing beneath me. A boy comes and stands near me and spits into the water. He keeps edging closer to me. When he is right next to me, he says, ‘Want to come and get something to drink with me? There’s nothing much doing round here.’

I am afraid to say no, follow him towards an ice-cream parlour on the other side of the bridge and answer his questions: don’t I have to go to school yet, do I live around here and when do I have to be back home? He is wearing threadbare gym shoes and his hair has been clipped like a soldier’s, short and bristly.

I am flattered that an older boy is bothering with me and paying me so much attention, but the sound of his voice and also the way he keeps scratching the back of his neck as he asks me questions put me off. When I realise that his arm is touching my shoulder I quickly move a bit further away from him.

‘What do you want, an ice-cream or something to drink?’ he asks when we are inside the small ice-cream parlour. ‘I can ask if they have lemonade.’ He looks at my legs which I have stuck awkwardly under my chair, smiles at me conspiratorially and whistles softly through his teeth.

When he is standing at the counter I race out of the shop and run on to the next bridge. I tram labouring up the incline just misses me, clusters of people hanging from the footboards. When the second carriage draws level I run beside it and grab a held-out hand. I balance on the extreme edge of the footboard and clutch the handle, dizzy with fear; I am going to fall, I’ll be lying in the street, Mummy won’t know where I am... My hands hurt and I am afraid they will slip off. Houses pass by at breakneck speed, the man next to me leans more and more heavily against me, but after a little while it begins to get exciting, there is a friendly atmosphere among that tangle of bodies, people joke with each other, shove up obligingly and call out to passers-by. When I finally dare to look out sideways I can see where we are: Kinkerstraat, that was quick and it didn’t cost anything either!

They have organised a children’s party by the canal near our house. I can hear the singing in the distance already. My little neighbours are lined up in long rows at the edge of the pavement, dancing and cheering at the command of a determined young woman. I go and sit on a low wall until a girl runs up to me and puts out her hand.

At first, surly and shamefaced, I refuse to take it, but the woman beckons. ‘Everyone has to join in, this is a party for everyone, no exceptions allowed!’

I join the big circle and see Jan and the boy with the cigarette holding hands with the milkman’s daughter. We hop about singing, changing places, passing from hand to sweaty hand, skip, bow deeply and clap in time with the music.

Mothers are standing on the balconies, looking down on their singing children, mine too, in her yellow dress, my brother on her arm. I wave.

>‘Green the grass, green the grass,
>Green beneath my feet,
>My best friend now has gone away,
>Shall we meet again one day?
>All of you must stand aside
>For that maid so sweet and bright...’

We are twirling about, inside, outside, first to the left, then to the right. I feel the touch of hands, sweep past hot, happy faces, catch girls and boys around their waists and skip about in a circle with them.

In the evening my father sits at the table with a grim expression reading the paper. When he unfolds it I can see the front page: *The Truth*, it says, and a little further down: ‘Atom Bomb on Hiroshima. Catastrophic results of U.S. Air Force Raid on Japan.’ I walk into the kitchen. ‘What’s that, Mum, an atom bomb?’

‘Ah, darling,’ she says, ‘I don’t rightly know myself. But it’s nothing for children to bother themselves with. Best not think about it, you’re too small for things like that.’

I have tied my arm to the edge of the bed so with a piece of ribbon so that I can’t get up and sleep-walk around the house and more. A gentle breeze billows the curtain, wafting in scents from the garden. I can hear a monotone voice on the radio in another house sending news through the still summer night: ‘According to the latest reports the entire city has been laid waste. Estimates put the number of dead at tens of thousands. A second raid by the U.S. Air Force...’

What if he is over there, what if he is having to fight in Japan? Where is that, how would I get there? He is lying in his tent with bombs exploding all round him...

The voice on the radio is still reading the news, but I have stopped trying to make out the words.

He is washing at the basin. His strong, supple body, the water running down his legs to the floor. He pulls me towards him and reassures me.

The expedition to the Ceintuubaan is my last.

<br>

##  4

‘YOUR NEW SCHOOL is near the centre, quite a long way from home,’ says my father. ‘If it’s too far I can give you the tram fare now and then.’

He is shaving in the kitchen, his head held stiffly backwards and his eyes fixed on his image in the mirror. His arm moves steadily, surely and unhesitatingly as he strokes the finely-honed cut-throat razor up and down. If he had any idea how much walking I had done this summer, my criss-crossing of the city, my certainty that sooner or later I would rediscover those eyes, that smell, that breath. The razor scrapes across his face collecting thick blobs of soap as the naked skin over his jaw is laid bare strip by strip.

‘It won’t be too bad,’ I tell him, ‘the school in Friesland was a long way, too, probably a bit further.’

It’s still early. I eat my sandwich and listen to the rustling of the trees in the garden. My little brother starts to bawl in the living-room, his screams disturbing our relaxed mood. A moment later my mother comes into the kitchen looking harassed, holding out a plate with a sandwich cut into four cubes.

‘He’s driving me mad, he won’t eat a bite. I can’t get anything down him.’ I look at her desperate and tired face. She runs her hands nervously over her temples and puts a kettle on the gas stove.

‘Don’t always let it get you down so.’ My father’s voice is distorted by the strange way he has to twist his head round. ‘ He’ll eat soon enough when he gets hungry. Just leave him be.’

I take a deep breath and think of my new school. The smell of the shaving-soap is clean and pleasant. The small voice in the living-room continues to scream furiously and I think of my mother when she sat in the meadow, hidden deep among the buttercups and plumes of grass, relishing the peace and quiet.

I leave home half an hour after my father, my satchel heavy with all the new books: *First year Algebra*, *New Dutch Writing* and *English for Beginners*.

I am wearing shoes that have been polished so thoroughly that they look brand-new, but they are too small for me and hurt. The plus-fours have been brought out again and my mother has knotted a shiny tie over the shirt with the little pocket.

‘Just for the first day,’ she had said, ‘so you can make a good impression.’

When I had seen myself in the mirror I had looked away quickly. Was that me? The sheltered days of the White School were gone for good. For a moment it seemed that the heavy satchel and the uncomfortable shoes were going to make me lose my balance on the stairs. I had clutched the banister and carried on downwards, stepping cautiously.

Now, at the corner of the street, I glance back. My mother waves to me with a girlish gesture from behind bright red geraniums. I can see how proud she is of me, her son who is now going to attend Class 6A, and after that presumably the High School. When I have rounded the corner I am tempted to go back to see if she is still standing there, to etch the sight indelibly on my memory.

Everywhere there are cyclists going to work and small groups of children sporting satchels and walking in various directions through the streets. It feels chilly, and on Admiraal de Ruyterweg I take the sunny side of the street, changing the satchel constantly from one hand to the other. As I cross the bridge in De Clerqstraat I see a car, an army car, coming slowly in my direction along the embankment. It stops at every side street as if the driver isn’t quite sure where he is going. I stand still.

Have I got time? Can I hang on here?

Quickly I cross the bridge and, leaning far out over the water, stare intently at the car as it creeps closer like a whirring beetle. The sun is dazzling on the windscreen so that I can hardly make out the man behind the wheel clearly, but the elbow hanging out of the window looks familiar and it seems to me that the moving smudge that is his head is sandy-blond and close-cropped.

Blood rushes to my head: it’s him, it’s Walt! I can see everything clearly now: the white tee-shirt with a strong, sun-burnt arm in the short sleeve, the watch, and the car too, they are all the same. I am paralysed; what now? The car stops and some children come scampering past. What time is it? The car is moving again. He’ll be here any second...

I look at my shoes sticking through the bars of the bridge, stiff and shiny. And at the plus-fours. I remember my image in the mirror and feel ashamed, Walt will find the knickerbockers odd — he has only ever seen me in old clogs and a threadbare pair of shorts. I quickly pull off the tie, crumple it into my pocket and undo the top buttons of my shirt. I would dearly love to run back home and put on my shorts, so as to look the way I used to.

I walk off the bridge and go and stand on the edge of the pavement. The car is just a block away now, what should I do, run towards it? I start to run but quickly turn round again: he might easily drive past me, I had better stay where I am on the corner. People bump into me and a cyclist swears in my direction.

He’s coming, really close, there, he’s coming. I swallow, put my satchel down and pick it up again. Thank You, God, for helping me. All that praying in bed was not for nothing.

The sound of the horn, for me? Has he seen me already? What shall I do if he asks me to get in, say yes?

I can hear the purr of the engine close to me, and a dark shape edges into my field of vision. Of course I’ll go along with him, miss the first day of school, I’ll think later about what to tell them at home.

A soldier with a small moustache looks vacantly at me out of the window for a moment, a match wiggling between his teeth. He casts a bored look down De Clerqstraat, then shrugs a shoulder. The engine speeds up audibly, there is a penetrating stench of exhaust fumes and I am left standing in the bright light again. The tram rails shine in the sun, a twisting ribbon up the road.

I run all the way to school.

<br>

In front of the school there is a large sandpit surrounded by a high fence made up of metal spikes and scrolls. Inside, some children are playing with buckets and spades and little wheelbarrows under the watchful eyes of their patiently seated mothers.

Between the fence and the canal a narrow path beside the sandpit leads to the school. I run down it as fast as I can so as to get there before they shut the doors. My shadow speeds along next to me, deformed by the hills and holes dug in the sand and bisected by the iron bars of the fence.

I join up with the assembled group of children, allow myself to be borne inside along with the neatly ironed dresses and freshly laundered shirts, listen to the headmaster’s address, give my name, am assigned to a class, attach myself to a group, walk up a wide flight of stone stairs, go into a classroom and sit where shown at a desk next to another boy.

‘Hullo.’

‘Hello.’

‘Good morning, boys and girls... books on the desk... by tomorrow they must all be neatly covered.... preparation from playing your part in society... discipline... hard work...’

I listen to the flood of words, then obediently bring out a book, open it and look at the figures, signs and formulas. I feel dizzy and slow-witted and bend feebly over the letters: A is to C as B is to A.

‘That boy over there, yes, you.’ I quickly put on a knowing, intelligent expression, but blush hopelessly ‘You look as if you’d seen a cow in a pair of knickers flying over the chimney. Didn’t they teach you to count beyond ten at your last school, you ninny?’

The class sniggers politely.

The words and the tone in which they are said cut right through me, I have never been rebuked with such scorn before. My throat tightens and I start to splutter and cough. The boy beside me snatches his books away. Will I have to put up with this sort of thing for a whole year? I remain in a state of confusion until an older lady comes in to relieve the first teacher.

‘The English language, boys and girls,’ she says, ‘is one all of us can of course use to good effect these days. I take it that some of you are familiar with quite a few English expressions. Is there anybody, for instance, who hasn’t yet spoken to one of our liberators?’ A few arms go up, all of them girls’. Before I know what I am doing I have raised my hand as well.

She starts reading outs sentences from *English for Beginners* and tells us to say them aloud as she writes the words up on the blackboard. I look at her face, the curly grey hair, the calm grey eyes above the dark blue jumper.

*‘My name is Mister Brown. This is my house and that is my wife.’*

She pronounces the words with exaggerated emphasis, pausing between each one. With droning, sing-song voices we repeat them after her, sentence after sentence. The sounds I make become lost in the general chorus.

*‘My name is Mister Brown. This is my house and that is my wife... Say it: Walt... I work in the city... Is okay, no problem... She works in the kitchen... Hold it, yes, move. Go on... Time is going fast. In the evening I come home... Faster, don’t stop, come on... My wife holds my hat... We sit at the table... Oh, is good, is good... We drink tea with a biscuit... Baby, I love you. Come on, smile...’*

<br>

They all rushed out in front of me and I let them pass. I waited until the corridor was empty and only then did I leave. 

I walk past the iron fence. The spikes stab the air and mark out the path. The shadows over the sand have gone and most of the playing children are back home.

The sky is overcast, suddenly the city looks drab and inaccessible. I stop for a moment at the De Cerqstraat crossroads. In the distance I can see my classmates disappearing, all hurrying home as fast they can.

I hesitate. What am I going to do?

A tram goes by, boys from my class are hanging on and leaning out from the footboard, yelling and swinging their satchels daringly. I watch the tram undulating over the humps of the bridge and on down the empty street.

*My name is Mister Brown. This is my house*... I feel a few drops of rain and button up my shirt. Carry on walking, now.

...*and that is my wife*...

\pagebreak
\pagebreak

I PUT ALL the papers scattered over the table to one side, and push the plate I have filled in the kitchen into the space I have made. Mustn’t make a mess.

Leaning on one elbow I read some more in the book I happened to pick up yesterday. But I am disappointed, I cannot recapture the impression it made on me years ago. Even so, I read on in the hope that the sense of excitement and admiration I felt at the time might return.

I pour out a little stale wine and swallow my food mechanically. ‘Don’t eat with your chin in your hands all the time,’ I can still hear her say. Time after time I had bent over a book in the middle of lunch, cramming bread into my mouth absent-mindedly. When my mother finally snatched the book away I would rebelliously turn the jampot or the box of sugared caraway seeds round and round to read and re-read the labels.

After five minutes, or a quarter of an hour — how much time has gone by? — I notice that I have read two pages without taking in anything   at all, just a blur of isolated words without meaning or connection. I push my plate away. Almost six o’clock. There is to be a talk on the local radio to which I want to listen. I turn the small set on and re-read a few pages of the book. The announcer says that tomorrow it will be thirty-five years since the Allies liberated the Netherlands.

‘At the invitation of Amsterdam City Council a group of Canadian veterans are visiting the capital. A few hours ago they were welcomed by a small but enthusiastic reception committee at Schiphol airport. Tonight they will be attending a dinner in the Royal Palace on the Dam, where tomorrow there will be a wreath-laying ceremony in the presence of Her Majesty Queen Beatrix.’

I close the book, turn the radio off and go to the window. Across the street I can see a mountain of refuse bags. A woman is neurotically washing every speck from her car parked outside my door. Every so often she inspects the result from a distance and then flings herself back tight-lipped at the same spot.

A flag is hanging outside the hotel. I try to remember if it is there all the time or has been put out specially for the occasion.

Canadians, liberators, Her Majesty, the Royal Palace on the Dam, how often have I heard these words before without the slightest reaction? It is like the book, I read it and nothing in it gets through to me.

For the first two years after the war I commemorated the liberation frenziedly, every single day. Obsessed, like a maniac. Prayer sessions, offerings, supplications, solemn promises — all in the privacy of my small room. For two years I tried to remember features, smells, excitement, fear. Then it all faded away, a slow erosion that went hand in hand with bouts of self-reproach and dejected, silent withdrawal into myself.

‘It’s puberty, all boys that age act like that,’ I heard a relative say to my parents.

I went to school and learnt thing, by rote, then found my vocation and became completely immersed in it. Or had I deliberately buried myself in it?

I made my choice and set to with a vengeance, I tell myself testily.

Until eight o’clock I carry on reading the book that leaves me so cold, then I switch on the television: I want to see those Canadians.

A bus crash, a summit conference. Then: Schiphol, a small group of people crowded in front of a glass partition. The mayor. I get up because the telephone rings.

‘Hello, Govert. No, give me a ring a bit later, I’m busy with something right now.’

By the time I am back sitting down the Canadians are driving through the city on a tank. I see a group of greying men in uniform riding over the Rokin, smiling broadly. The heroes of yesterday. Their ungainly stoutness is emphasised by their uniforms. They have pinned decorations to their jackets. I catch myself looking at them compassionately. They have friendly, blank expressions, ordinary men of the kind you might see on a tram, or in a doctor’s waiting room. Ageing men in their sixties.

Extracts from old newsreels are shown. Pictures I have often seen: soldiers ploughing through the surf, crawling up beaches. The tumult of the sea and the explosions. Boats that are only just under control. I see trenches filled with maimed or dismembered bodies, the black fury of young, driven faces. Then the endless field of white crosses. The madness to which we fall prey time after time after time. What is wrong with us?

When I think back to those days I see myself sitting beside a lean, young soldier in an army car, accepting chewing-gum from a firm hand and inhaling the metallic smell of uncertainty during embraces and contacts I did not want and yet longed for like one possessed. The screen is showing the reception outside the Palace. I kneel on the floor and go and sit right in front of the set to get a closer view of the soldiers. The wrinkles, the bags under their eyes, the folds bulging over the collars of their stiff uniforms.

They aren’t completely bald yet, I think. Why am I dissecting them so mercilessly? Why do I feel so detached from something that swept me off my feet years ago into a state of such excitement?

The army band strikes up some patriotic songs. There they stand, the lost soldiers. A few final flamboyant chords and it is all over. The small crowd disperses, and the veterans disappear into a waiting bus.

A pigeon is cooing outside and picking buds off the hawthorn. Whenever I think of the soldier I still see him as a boy and myself as a child. We have remained as we were, the time in between does not exist. He has nothing to do with the group of men I have just seen leave on the bus.

I sit beside my mother in the boat and watch Friesland disappear and Amsterdam come into view on the other side. I hang over the railing with Jan and look at the eddying water.  A warm, carefree day it was, with blue skies and mothers in summer dresses. We had a calm and trouble-free crossing from Friesland to Amsterdam. A detached moment in my life.

I go upstairs and lie down on my bed. The ceiling is a large, pale smudge. Outside, a tram stops with a clang and pulls away again. Summer evening noises, the curtain gently billowing in front of the open window. The soldier leans across me and tucks a letter away in the side pocket of the tent. The naked underside of his body curving upwards, the voluptuous shape, the triumphal arch, has been etched into my memory. For good, ineradicably.

The telephone.

Naked, I walk down the stairs and look at my body: the soldier stepping hastily off the rocks and into the sea, caught out.

‘Yes, Govert,’ I say, ‘ sorry, but better not come round tonight. I think I’m going to turn in early for once, I’m dead beat.’

Back in bed I make myself come, the soldier is doing drill with a boy, enervating exercises to develop their endurance.

The Canadians are sitting down to dinner now in the Palace. What do they remember, what are they thinking of? And of whom? Or have they stopped thinking, have all the details become blurred?

Are events that were highlights for us trifles for them in a gigantic, heroic whole?

Are you alive, do you still exist?

The idea that you might have died strikes me suddenly as absurd, unthinkable. You can’t possibly have disappeared for good before we have looked each other in the eye just one more time, wondering together or perhaps smiling together as we reconsider our strange encounter.

When I was small — yes, that time, during the war — it was simple: I saw all of you sitting there, the minister’s wife, my mother and you, on a large grey bench. Like statues, staring fixedly into the void. I could read eternity in your eyes.

How simple it was then to walk up to you, to watch you move up and make room for me on that bench and to wait for the moment — and I felt sure that moment would come — when you would silently place your hand on my knee.

I rub my body dry as I used to dry my tears.

*Paris, June 1984 — Amsterdam, December 1985*
*With thanks to Inge, C.P. and Toer*
