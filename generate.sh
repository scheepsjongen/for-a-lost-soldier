#!/bin/bash

rm -f files/*

# PDF
pandoc --toc -o files/for-a-lost-soldier.pdf metadata.yml book.md

# HTML
pandoc --standalone --toc -o files/for-a-lost-soldier.html metadata.yml book.md

# EPUB3
pandoc -t epub3 -o files/for-a-lost-soldier.epub metadata.yml book.md

# EPUB3 to AZW3 using Calibre's ebook-convert
ebook-convert files/for-a-lost-soldier.epub files/for-a-lost-soldier.azw3
